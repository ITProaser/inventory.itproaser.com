﻿namespace ITProaser.Inventory.API.Context;

public class InventoryDbContext : ITProaserDbContext
{
    public InventoryDbContext(DbContextOptions options) : base(options)
    {
    }
 
    public DbSet<Item> Items { get; set; } = null!;

    public DbSet<ItemBrand> ItemBrands { get; set; } = null!;
    
    public DbSet<ItemCategory> ItemCategories { get; set; } = null!;

    public DbSet<ItemUnit> ItemUnits { get; set; } = null!;

    public DbSet<ItemManufacturer> ItemManufacturers { get; set; } = null!;

    public DbSet<ItemGroup> ItemGroups { get; set; } = null!;

    public DbSet<ItemTransactionInfo> ItemTransactionsInfo { get; set; } = null!;

    public DbSet<Customer> Customers { get; set; } = null!;

    public DbSet<Vendor> Vendors { get; set; } = null!;

    public DbSet<PurchaseOrder> PurchaseOrders { get; set; } = null!;
    
    public DbSet<PurchaseOrderLine> PurchaseOrderLines { get; set; } = null!;

    public DbSet<SaleOrder> SaleOrders { get; set; } = null!;
    
    public DbSet<SaleOrderLine> SaleOrderLines { get; set; } = null!;
}