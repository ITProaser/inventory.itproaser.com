﻿using System.Security.Claims;
using ITProaser.Models.Security;

namespace ITProaser.Inventory.API;

public sealed class InventoryDefaults
{
    public sealed class RolePermissions
    {
        public static readonly RoleClaim Items = new()
        {
            ClaimType = ClaimTypes.Role,
            ClaimValue = "items",
            IsAction = true
        };

        public static readonly RoleClaim ItemGroups = new()
        {
            ClaimType = ClaimTypes.Role,
            ClaimValue = "item_groups",
            IsAction = true
        };
        
        public static readonly RoleClaim ItemBrands = new()
        {
            ClaimType = ClaimTypes.Role,
            ClaimValue = "item_brands",
            IsAction = true
        };
        
        public static readonly RoleClaim ItemCategories = new()
        {
            ClaimType = ClaimTypes.Role,
            ClaimValue = "item_categories",
            IsAction = true
        };
        
        public static readonly RoleClaim ItemManufacturers = new()
        {
            ClaimType = ClaimTypes.Role,
            ClaimValue = "item_manufacturers",
            IsAction = true
        };
        
        public static readonly RoleClaim ItemUnits = new()
        {
            ClaimType = ClaimTypes.Role,
            ClaimValue = "item_units",
            IsAction = true
        };
        
        public static readonly RoleClaim Customers = new()
        {
            ClaimType = ClaimTypes.Role,
            ClaimValue = "customers",
            IsAction = true
        };
        
        public static readonly RoleClaim Vendors = new()
        {
            ClaimType = ClaimTypes.Role,
            ClaimValue = "vendors",
            IsAction = true
        };
        
        public static readonly RoleClaim SalesOrders = new()
        {
            ClaimType = ClaimTypes.Role,
            ClaimValue = "sales_orders",
            IsAction = true
        };
        
        public static readonly RoleClaim Invoices = new()
        {
            ClaimType = ClaimTypes.Role,
            ClaimValue = "invoices",
            IsAction = true
        };
        
        public static readonly RoleClaim PurchaseOrders = new()
        {
            ClaimType = ClaimTypes.Role,
            ClaimValue = "purchase_orders",
            IsAction = true
        };
        
        public static readonly RoleClaim Bills = new()
        {
            ClaimType = ClaimTypes.Role,
            ClaimValue = "bills",
            IsAction = true
        };

        public static readonly List<RoleClaim> Defaults = new()
        {
            Items, ItemBrands, ItemCategories, ItemManufacturers, ItemUnits, Customers, Vendors, SalesOrders, PurchaseOrders, Invoices, Bills
        };
    }
}