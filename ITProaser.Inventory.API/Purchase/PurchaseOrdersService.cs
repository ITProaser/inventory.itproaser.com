﻿namespace ITProaser.Inventory.API.Purchase;

public sealed class PurchaseOrdersService<TContext> : IPurchaseOrdersService where TContext : InventoryDbContext
{
    private readonly TContext _context;
    private readonly ICompanyService _companyService;
    
    public PurchaseOrdersService(TContext context, ICompanyService companyService)
        => (_context, _companyService) = (context, companyService);

    public async Task<bool> CheckOrderAsync(string tenancy, string id)
    {
        string? tenancyId = await _companyService.GetCompanyAsync(tenancy, c => c.Id);
        if (tenancyId is null)
            return false;
        return await (from order in _context.PurchaseOrders.AsNoTracking()
            where order.CompanyId == tenancyId && order.UId == id
            select order).AnyAsync();
    }

    public async Task<List<PurchaseOrder>> GetOrdersAsync(string tenancy)
        => await GetOrdersAsync(tenancy, order => order);

    public async Task<List<TResult>> GetOrdersAsync<TResult>(string tenancy, Expression<Func<PurchaseOrder, TResult>> selector)
    {
        string? tenancyId = await _companyService.GetCompanyAsync(tenancy, c => c.Id);
        if (tenancyId == null)
            return new List<TResult>();
        return await (from order in _context.PurchaseOrders.AsNoTracking()
            where order.CompanyId == tenancyId
            select order).Select(selector).Take(50).ToListAsync();
    }

    public async Task<PurchaseOrder?> GetOrderAsync(string tenancy, string id)
        => await GetOrderAsync(tenancy, id, order => order);

    public async Task<TResult?> GetOrderAsync<TResult>(string tenancy, string id, Expression<Func<PurchaseOrder, TResult>> selector)
    {
        string? tenancyId = await _companyService.GetCompanyAsync(tenancy, c => c.Id);
        if (tenancyId == null)
            return default;
        return await (from order in _context.PurchaseOrders.AsNoTracking()
            where order.CompanyId == tenancyId && order.UId == id
            select order).Select(selector).FirstOrDefaultAsync();
    }

    public async Task<PurchaseOrder?> GetOrderByReferenceAsync(string tenancy, string reference)
        => await GetOrderByReferenceAsync(tenancy, reference, order => order);

    public async Task<TResult?> GetOrderByReferenceAsync<TResult>(string tenancy, string reference, Expression<Func<PurchaseOrder, TResult>> selector)
    {
        string? tenancyId = await _companyService.GetCompanyAsync(tenancy, c => c.Id);
        if (tenancyId == null)
            return default;
        return await (from order in _context.PurchaseOrders.AsNoTracking()
            where order.CompanyId == tenancyId && order.Reference == reference
            select order).Select(selector).FirstOrDefaultAsync();
    }

    public async Task<PurchaseOrder?> GetOrderByNumberAsync(string tenancy, string number)
        => await GetOrderByNumberAsync(tenancy, number, order => order);

    public async Task<TResult?> GetOrderByNumberAsync<TResult>(string tenancy, string number, Expression<Func<PurchaseOrder, TResult>> selector)
    {
        string? tenancyId = await _companyService.GetCompanyAsync(tenancy, c => c.Id);
        if (tenancyId == null)
            return default;
        return await (from order in _context.PurchaseOrders.AsNoTracking()
            where order.CompanyId == tenancyId && order.Number == number
            select order).Select(selector).FirstOrDefaultAsync();
    }

    public async Task<bool> CreateOrderAsync(string tenancy, PurchaseOrder order)
    {
        throw new NotImplementedException();
    }
}