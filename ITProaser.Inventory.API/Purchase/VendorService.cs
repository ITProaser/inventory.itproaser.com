﻿namespace ITProaser.Inventory.API.Purchase;

public sealed class VendorService<TContext> : IVendorService where TContext : InventoryDbContext
{
    private readonly TContext _context;

    private readonly ICompanyService _companyService;

    public VendorService(TContext context, ICompanyService companyService)
        => (_context, _companyService) = (context, companyService);

    public async Task<bool> AddVendorAsync(string tenancy, Vendor vendor)
    {
        string? tenancyId = await _companyService.GetCompanyAsync(tenancy, company => company.Id);
        if (tenancyId is null)
            return false;
        if (vendor.CurrencyId is null)
            return false;
        if (await GetVendorByDisplayNameAsync(tenancy, vendor.DisplayName) is not null)
            return false;
        vendor.CompanyId = tenancyId;
        vendor.NormalizedFirstName = vendor.FirstName?.ToUpper();
        vendor.NormalizedLastName = vendor.LastName.ToUpper();
        vendor.NormalizedDisplayName = vendor.DisplayName.ToUpper();
        vendor.NormalizedEmail = vendor.Email?.ToUpper();
        await _context.Vendors.AddAsync(vendor);
        return await _context.SaveChangesAsync() is not 0;
    }

    public async Task<Vendor?> GetVendorAsync(string tenancy, string id)
        => await GetVendorAsync(tenancy, id, vendor => vendor);

    public async Task<TResult?> GetVendorAsync<TResult>(string tenancy, string id,
        Expression<Func<Vendor, TResult>> selector)
    {
        string? tenancyId = await _companyService.GetCompanyAsync(tenancy, company => company.Id);
        if (tenancyId is null)
            return default;
        return await (from vendor in _context.Vendors.AsNoTracking()
            where vendor.CompanyId == tenancyId && vendor.UId == id
            select vendor).Select(selector).FirstOrDefaultAsync();
    }

    public async Task<Vendor?> GetVendorByDisplayNameAsync(string tenancy, string displayName)
        => await GetVendorByDisplayNameAsync(tenancy, displayName, vendor => vendor);

    public async Task<TResult?> GetVendorByDisplayNameAsync<TResult>(string tenancy, string displayName,
        Expression<Func<Vendor, TResult>> selector)
    {
        string? tenancyId = await _companyService.GetCompanyAsync(tenancy, company => company.Id);
        if (tenancyId is null)
            return default;
        return await (from vendor in _context.Vendors.AsNoTracking()
            where vendor.CompanyId == tenancyId && vendor.NormalizedDisplayName == displayName.ToUpper()
            select vendor).Select(selector).FirstOrDefaultAsync();
    }

    public async Task<Currency?> GetVendorCurrencyAsync(string tenancy, string id)
        => await GetVendorCurrencyAsync(tenancy, id, currency => currency);

    public async Task<TResult?> GetVendorCurrencyAsync<TResult>(string tenancy, string id,
        Expression<Func<Currency, TResult>> selector)
    {
        string? tenancyId = await _companyService.GetCompanyAsync(tenancy, company => company.Id);
        if (tenancyId is null)
            return default;
        string? currency = await GetVendorAsync(tenancy, id, vendor => vendor.CurrencyId);
        if (currency is null)
            return default;
        return await (from c in _context.Currencies.AsNoTracking()
            where c.Id == currency && c.CompanyId == tenancyId
            select c).Select(selector).FirstOrDefaultAsync();
    }

    public async Task<List<Vendor>> GetVendorsAsync(string tenancy)
        => await GetVendorsAsync(tenancy, vendors => vendors);

    public async Task<List<TResult>> GetVendorsAsync<TResult>(string tenancy,
        Expression<Func<Vendor, TResult>> selector)
    {
        string? tenancyId = await _companyService.GetCompanyAsync(tenancy, company => company.Id);
        if (tenancyId is null)
            return new List<TResult>();
        return await (from vendor in _context.Vendors.Include(v => v.Currency).Include(v => v.Tax)
                    .Include(v => v.PaymentTerm).Include(v => v.BillingAddress).Include(v => v.ShippingAddress)
                    .AsNoTracking()
                where vendor.CompanyId == tenancyId
                select vendor)
            .Select(selector).ToListAsync();
    }

    public async Task<bool> UpdateVendorAsync(string tenancy, string id, Vendor vendor)
    {
        string? tenancyId = await _companyService.GetCompanyAsync(tenancy, company => company.Id);
        if (tenancyId is null)
            return false;
        if (vendor.CurrencyId is null)
            return false;
        string? exists = await GetVendorByDisplayNameAsync(tenancy, vendor.DisplayName, v => v.UId);
        if (exists is not null && exists != id)
            return false;
        var target = await (from v in _context.Vendors.Include(v => v.BillingAddress)
                .Include(v => v.ShippingAddress).AsTracking()
            where v.CompanyId == tenancyId && v.UId == id
            select v).FirstOrDefaultAsync();
        if (target is null)
            return false;
        target.FirstName = vendor.FirstName;
        target.LastName = vendor.LastName;
        target.DisplayName = vendor.DisplayName;
        target.Email = vendor.Email;
        target.NormalizedFirstName = vendor.FirstName?.ToUpper();
        target.NormalizedLastName = vendor.LastName.ToUpper();
        target.NormalizedDisplayName = vendor.DisplayName.ToUpper();
        target.NormalizedEmail = vendor.Email?.ToUpper();
        target.Remarks = vendor.Remarks;
        target.Mobile = vendor.Mobile;
        target.Title = vendor.Title;
        target.Phone = vendor.Phone;
        target.Website = vendor.Website;
        target.TaxId = vendor.TaxId;
        target.CurrencyId = vendor.CurrencyId;
        target.PaymentTermId = vendor.PaymentTermId;
        if (target.BillingAddress is null)
            target.BillingAddress = vendor.BillingAddress;
        else if (vendor.BillingAddress is not null)
        {
            target.BillingAddress.Country = vendor.BillingAddress.Country;
            target.BillingAddress.City = vendor.BillingAddress.City;
            target.BillingAddress.Street = vendor.BillingAddress.Street;
            target.BillingAddress.ZipCode = vendor.BillingAddress.ZipCode;
            target.BillingAddress.State = vendor.BillingAddress.State;
        }

        if (target.ShippingAddress is null)
            target.ShippingAddress = vendor.ShippingAddress;
        else if (vendor.ShippingAddress is not null)
        {
            target.ShippingAddress.Country = vendor.ShippingAddress.Country;
            target.ShippingAddress.City = vendor.ShippingAddress.City;
            target.ShippingAddress.Street = vendor.ShippingAddress.Street;
            target.ShippingAddress.ZipCode = vendor.ShippingAddress.ZipCode;
            target.ShippingAddress.State = vendor.ShippingAddress.State;
        }
        return await _context.SaveChangesAsync() is not 0;
    }
}