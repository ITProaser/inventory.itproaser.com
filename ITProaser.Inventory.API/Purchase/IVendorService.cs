﻿namespace ITProaser.Inventory.API.Purchase;

public interface IVendorService
{
    public Task<bool> AddVendorAsync(string tenancy, Vendor vendor);
    
    public Task<Vendor?> GetVendorAsync(string tenancy, string id);
    
    public Task<TResult?> GetVendorAsync<TResult>(string tenancy, string id, Expression<Func<Vendor, TResult>> selector);
    
    public Task<Vendor?> GetVendorByDisplayNameAsync(string tenancy, string displayName);
    
    public Task<TResult?> GetVendorByDisplayNameAsync<TResult>(string tenancy, string displayName, Expression<Func<Vendor, TResult>> selector);
    
    public Task<Currency?> GetVendorCurrencyAsync(string tenancy, string id);
    
    public Task<TResult?> GetVendorCurrencyAsync<TResult>(string tenancy, string id, Expression<Func<Currency, TResult>> selector);
    
    public Task<List<Vendor>> GetVendorsAsync(string tenancy);
    
    public Task<List<TResult>> GetVendorsAsync<TResult>(string tenancy, Expression<Func<Vendor, TResult>> selector);
    
    public Task<bool> UpdateVendorAsync(string tenancy, string id, Vendor vendor);
}