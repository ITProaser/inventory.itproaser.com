﻿namespace ITProaser.Inventory.API.Purchase;

public interface IPurchaseOrdersService
{
    public Task<bool> CheckOrderAsync(string tenancy, string id);
    
    public Task<List<PurchaseOrder>> GetOrdersAsync(string tenancy);
    
    public Task<List<TResult>> GetOrdersAsync<TResult>(string tenancy, Expression<Func<PurchaseOrder, TResult>> selector);

    public Task<PurchaseOrder?> GetOrderAsync(string tenancy, string id);
    
    public Task<TResult?> GetOrderAsync<TResult>(string tenancy, string id, Expression<Func<PurchaseOrder, TResult>> selector);
    
    public Task<PurchaseOrder?> GetOrderByReferenceAsync(string tenancy, string reference);
    
    public Task<TResult?> GetOrderByReferenceAsync<TResult>(string tenancy, string reference, Expression<Func<PurchaseOrder, TResult>> selector);
    
    public Task<PurchaseOrder?> GetOrderByNumberAsync(string tenancy, string number);
    
    public Task<TResult?> GetOrderByNumberAsync<TResult>(string tenancy, string number, Expression<Func<PurchaseOrder, TResult>> selector);
    
    public Task<bool> CreateOrderAsync(string tenancy, PurchaseOrder order);
}