﻿namespace ITProaser.Inventory.API.Sales;

public sealed class CustomerService<TContext> : ICustomerService where TContext : InventoryDbContext
{
    private readonly TContext _context;

    private readonly ICompanyService _companyService;

    public CustomerService(TContext context, ICompanyService companyService)
        => (_context, _companyService) = (context, companyService);

    public async Task<bool> AddCustomerAsync(string tenancy, Customer customer)
    {
        string? tenancyId = await _companyService.GetCompanyAsync(tenancy, company => company.Id);
        if (tenancyId is null)
            return false;
        if (customer.CurrencyId is null)
            return false;
        customer.CompanyId = tenancyId;
        customer.NormalizedFirstName = customer.FirstName?.ToUpper();
        customer.NormalizedLastName = customer.LastName.ToUpper();
        customer.NormalizedDisplayName = customer.DisplayName.ToUpper();
        customer.NormalizedEmail = customer.Email?.ToUpper();
        if (await GetCustomerByDisplayNameAsync(tenancy, customer.DisplayName) is not null)
            return false;
        await _context.Customers.AddAsync(customer);
        return await _context.SaveChangesAsync() is not 0;
    }

    public async Task<Customer?> GetCustomerAsync(string tenancy, string id)
        => await GetCustomerAsync(tenancy, id, customer => customer);

    public async Task<TResult?> GetCustomerAsync<TResult>(string tenancy, string id,
        Expression<Func<Customer, TResult>> selector)
    {
        string? tenancyId = await _companyService.GetCompanyAsync(tenancy, company => company.Id);
        if (tenancyId is null)
            return default;
        return await (from customer in _context.Customers.AsNoTracking()
            where customer.CompanyId == tenancyId && customer.UId == id
            select customer).Select(selector).FirstOrDefaultAsync();
    }

    public async Task<Customer?> GetCustomerByDisplayNameAsync(string tenancy, string displayName)
        => await GetCustomerAsync(tenancy, displayName, customer => customer);

    public async Task<TResult?> GetCustomerByDisplayNameAsync<TResult>(string tenancy, string displayName,
        Expression<Func<Customer, TResult>> selector)
    {
        string? tenancyId = await _companyService.GetCompanyAsync(tenancy, company => company.Id);
        if (tenancyId is null)
            return default;
        return await (from customer in _context.Customers.AsNoTracking()
                where customer.CompanyId == tenancyId && customer.NormalizedDisplayName == displayName.ToUpper()
                select customer)
            .Select(selector).FirstOrDefaultAsync();
    }

    public async Task<Currency?> GetCustomerCurrencyAsync(string tenancy, string id)
        => await GetCustomerCurrencyAsync(tenancy, id, currency => currency);

    public async Task<TResult?> GetCustomerCurrencyAsync<TResult>(string tenancy, string id,
        Expression<Func<Currency, TResult>> selector)
    {
        string? tenancyId = await _companyService.GetCompanyAsync(tenancy, company => company.Id);
        if (tenancyId is null)
            return default;
        string? currency = await GetCustomerAsync(tenancy, id, vendor => vendor.CurrencyId);
        if (currency is null)
            return default;
        return await (from c in _context.Currencies.AsNoTracking()
            where c.Id == currency
            select c).Select(selector).FirstOrDefaultAsync();
    }

    public async Task<List<Customer>> GetCustomersAsync(string tenancy)
        => await GetCustomersAsync(tenancy, customers => customers);

    public async Task<List<TResult>> GetCustomersAsync<TResult>(string tenancy,
        Expression<Func<Customer, TResult>> selector)
    {
        string? tenancyId = await _companyService.GetCompanyAsync(tenancy, company => company.Id);
        if (tenancyId is null)
            return new List<TResult>();
        return await (from customer in _context.Customers.Include(c => c.Currency).Include(c => c.Tax)
                .Include(c => c.PaymentTerm).Include(c => c.BillingAddress).Include(c => c.ShippingAddress)
                .Include(c => c.Owner).AsNoTracking()
            where customer.CompanyId == tenancyId
            select customer).Select(selector).ToListAsync();
    }

    public async Task<bool> UpdateCustomerAsync(string tenancy, string id, Customer customer)
    {
        string? tenancyId = await _companyService.GetCompanyAsync(tenancy, company => company.Id);
        if (tenancyId is null)
            return false;
        if (customer.CurrencyId is null)
            return false;
        var target = await (from v in _context.Customers.Include(v => v.BillingAddress)
                .Include(v => v.ShippingAddress).AsTracking()
            where v.CompanyId == tenancyId && v.UId == id
            select v).FirstOrDefaultAsync();
        if (target is null)
            return false;
        string? exists = await GetCustomerByDisplayNameAsync(tenancy, customer.DisplayName, v => v.UId);
        if (exists is not null && exists != id)
            return false;
        target.FirstName = customer.FirstName;
        target.LastName = customer.LastName;
        target.DisplayName = customer.DisplayName;
        target.Email = customer.Email;
        target.NormalizedFirstName = customer.FirstName?.ToUpper();
        target.NormalizedLastName = customer.LastName.ToUpper();
        target.NormalizedDisplayName = customer.DisplayName.ToUpper();
        target.NormalizedEmail = customer.Email?.ToUpper();
        target.Remarks = customer.Remarks;
        target.Mobile = customer.Mobile;
        target.Title = customer.Title;
        target.Phone = customer.Phone;
        target.Website = customer.Website;
        target.CurrencyId = customer.CurrencyId;
        target.TaxId = customer.TaxId;
        target.PaymentTermId = customer.PaymentTermId;
        if (target.BillingAddress is null)
            target.BillingAddress = customer.BillingAddress;
        else if (customer.BillingAddress is not null)
        {
            target.BillingAddress.Country = customer.BillingAddress.Country;
            target.BillingAddress.City = customer.BillingAddress.City;
            target.BillingAddress.Street = customer.BillingAddress.Street;
            target.BillingAddress.ZipCode = customer.BillingAddress.ZipCode;
            target.BillingAddress.State = customer.BillingAddress.State;
        }
        if (target.ShippingAddress is null)
            target.ShippingAddress = customer.ShippingAddress;
        else if (customer.ShippingAddress is not null)
        {
            target.ShippingAddress.Country = customer.ShippingAddress.Country;
            target.ShippingAddress.City = customer.ShippingAddress.City;
            target.ShippingAddress.Street = customer.ShippingAddress.Street;
            target.ShippingAddress.ZipCode = customer.ShippingAddress.ZipCode;
            target.ShippingAddress.State = customer.ShippingAddress.State;
        }
        return await _context.SaveChangesAsync() is not 0;
    }
}