﻿namespace ITProaser.Inventory.API.Sales;

public interface ICustomerService
{
    public Task<bool> AddCustomerAsync(string tenancy, Customer customer);
    
    public Task<Customer?> GetCustomerAsync(string tenancy, string id);
    
    public Task<TResult?> GetCustomerAsync<TResult>(string tenancy, string id, Expression<Func<Customer, TResult>> selector);
    
    public Task<Customer?> GetCustomerByDisplayNameAsync(string tenancy, string displayName);
    
    public Task<TResult?> GetCustomerByDisplayNameAsync<TResult>(string tenancy, string displayName, Expression<Func<Customer, TResult>> selector);

    public Task<Currency?> GetCustomerCurrencyAsync(string tenancy, string id);
    
    public Task<TResult?> GetCustomerCurrencyAsync<TResult>(string tenancy, string id, Expression<Func<Currency, TResult>> selector);

    public Task<List<Customer>> GetCustomersAsync(string tenancy);
    
    public Task<List<TResult>> GetCustomersAsync<TResult>(string tenancy, Expression<Func<Customer, TResult>> selector);
    
    public Task<bool> UpdateCustomerAsync(string tenancy, string id, Customer customer);
}