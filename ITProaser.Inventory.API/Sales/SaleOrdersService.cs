﻿namespace ITProaser.Inventory.API.Sales;

public sealed class SaleOrdersService<TContext> : ISaleOrdersService where TContext : InventoryDbContext
{
    private readonly TContext _context;
    private readonly ICompanyService _companyService;

    public SaleOrdersService(TContext context, ICompanyService companyService)
        => (_context, _companyService) = (context, companyService);

    public async Task<bool> CheckOrderAsync(string tenancy, string id)
    {
        string? tenancyId = await _companyService.GetCompanyAsync(id, c => c.Id);
        if (tenancyId is null)
            return false;
        return await (from order in _context.SaleOrders.AsNoTracking()
            where order.CompanyId == tenancyId && order.UId == id
            select order).AnyAsync();
    }

    public async Task<List<SaleOrder>> GetOrdersAsync(string tenancy)
        => await GetOrdersAsync(tenancy, order => order);

    public async Task<List<TResult>> GetOrdersAsync<TResult>(string tenancy,
        Expression<Func<SaleOrder, TResult>> selector)
    {
        string? tenancyId = await _companyService.GetCompanyAsync(tenancy, c => c.Id);
        if (tenancyId is null)
            return new List<TResult>();
        return await (from order in _context.SaleOrders.AsNoTracking() where order.CompanyId == tenancyId select order)
            .Select(selector).ToListAsync();
    }

    public async Task<SaleOrder?> GetOrderAsync(string tenancy, string id)
        => await GetOrderAsync(tenancy, id, order => order);

    public async Task<TResult?> GetOrderAsync<TResult>(string tenancy, string id,
        Expression<Func<SaleOrder, TResult>> selector)
    {
        string? tenancyId = await _companyService.GetCompanyAsync(tenancy, c => c.Id);
        if (tenancyId is null)
            return default;
        return await (from order in _context.SaleOrders.AsNoTracking() where order.CompanyId == tenancyId && order.Id == id select order)
            .Select(selector).FirstOrDefaultAsync();
    }

    public async Task<SaleOrder?> GetOrderByReferenceAsync(string tenancy, string reference)
        => await GetOrderAsync(tenancy, reference, order => order);

    public async Task<TResult?> GetOrderByReferenceAsync<TResult>(string tenancy, string reference,
        Expression<Func<SaleOrder, TResult>> selector)
    {
        string? tenancyId = await _companyService.GetCompanyAsync(tenancy, c => c.Id);
        if (tenancyId is null)
            return default;
        return await (from order in _context.SaleOrders.AsNoTracking() where order.CompanyId == tenancyId && order.Reference == reference select order)
            .Select(selector).FirstOrDefaultAsync();
    }

    public async Task<SaleOrder?> GetOrderByNumberAsync(string tenancy, string number)
        => await GetOrderAsync(tenancy, number, order => order);

    public async Task<TResult?> GetOrderByNumberAsync<TResult>(string tenancy, string number,
        Expression<Func<SaleOrder, TResult>> selector)
    {
        string? tenancyId = await _companyService.GetCompanyAsync(tenancy, c => c.Id);
        if (tenancyId is null)
            return default;
        return await (from order in _context.SaleOrders.AsNoTracking() where order.CompanyId == tenancyId && order.Number == number select order)
            .Select(selector).FirstOrDefaultAsync();
    }
}