﻿namespace ITProaser.Inventory.API.Sales;

public interface ISaleOrdersService
{
    public Task<bool> CheckOrderAsync(string tenancy, string id);
    
    public Task<List<SaleOrder>> GetOrdersAsync(string tenancy);
    
    public Task<List<TResult>> GetOrdersAsync<TResult>(string tenancy, Expression<Func<SaleOrder, TResult>> selector);

    public Task<SaleOrder?> GetOrderAsync(string tenancy, string id);
    
    public Task<TResult?> GetOrderAsync<TResult>(string tenancy, string id, Expression<Func<SaleOrder, TResult>> selector);
    
    public Task<SaleOrder?> GetOrderByReferenceAsync(string tenancy, string reference);
    
    public Task<TResult?> GetOrderByReferenceAsync<TResult>(string tenancy, string reference, Expression<Func<SaleOrder, TResult>> selector);
    
    public Task<SaleOrder?> GetOrderByNumberAsync(string tenancy, string number);
    
    public Task<TResult?> GetOrderByNumberAsync<TResult>(string tenancy, string number, Expression<Func<SaleOrder, TResult>> selector);
}