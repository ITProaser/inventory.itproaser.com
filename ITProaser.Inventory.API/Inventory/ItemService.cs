﻿namespace ITProaser.Inventory.API.Inventory;

public sealed class ItemService<TContext> : IItemService where TContext : InventoryDbContext
{
    private readonly TContext _context;
    private readonly ICompanyService _companyService;

    public ItemService(TContext context, ICompanyService companyService)
        => (_context, _companyService) = (context, companyService);

    public async Task<bool> CheckItemAsync(string tenantId, string id)
    {
        string? tenant = await _companyService.GetCompanyAsync(tenantId, company => company.Id);
        if (tenant is null) return false;
        return await (from item in _context.Items.AsNoTracking()
            where item.CompanyId == tenant && item.UId == id
            select item).AnyAsync();
    }

    public async Task<bool> CheckItemByNameAsync(string tenantId, string name)
    {
        string? tenant = await _companyService.GetCompanyAsync(tenantId, company => company.Id);
        if (tenant is null) return false;
        return await (from item in _context.Items.AsNoTracking()
            where item.CompanyId == tenant && item.NormalizedName == name.ToUpper()
            select item).AnyAsync();
    }

    public async Task<bool> CheckItemBrandAsync(string tenantId, string id)
    {
        string? tenant = await _companyService.GetCompanyAsync(tenantId, company => company.Id);
        if (tenant is null) return false;
        return await (from brand in _context.ItemBrands.AsNoTracking()
            where brand.CompanyId == tenant && brand.UId == id
            select brand).AnyAsync();
    }

    public async Task<bool> CheckItemBrandByNameAsync(string tenantId, string name)
    {
        string? tenant = await _companyService.GetCompanyAsync(tenantId, company => company.Id);
        if (tenant is null) return false;
        return await (from brand in _context.ItemBrands.AsNoTracking()
            where brand.CompanyId == tenant && brand.NormalizedName == name.ToUpper()
            select brand).AnyAsync();
    }

    public async Task<bool> CheckItemCategoryAsync(string tenantId, string id)
    {
        string? tenant = await _companyService.GetCompanyAsync(tenantId, company => company.Id);
        if (tenant is null) return false;
        return await (from category in _context.ItemCategories.AsNoTracking()
            where category.CompanyId == tenant && category.UId == id
            select category).AnyAsync();
    }

    public async Task<bool> CheckItemCategoryByNameAsync(string tenantId, string name)
    {
        string? tenant = await _companyService.GetCompanyAsync(tenantId, company => company.Id);
        if (tenant is null) return false;
        return await (from category in _context.ItemCategories.AsNoTracking()
            where category.CompanyId == tenant && category.NormalizedName == name.ToUpper()
            select category).AnyAsync();
    }

    public async Task<bool> CheckItemManufacturerAsync(string tenantId, string id)
    {
        string? tenant = await _companyService.GetCompanyAsync(tenantId, company => company.Id);
        if (tenant is null) return false;
        return await (from manufacturer in _context.ItemManufacturers.AsNoTracking()
            where manufacturer.CompanyId == tenant && manufacturer.UId == id
            select manufacturer).AnyAsync();
    }

    public async Task<bool> CheckItemManufacturerByNameAsync(string tenantId, string name)
    {
        string? tenant = await _companyService.GetCompanyAsync(tenantId, company => company.Id);
        if (tenant is null) return false;
        return await (from manufacturer in _context.ItemManufacturers.AsNoTracking()
            where manufacturer.CompanyId == tenant && manufacturer.NormalizedName == name.ToUpper()
            select manufacturer).AnyAsync();
    }

    public async Task<bool> CheckItemUnitAsync(string tenantId, string id)
    {
        string? tenant = await _companyService.GetCompanyAsync(tenantId, company => company.Id);
        if (tenant is null) return false;
        return await (from unit in _context.ItemUnits.AsNoTracking()
            where unit.CompanyId == tenant && unit.UId == id
            select unit).AnyAsync();
    }

    public async Task<bool> CheckItemUnitByNameAsync(string tenantId, string name)
    {
        string? tenant = await _companyService.GetCompanyAsync(tenantId, company => company.Id);
        if (tenant is null) return false;
        return await (from unit in _context.ItemUnits.AsNoTracking()
            where unit.CompanyId == tenant && unit.NormalizedName == name.ToUpper()
            select unit).AnyAsync();
    }

    public async Task<bool> CreateBrandAsync(string tenantId, ItemBrand brand)
    {
        if (await CheckItemBrandByNameAsync(tenantId, brand.Name)) return false;
        string? tenancy = await _companyService.GetCompanyAsync(tenantId, company => company.Id);
        if (tenancy is null)
            return false;
        await _context.ItemBrands.AddAsync(brand);
        brand.CompanyId = tenancy;
        return await _context.SaveChangesAsync() is not 0;
    }

    public async Task<bool> CreateCategoryAsync(string tenantId, ItemCategory category)
    {
        if (await CheckItemCategoryByNameAsync(tenantId, category.Name)) return false;
        string? tenancy = await _companyService.GetCompanyAsync(tenantId, company => company.Id);
        if (tenancy is null)
            return false;
        await _context.ItemCategories.AddAsync(category);
        category.CompanyId = tenancy;
        return await _context.SaveChangesAsync() is not 0;
    }

    public async Task<bool> CreateItemAsync(string tenantId, Item item)
    {
        string? tenancy = await _companyService.GetCompanyAsync(tenantId, company => company.Id);
        if (tenancy is null)
            return false;
        if (await CheckItemByNameAsync(tenantId, item.Name)) return false;
        string? currency = await _companyService.GetCurrencyAsync(tenantId, c => c.Id);
        item.CompanyId = tenancy;
        item.NormalizedName = item.Name.ToUpper();
        if (item.PurchaseInformation is not null && currency is not null)
        {
            item.PurchaseInformation.CurrencyId = currency;
            item.PurchaseInformation.CompanyId = tenancy;
        }
        if (item.SalesInformation is not null && currency is not null)
        {
            item.SalesInformation.CurrencyId = currency;
            item.SalesInformation.CompanyId = tenancy;
        }
        await _context.Items.AddAsync(item);
        return await _context.SaveChangesAsync() is not 0;
    }

    public async Task<bool> CreateManufacturerAsync(string tenantId, ItemManufacturer manufacturer)
    {
        if (await CheckItemManufacturerByNameAsync(tenantId, manufacturer.Name)) return false;
        string? tenancy = await _companyService.GetCompanyAsync(tenantId, company => company.Id);
        if (tenancy is null)
            return false;
        await _context.ItemManufacturers.AddAsync(manufacturer);
        manufacturer.CompanyId = tenancy;
        return await _context.SaveChangesAsync() is not 0;
    }

    public async Task<bool> CreateUnitAsync(string tenantId, ItemUnit unit)
    {
        if (await CheckItemUnitByNameAsync(tenantId, unit.Name)) return false;
        string? tenancy = await _companyService.GetCompanyAsync(tenantId, company => company.Id);
        if (tenancy is null)
            return false;
        await _context.ItemUnits.AddAsync(unit);
        unit.CompanyId = tenancy;
        return await _context.SaveChangesAsync() is not 0;
    }

    public async Task<ItemBrand?> GetItemBrandAsync(string tenantId, string id)
        => await GetItemBrandAsync(tenantId, id, brand => brand);

    public async Task<TResult?> GetItemBrandAsync<TResult>(string tenantId, string id,
        Expression<Func<ItemBrand, TResult>> selector)
    {
        string? tenant = await _companyService.GetCompanyAsync(tenantId, company => company.Id);
        if (tenant is null) return default;
        return await (from brand in _context.ItemBrands.AsNoTracking()
            where brand.CompanyId == tenant && brand.UId == id
            select brand).Select(selector).FirstOrDefaultAsync();
    }
    
    public async Task<ItemBrand?> GetItemBrandByNameAsync(string tenantId, string name)
        => await GetItemBrandAsync(tenantId, name, brand => brand);

    public async Task<TResult?> GetItemBrandByNameAsync<TResult>(string tenantId, string name,
        Expression<Func<ItemBrand, TResult>> selector)
    {
        string? tenant = await _companyService.GetCompanyAsync(tenantId, company => company.Id);
        if (tenant is null) return default;
        return await (from brand in _context.ItemBrands.AsNoTracking()
            where brand.CompanyId == tenant && brand.Name == name.ToUpper()
            select brand).Select(selector).FirstOrDefaultAsync();
    }

    public async Task<ItemBrand?> GetItemBrandTrackingByNameAsync(string tenantId, string name)
        => await GetItemBrandAsync(tenantId, name, brand => brand);

    public async Task<TResult?> GetItemBrandTrackingByNameAsync<TResult>(string tenantId, string name,
        Expression<Func<ItemBrand, TResult>> selector)
    {
        string? tenant = await _companyService.GetCompanyAsync(tenantId, company => company.Id);
        if (tenant is null) return default;
        return await (from brand in _context.ItemBrands.AsTracking()
            where brand.CompanyId == tenant && brand.Name == name.ToUpper()
            select brand).Select(selector).FirstOrDefaultAsync();
    }

    public async Task<List<ItemBrand>> GetItemBrandsAsync(string tenantId)
        => await GetItemBrandsAsync(tenantId, brand => brand);

    public async Task<List<TResult>> GetItemBrandsAsync<TResult>(string tenantId,
        Expression<Func<ItemBrand, TResult>> selector)
    {
        string? tenant = await _companyService.GetCompanyAsync(tenantId, company => company.Id);
        if (tenant is null) return new List<TResult>();
        return await (from brand in _context.ItemBrands.AsNoTracking()
            where brand.CompanyId == tenant
            select brand).Select(selector).ToListAsync();
    }

    public async Task<ItemCategory?> GetItemCategoryAsync(string tenantId, string id)
        => await GetItemCategoryAsync(tenantId, id, category => category);

    public async Task<TResult?> GetItemCategoryAsync<TResult>(string tenantId, string id,
        Expression<Func<ItemCategory, TResult>> selector)
    {
        string? tenant = await _companyService.GetCompanyAsync(tenantId, company => company.Id);
        if (tenant is null) return default;
        return await (from category in _context.ItemCategories.AsNoTracking()
            where category.CompanyId == tenant && category.UId == id
            select category).Select(selector).FirstOrDefaultAsync();
    }

    public async Task<ItemCategory?> GetItemCategoryByNameAsync(string tenantId, string name)
        => await GetItemCategoryAsync(tenantId, name, category => category);

    public async Task<TResult?> GetItemCategoryByNameAsync<TResult>(string tenantId, string name,
        Expression<Func<ItemCategory, TResult>> selector)
    {
        string? tenant = await _companyService.GetCompanyAsync(tenantId, company => company.Id);
        if (tenant is null) return default;
        return await (from category in _context.ItemCategories.AsNoTracking()
            where category.CompanyId == tenant && category.Name == name.ToUpper()
            select category).Select(selector).FirstOrDefaultAsync();
    }

    public async Task<ItemCategory?> GetItemCategoryTrackingByNameAsync(string tenantId, string name)
        => await GetItemCategoryAsync(tenantId, name, category => category);

    public async Task<TResult?> GetItemCategoryTrackingByNameAsync<TResult>(string tenantId, string name,
        Expression<Func<ItemCategory, TResult>> selector)
    {
        string? tenant = await _companyService.GetCompanyAsync(tenantId, company => company.Id);
        if (tenant is null) return default;
        return await (from category in _context.ItemCategories.AsTracking()
            where category.CompanyId == tenant && category.Name == name.ToUpper()
            select category).Select(selector).FirstOrDefaultAsync();
    }

    public async Task<List<ItemCategory>> GetItemCategoriesAsync(string tenantId)
        => await GetItemCategoriesAsync(tenantId, category => category);

    public async Task<List<TResult>> GetItemCategoriesAsync<TResult>(string tenantId,
        Expression<Func<ItemCategory, TResult>> selector)
    {
        string? tenant = await _companyService.GetCompanyAsync(tenantId, company => company.Id);
        if (tenant is null) return new List<TResult>();
        return await (from category in _context.ItemCategories.AsNoTracking()
            where category.CompanyId == tenant
            select category).Select(selector).ToListAsync();
    }

    public async Task<ItemManufacturer?> GetItemManufacturerAsync(string tenantId, string id)
        => await GetItemManufacturerAsync(tenantId, id, manufacturer => manufacturer);

    public async Task<TResult?> GetItemManufacturerAsync<TResult>(string tenantId, string id,
        Expression<Func<ItemManufacturer, TResult>> selector)
    {
        string? tenant = await _companyService.GetCompanyAsync(tenantId, company => company.Id);
        if (tenant is null) return default;
        return await (from manufacturer in _context.ItemManufacturers.AsNoTracking()
            where manufacturer.CompanyId == tenant && manufacturer.UId == id
            select manufacturer).Select(selector).FirstOrDefaultAsync();
    }

    public async Task<ItemManufacturer?> GetItemManufacturerByNameAsync(string tenantId, string name)
        => await GetItemManufacturerAsync(tenantId, name, manufacturer => manufacturer);

    public async Task<TResult?> GetItemManufacturerByNameAsync<TResult>(string tenantId, string name,
        Expression<Func<ItemManufacturer, TResult>> selector)
    {
        string? tenant = await _companyService.GetCompanyAsync(tenantId, company => company.Id);
        if (tenant is null) return default;
        return await (from manufacturer in _context.ItemManufacturers.AsNoTracking()
            where manufacturer.CompanyId == tenant && manufacturer.Name == name.ToUpper()
            select manufacturer).Select(selector).FirstOrDefaultAsync();
    }

    public async Task<ItemManufacturer?> GetItemManufacturerTrackingByNameAsync(string tenantId, string name)
        => await GetItemManufacturerAsync(tenantId, name, manufacturer => manufacturer);

    public async Task<TResult?> GetItemManufacturerTrackingByNameAsync<TResult>(string tenantId, string name,
        Expression<Func<ItemManufacturer, TResult>> selector)
    {
        string? tenant = await _companyService.GetCompanyAsync(tenantId, company => company.Id);
        if (tenant is null) return default;
        return await (from manufacturer in _context.ItemManufacturers.AsTracking()
            where manufacturer.CompanyId == tenant && manufacturer.Name == name.ToUpper()
            select manufacturer).Select(selector).FirstOrDefaultAsync();
    }

    public async Task<List<ItemManufacturer>> GetItemManufacturersAsync(string tenantId)
        => await GetItemManufacturersAsync(tenantId, manufacturer => manufacturer);

    public async Task<List<TResult>> GetItemManufacturersAsync<TResult>(string tenantId,
        Expression<Func<ItemManufacturer, TResult>> selector)
    {
        string? tenant = await _companyService.GetCompanyAsync(tenantId, company => company.Id);
        if (tenant is null) return new List<TResult>();
        return await (from manufacturer in _context.ItemManufacturers.AsNoTracking()
            where manufacturer.Company.UId == tenantId
            select manufacturer).Select(selector).ToListAsync();
    }

    public async Task<ItemUnit?> GetItemUnitAsync(string tenantId, string id)
        => await GetItemUnitAsync(tenantId, id, unit => unit);

    public async Task<TResult?> GetItemUnitAsync<TResult>(string tenantId, string id,
        Expression<Func<ItemUnit, TResult>> selector)
    {
        string? tenant = await _companyService.GetCompanyAsync(tenantId, company => company.Id);
        if (tenant is null) return default;
        return await (from unit in _context.ItemUnits.AsNoTracking()
            where unit.CompanyId == tenant && unit.UId == id
            select unit).Select(selector).FirstOrDefaultAsync();
    }

    public async Task<ItemUnit?> GetItemUnitByNameAsync(string tenantId, string name)
        => await GetItemUnitAsync(tenantId, name, unit => unit);

    public async Task<TResult?> GetItemUnitByNameAsync<TResult>(string tenantId, string name,
        Expression<Func<ItemUnit, TResult>> selector)
    {
        string? tenant = await _companyService.GetCompanyAsync(tenantId, company => company.Id);
        if (tenant is null) return default;
        return await (from unit in _context.ItemUnits.AsNoTracking()
            where unit.CompanyId == tenant && unit.Name == name.ToUpper()
            select unit).Select(selector).FirstOrDefaultAsync();
    }

    public async Task<ItemUnit?> GetItemUnitTrackingByNameAsync(string tenantId, string name)
        => await GetItemUnitAsync(tenantId, name, unit => unit);

    public async Task<TResult?> GetItemUnitTrackingByNameAsync<TResult>(string tenantId, string name,
        Expression<Func<ItemUnit, TResult>> selector)
    {
        string? tenant = await _companyService.GetCompanyAsync(tenantId, company => company.Id);
        if (tenant is null) return default;
        return await (from unit in _context.ItemUnits.AsTracking()
            where unit.CompanyId == tenant && unit.Name == name.ToUpper()
            select unit).Select(selector).FirstOrDefaultAsync();
    }

    public async Task<List<ItemUnit>> GetItemUnitsAsync(string tenantId)
        => await GetItemUnitsAsync(tenantId, unit => unit);

    public async Task<List<TResult>> GetItemUnitsAsync<TResult>(string tenantId,
        Expression<Func<ItemUnit, TResult>> selector)
    {
        string? tenant = await _companyService.GetCompanyAsync(tenantId, company => company.Id);
        if (tenant is null) return new List<TResult>();
        return await (from unit in _context.ItemUnits.AsNoTracking()
            where unit.CompanyId == tenant
            select unit).Select(selector).ToListAsync();
    }

    public async Task<List<Item>> GetItemsAsync(string tenantId, int max = 50) =>
        await GetItemsAsync(tenantId, item => item, max);

    public async Task<List<TResult>> GetItemsAsync<TResult>(string tenantId, Expression<Func<Item, TResult>> selector,
        int max = 50)
    {
        string? tenant = await _companyService.GetCompanyAsync(tenantId, company => company.Id);
        if (tenant is null) return new List<TResult>();
        return await (from item in _context.Items.AsNoTracking() where item.CompanyId == tenant select item).Take(max)
            .Select(selector).ToListAsync();
    }

    public async Task<List<Item>> GetItemsAsync(string tenantId, int count, int from) =>
        await GetItemsAsync(tenantId, item => item, count, from);

    public async Task<List<TResult>> GetItemsAsync<TResult>(string tenantId, Expression<Func<Item, TResult>> selector,
        int count, int from)
    {
        string? tenant = await _companyService.GetCompanyAsync(tenantId, company => company.Id);
        if (tenant is null) return new List<TResult>();
        return await (from item in _context.Items.AsNoTracking() where item.CompanyId == tenant select item)
            .Skip(from).Take(count).Select(selector).ToListAsync();
    }

    public async Task<Item?> GetItemAsync(string tenantId, string id) => await GetItemAsync(tenantId, id, item => item);

    public async Task<TResult?> GetItemAsync<TResult>(string tenantId, string id,
        Expression<Func<Item, TResult>> selector)
    {
        string? tenant = await _companyService.GetCompanyAsync(tenantId, company => company.Id);
        if (tenant is null) return default;
        return await (from item in _context.Items.AsNoTracking()
            where item.CompanyId == tenant && item.UId == id
            select item).Select(selector).FirstOrDefaultAsync();
    }

    public async Task<Item?> GetItemByNameAsync(string tenantId, string name) =>
        await GetItemByNameAsync(tenantId, name, item => item);

    public async Task<TResult?> GetItemByNameAsync<TResult>(string tenantId, string name,
        Expression<Func<Item, TResult>> selector)
    {
        string? tenant = await _companyService.GetCompanyAsync(tenantId, company => company.Id);
        if (tenant is null) return default;
        return await (from item in _context.Items.AsNoTracking()
            where item.CompanyId == tenant && item.Name == name
            select item).Select(selector).FirstOrDefaultAsync();
    }
    
    public async Task<List<Item>> GetItemsByTypeAsync(string tenantId, ItemType type) =>
        await GetItemsByTypeAsync(tenantId, type, item => item);

    public async Task<List<TResult>> GetItemsByTypeAsync<TResult>(string tenantId, ItemType type,
        Expression<Func<Item, TResult>> selector)
    {
        string? tenant = await _companyService.GetCompanyAsync(tenantId, company => company.Id);
        if (tenant is null) return new List<TResult>();
        return await (from item in _context.Items.AsNoTracking()
            where item.CompanyId == tenant && item.Type == type
            select item).Select(selector).ToListAsync();
    }

    public async Task<bool> UpdateItemBrandAsync(string tenancy, string id, ItemBrand brand)
    {
        string? exists = await GetItemBrandByNameAsync(tenancy, brand.Name, b => b.UId);
        if (exists is not null && exists != id) return false;
        string? tenant = await _companyService.GetCompanyAsync(tenancy, company => company.Id);
        if (tenant is null) return false;
        var itemBrand = await (from item in _context.ItemBrands.AsTracking()
            where item.CompanyId == tenant && item.UId == id
            select item).FirstOrDefaultAsync();
        if (itemBrand is null) return false;
        itemBrand.Name = brand.Name;
        itemBrand.NormalizedName = brand.Name.ToUpper();
        itemBrand.Description = brand.Description;
        return await _context.SaveChangesAsync() is not 0;
    }

    public async Task<bool> UpdateItemCategoryAsync(string tenancy, string id, ItemCategory category)
    {
        string? exists = await GetItemCategoryByNameAsync(tenancy, category.Name, c => c.UId);
        if (exists is not null && exists != id) return false;
        string? tenant = await _companyService.GetCompanyAsync(tenancy, company => company.Id);
        if (tenant is null) return false;
        var itemCategory = await (from item in _context.ItemCategories.AsTracking()
                where item.CompanyId == tenant && item.UId == id
                select item)
            .FirstOrDefaultAsync();
        if (itemCategory is null) return false;
        itemCategory.Name = category.Name;
        itemCategory.NormalizedName = category.Name.ToUpper();
        itemCategory.Description = category.Description;
        return await _context.SaveChangesAsync() is not 0;
    }

    public async Task<bool> UpdateItemManufacturerAsync(string tenancy, string id, ItemManufacturer manufacturer)
    {
        string? exists = await GetItemManufacturerByNameAsync(tenancy, manufacturer.Name, m => m.UId);
        if (exists is not null && exists != id) return false;
        string? tenant = await _companyService.GetCompanyAsync(tenancy, company => company.Id);
        if (tenant is null) return false;
        var itemManufacturer =
            await (from item in _context.ItemManufacturers.AsTracking()
                where item.CompanyId == tenant && item.UId == id
                select item).FirstOrDefaultAsync();
        if (itemManufacturer is null) return false;
        itemManufacturer.Name = manufacturer.Name;
        itemManufacturer.NormalizedName = manufacturer.Name.ToUpper();
        itemManufacturer.Description = manufacturer.Description;
        return await _context.SaveChangesAsync() is not 0;
    }

    public async Task<bool> UpdateItemUnitAsync(string tenancy, string id, ItemUnit unit)
    {
        string? exists = await GetItemUnitByNameAsync(tenancy, unit.Name, u => u.UId);
        if (exists is not null && exists != id) return false;
        string? tenant = await _companyService.GetCompanyAsync(tenancy, company => company.Id);
        if (tenant is null) return false;
        var itemUnit = await (from item in _context.ItemUnits.AsTracking()
            where item.CompanyId == tenant && item.UId == id
            select item).FirstOrDefaultAsync();
        if (itemUnit is null) return false;
        itemUnit.Name = unit.Name;
        itemUnit.NormalizedName = unit.Name.ToUpper();
        itemUnit.Description = unit.Description;
        return await _context.SaveChangesAsync() is not 0;
    }
}