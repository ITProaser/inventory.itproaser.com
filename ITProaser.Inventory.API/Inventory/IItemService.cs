﻿namespace ITProaser.Inventory.API.Inventory;

public interface IItemService
{
    Task<bool> CheckItemAsync(string tenantId, string id);

    Task<bool> CheckItemByNameAsync(string tenantId, string name);
    
    Task<bool> CheckItemBrandAsync(string tenantId, string id);
    
    Task<bool> CheckItemBrandByNameAsync(string tenantId, string name);
    
    Task<bool> CheckItemCategoryAsync(string tenantId, string id);
    
    Task<bool> CheckItemCategoryByNameAsync(string tenantId, string name);
    
    Task<bool> CheckItemManufacturerAsync(string tenantId, string id);
    
    Task<bool> CheckItemManufacturerByNameAsync(string tenantId, string name);
    
    Task<bool> CheckItemUnitAsync(string tenantId, string id);
    
    Task<bool> CheckItemUnitByNameAsync(string tenantId, string name);
    
    Task<bool> CreateBrandAsync(string tenantId, ItemBrand brand);
    
    Task<bool> CreateCategoryAsync(string tenantId, ItemCategory category);
    
    Task<bool> CreateItemAsync(string tenantId, Item item);
    
    Task<bool> CreateManufacturerAsync(string tenantId, ItemManufacturer manufacturer);
    
    Task<bool> CreateUnitAsync(string tenantId, ItemUnit unit);

    Task<ItemBrand?> GetItemBrandAsync(string tenantId, string id);
    
    Task<TResult?> GetItemBrandAsync<TResult>(string tenantId, string id, Expression<Func<ItemBrand, TResult>> selector);
    
    Task<ItemBrand?> GetItemBrandByNameAsync(string tenantId, string name);
    
    Task<TResult?> GetItemBrandByNameAsync<TResult>(string tenantId, string name, Expression<Func<ItemBrand, TResult>> selector);

    Task<List<ItemBrand>> GetItemBrandsAsync(string tenantId);
    
    Task<List<TResult>> GetItemBrandsAsync<TResult>(string tenantId, Expression<Func<ItemBrand, TResult>> selector);
    
    Task<ItemCategory?> GetItemCategoryAsync(string tenantId, string id);
    
    Task<TResult?> GetItemCategoryAsync<TResult>(string tenantId, string id, Expression<Func<ItemCategory, TResult>> selector);

    Task<ItemCategory?> GetItemCategoryByNameAsync(string tenantId, string name);
    
    Task<TResult?> GetItemCategoryByNameAsync<TResult>(string tenantId, string name, Expression<Func<ItemCategory, TResult>> selector);

    Task<List<ItemCategory>> GetItemCategoriesAsync(string tenantId);
    
    Task<List<TResult>> GetItemCategoriesAsync<TResult>(string tenantId, Expression<Func<ItemCategory, TResult>> selector);
    
    Task<ItemManufacturer?> GetItemManufacturerAsync(string tenantId, string id);
    
    Task<TResult?> GetItemManufacturerAsync<TResult>(string tenantId, string id, Expression<Func<ItemManufacturer, TResult>> selector);

    Task<ItemManufacturer?> GetItemManufacturerByNameAsync(string tenantId, string name);
    
    Task<TResult?> GetItemManufacturerByNameAsync<TResult>(string tenantId, string name, Expression<Func<ItemManufacturer, TResult>> selector);

    Task<List<ItemManufacturer>> GetItemManufacturersAsync(string tenantId);
    
    Task<List<TResult>> GetItemManufacturersAsync<TResult>(string tenantId, Expression<Func<ItemManufacturer, TResult>> selector);
    
    Task<ItemUnit?> GetItemUnitAsync(string tenantId, string id);
    
    Task<TResult?> GetItemUnitAsync<TResult>(string tenantId, string id, Expression<Func<ItemUnit, TResult>> selector);

    Task<ItemUnit?> GetItemUnitByNameAsync(string tenantId, string name);
    
    Task<TResult?> GetItemUnitByNameAsync<TResult>(string tenantId, string name, Expression<Func<ItemUnit, TResult>> selector);

    Task<List<ItemUnit>> GetItemUnitsAsync(string tenantId);
    
    Task<List<TResult>> GetItemUnitsAsync<TResult>(string tenantId, Expression<Func<ItemUnit, TResult>> selector);

    Task<List<Item>> GetItemsAsync(string tenantId, int max = 50);

    Task<List<TResult>> GetItemsAsync<TResult>(string tenantId, Expression<Func<Item, TResult>> selector, int max = 50);

    Task<List<Item>> GetItemsAsync(string tenantId, int count, int from);

    Task<List<TResult>> GetItemsAsync<TResult>(string tenantId, Expression<Func<Item, TResult>> selector, int count,
        int from);

    Task<Item?> GetItemAsync(string tenantId, string id);

    Task<TResult?> GetItemAsync<TResult>(string tenantId, string id, Expression<Func<Item, TResult>> selector);

    Task<Item?> GetItemByNameAsync(string tenantId, string name);

    Task<TResult?> GetItemByNameAsync<TResult>(string tenantId, string name, Expression<Func<Item, TResult>> selector);

    Task<List<Item>> GetItemsByTypeAsync(string tenantId, ItemType type);

    Task<List<TResult>> GetItemsByTypeAsync<TResult>(string tenantId, ItemType type,
        Expression<Func<Item, TResult>> selector);
    
    Task<bool> UpdateItemBrandAsync(string tenancy, string id, ItemBrand brand);
    
    Task<bool> UpdateItemCategoryAsync(string tenancy, string id, ItemCategory category);
    
    Task<bool> UpdateItemManufacturerAsync(string tenancy, string id, ItemManufacturer manufacturer);
    
    Task<bool> UpdateItemUnitAsync(string tenancy, string id, ItemUnit unit);
}