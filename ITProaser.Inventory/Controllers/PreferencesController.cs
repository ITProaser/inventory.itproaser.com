﻿namespace ITProaser.Inventory.Controllers;

[ApiController]
[Route("api/[controller]/[action]")]
public sealed class PreferencesController : ControllerBase
{
    private const string ResponseType = "application/json";
    private readonly IUserService _userService;

    public PreferencesController(IUserService userService)
        => _userService = userService;

    [Produces(ResponseType)]
    [HttpGet]
    public async Task<Result<bool>> Profile()
    {
        try
        {
            if (User.Identity?.Name is null || !User.Identity.IsAuthenticated)
                return new Result<bool>(ResultCode.Forbidden, "NotLoggedIn");
            var user = await _userService.GetUserByNameAsync(User.Identity.Name, selector => new
            {
                selector.UId, selector.UserName, selector.Email, selector.DisplayName, selector.FirstName,
                selector.LastName
            });
            return user?.UId is not null
                ? new Result<bool>(true, ResultCode.Continue)
                : new Result<bool>(ResultCode.NotFound, "UserNotFound");
        }
        catch (Exception e)
        {
            return new Result<bool>(ResultCode.InternalServerError, e.Message);
        }
    }
}