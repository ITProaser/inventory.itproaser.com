﻿using ITProaser.Inventory.ViewModels.Tenancy;

namespace ITProaser.Inventory.Controllers;

[ApiController]
[Route("api/[controller]/[action]")]
public sealed class TenancyController : ControllerBase
{
    private const string ResponseType = "application/json";
    private readonly ICompanyService _companyService;
    private readonly IUserService _userService;

    public TenancyController(ICompanyService companyService, IUserService userservice)
        => (_companyService, _userService) = (companyService, userservice);

    [Produces(ResponseType)]
    [HttpGet]
    public async Task<Result<List<CurrencyProfile>>> Currencies()
    {
        try
        {
            if (User.Identity?.Name is null || !User.Identity.IsAuthenticated)
                return new Result<List<CurrencyProfile>>(ResultCode.Unauthorized, "userNotAuthenticated");
            string? tenancy = await _userService.GetTenantAsync(User.Identity.Name);
            if (tenancy is null) return new Result<List<CurrencyProfile>>("/company/register");
            return new Result<List<CurrencyProfile>>(
                await _companyService.GetCurrenciesAsync(tenancy, c => new CurrencyProfile(c)
            ), ResultCode.NoContent);
        }
        catch (Exception e)
        {
            return new Result<List<CurrencyProfile>>(ResultCode.InternalServerError, e.Message);
        }
    }

    [Produces(ResponseType)]
    [HttpGet]
    public async Task<Result<List<TaxProfile>>> Taxes()
    {
        try
        {
            if (User.Identity?.Name is null || !User.Identity.IsAuthenticated)
                return new Result<List<TaxProfile>>(ResultCode.Unauthorized, "userNotAuthenticated");
            string? tenancy = await _userService.GetTenantAsync(User.Identity.Name);
            if (tenancy is null) return new Result<List<TaxProfile>>("/company/register");
            return new Result<List<TaxProfile>>(
                await _companyService.GetTaxesAsync(tenancy, t => new TaxProfile(t)
            ), ResultCode.NoContent);
        }
        catch (Exception e)
        {
            return new Result<List<TaxProfile>>(ResultCode.InternalServerError, e.Message);
        }
    }

    [Produces(ResponseType)]
    [HttpGet]
    public async Task<Result<List<PaymentTermProfile>>> PaymentTerms()
    {
        try
        {
            if (User.Identity?.Name is null || !User.Identity.IsAuthenticated)
                return new Result<List<PaymentTermProfile>>(ResultCode.Unauthorized, "userNotAuthenticated");
            string? tenancy = await _userService.GetTenantAsync(User.Identity.Name);
            if (tenancy is null) return new Result<List<PaymentTermProfile>>("/company/register");
            return new Result<List<PaymentTermProfile>>(
                await _companyService.GetPaymentTermsAsync(tenancy, p => new PaymentTermProfile(p)
            ), ResultCode.NoContent);
        }
        catch (Exception e)
        {
            return new Result<List<PaymentTermProfile>>(ResultCode.InternalServerError, e.Message);
        }
    }
}