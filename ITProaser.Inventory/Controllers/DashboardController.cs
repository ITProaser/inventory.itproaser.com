﻿namespace ITProaser.Inventory.Controllers;

[ApiController]
[Route("api/[controller]")]
public sealed class DashboardController : ControllerBase
{
    private const string ResponseType = "application/json";
    private readonly IUserService _userService;

    public DashboardController(IUserService userService) => _userService = userService;

    [Produces(ResponseType)]
    [HttpGet]
    [Route("")]
    public async Task<Result<bool>> Get()
    {
        try
        {
            if (User.Identity?.Name is null || !User.Identity.IsAuthenticated)
                return new Result<bool>(ResultCode.Forbidden, "NotLoggedIn");
            string? userId = await _userService.GetUserByNameAsync(User.Identity.Name, selector => selector.UId);
            return userId is null ? new Result<bool>(ResultCode.BadRequest, "UserNotFound") : new Result<bool>(true, ResultCode.Continue);
        }
        catch (Exception e)
        {
            return new Result<bool>(ResultCode.InternalServerError, e.Message);
        }
    }
}