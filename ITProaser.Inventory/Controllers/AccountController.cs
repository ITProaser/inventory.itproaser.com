﻿using Microsoft.AspNetCore.Authorization;

namespace ITProaser.Inventory.Controllers;

[ApiController]
[Route("api/[controller]/[action]")]
public sealed class AccountController : ControllerBase
{
    private const string ResponseType = "application/json";
    private readonly SignInManager<User> _signInManager;
    
    public AccountController(SignInManager<User> signInManager) => _signInManager = signInManager;

    [Produces(ResponseType)]
    [HttpGet]
    [AllowAnonymous]
    public Result<bool> Login()
    {
        try
        {
            return User.Identity?.Name is not null && User.Identity.IsAuthenticated
                ? new Result<bool>("/")
                : new Result<bool>(true, ResultCode.Continue);
        }
        catch (Exception e)
        {
            return new Result<bool>(ResultCode.InternalServerError, e.Message);
        }
    }

    [Produces(ResponseType)]
    [HttpGet]
    [AllowAnonymous]
    public async Task<Result<bool>> Logout()
    {
        try
        {
            if (!_signInManager.IsSignedIn(User) || User.Identity?.Name is null)
                return new Result<bool>(ResultCode.Unauthorized);
            await _signInManager.SignOutAsync();
            return new Result<bool>(true, ResultCode.Continue);
        }
        catch (Exception e)
        {
            return new Result<bool>(ResultCode.InternalServerError, e.Message);
        }
    }
}