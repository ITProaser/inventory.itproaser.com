﻿using ITProaser.Inventory.ViewModels.Content;
using Microsoft.AspNetCore.Authorization;

namespace ITProaser.Inventory.Controllers;

[ApiController]
[Route("api/[controller]/[action]")]
public sealed class ContentController : ControllerBase
{
    private const string ResponseType = "application/json";
    private readonly IUserService _userService;
    private readonly ICompanyService _companyService;

    public ContentController(IUserService userService, ICompanyService companyService)
        => (_userService, _companyService) = (userService, companyService);

    [Produces(ResponseType)]
    [HttpGet]
    public async Task<Result<UserProfile>> Profile()
    {
        try
        {
            
            if (User.Identity?.Name is null) return new Result<UserProfile>(ResultCode.BadRequest, "User is not authenticated");
            var user = await _userService.GetUserByNameAsync(User.Identity.Name);
            if (user is null)
                return new Result<UserProfile>(ResultCode.Forbidden, "NotLoggedIn");
            return new Result<UserProfile>(
                new UserProfile(
                    user.UId, user.UserName, user.Email, user.DisplayName, user.FirstName,
                    user.LastName, user.CompanyId?.ToUId(),
                    await _userService.GetUserClaimsAsync(user.UId)
                ), ResultCode.Ok);
        }
        catch (Exception e)
        {
            return new Result<UserProfile>(ResultCode.InternalServerError, e.Message);
        }
    }

    [Produces(ResponseType)]
    [HttpGet]
    public async Task<Result<CompanyProfile>> Company()
    {
        try
        {
            if (User.Identity?.Name is null || !User.Identity.IsAuthenticated)
                return new Result<CompanyProfile>(ResultCode.Unauthorized, "userNotAuthenticated");
            string? tenancy = await _userService.GetTenantAsync(User.Identity.Name);
            if (tenancy is null) return new Result<CompanyProfile>("/company/register");
            var address = await _companyService.GetCompanyAddressAsync(tenancy, address => new AddressProfile(
                address.Country, address.State, address.City, address.ZipCode, address.Street
            ));
            var company = await _companyService.GetCompanyAsync(tenancy, profile => new CompanyProfile(
                profile.UId,
                profile.Name,
                profile.Industry,
                profile.CompanyId,
                address
            ));
            return company is null
                ? new Result<CompanyProfile>("/company/register")
                : new Result<CompanyProfile>(company, ResultCode.NoContent);
        }
        catch (Exception e)
        {
            return new Result<CompanyProfile>(ResultCode.InternalServerError, e.Message);
        }
    }

    [Produces(ResponseType)]
    [HttpGet]
    [AllowAnonymous]
    public async Task<Result<List<ClientRoute>>> Routes()
    {
        try
        {
            if (User.Identity?.Name is null || !User.Identity.IsAuthenticated)
                return new Result<List<ClientRoute>>(ResultCode.Forbidden, "NotLoggedIn");
            var routes = new List<ClientRoute>();
            if (await _userService.CheckPermissionAsync(User.Identity.Name, "items.read"))
                routes.Add(new ClientRoute("inventory", "cart-outline", "/", true, "inventory", new List<ClientRoute>
                {
                    new("items", "file-document-outline", "/items", true),
                    new("itemGroups", "folder-multiple-outline", "/item-groups", true)
                }));
            if (await _userService.CheckPermissionAsync(User.Identity.Name, "vendors.read"))
                routes.Add(new ClientRoute("vendors", "store-outline", "/purchases/vendors", true, "purchases"));
            if (await _userService.CheckPermissionAsync(User.Identity.Name, "purchase_orders.read"))
                routes.Add(new ClientRoute("purchase_orders", "cart-outline", "/purchases/orders", true, "purchases"));
            if (await _userService.CheckPermissionAsync(User.Identity.Name, "customers.read"))
                routes.Add(new ClientRoute("customers", "badge-account-horizontal-outline", "/sales/customers", true, "sales"));
            if (await _userService.CheckPermissionAsync(User.Identity.Name, "sales_orders.read"))
                routes.Add(new ClientRoute("sales_orders", "credit-card-outline", "/sales/orders", true, "sales"));
            var settings = new List<ClientRoute>();
            if (await _userService.CheckPermissionAsync(User.Identity.Name, "item_brands.read"))
                settings.Add(new ClientRoute("itemBrands", "tag-multiple-outline", "/item-brands", true));
            if (await _userService.CheckPermissionAsync(User.Identity.Name, "item_categories.read"))
                settings.Add(new ClientRoute("itemCategories", "shape-outline", "/item-categories", true));
            if (await _userService.CheckPermissionAsync(User.Identity.Name, "item_manufacturers.read"))
                settings.Add(new ClientRoute("itemManufacturers", "package-variant-closed", "/item-manufacturers", true));
            if (await _userService.CheckPermissionAsync(User.Identity.Name, "item_units.read"))
                settings.Add(new ClientRoute("itemUnits", "chart-box-outline", "/item-units", true));
            if (settings.Any())
                routes.Add(new ClientRoute("settings", "tune", "/settings", true, "settings", settings));
            return new Result<List<ClientRoute>>(routes, ResultCode.NoContent);
        }
        catch (Exception e)
        {
            return new Result<List<ClientRoute>>(ResultCode.InternalServerError, e.Message);
        }
    }
}