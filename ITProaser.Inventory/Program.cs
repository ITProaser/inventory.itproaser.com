using System.Globalization;
using System.Reflection;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);
string connectionString = builder.Configuration.GetConnectionString("DefaultConnection");
string? authority = builder.Configuration.GetValue<string>($"{nameof(LocalSettings)}:{nameof(LocalSettings.Authority)}");
string? clientUri = builder.Configuration.GetValue<string>($"{nameof(LocalSettings)}:{nameof(LocalSettings.ClientUri)}");
string? databaseEngine = builder.Configuration.GetValue<string>($"{nameof(LocalSettings)}:{nameof(LocalSettings.DatabaseEngine)}");
string assembly = typeof(Program).Assembly.GetName().FullName;

var localizationOptions = new Action<RequestLocalizationOptions>(options =>
{
    var supportedCultures = new List<CultureInfo> { new("en"), new("es") };
    options.SetDefaultCulture("en");
    options.ApplyCurrentCultureToResponseHeaders = true;
    options.SupportedCultures = supportedCultures;
    options.SupportedUICultures = supportedCultures;
}); 

// Localization service
builder.Services.AddRequestLocalization(localizationOptions)
    .AddLocalization(action => action.ResourcesPath = "Resources");
builder.Services.AddAntiforgery(options => options.HeaderName = "X-CSRF-TOKEN");

builder.Services.AddDbContext<InventoryDbContext>(options => _ = databaseEngine switch
        {
            DatabaseEngine.SqlServer => options.UseSqlServer(connectionString, sql => sql.MigrationsAssembly(assembly)),
            DatabaseEngine.Postgres => options.UseNpgsql(connectionString, pg => pg.MigrationsAssembly(assembly)),
            _ => throw new NotSupportedException($"Database engine {databaseEngine} is not supported.")
        }
    )
    .AddDataProtection().SetApplicationName("ITProaser").PersistKeysToDbContext<InventoryDbContext>();

builder.Services.AddIdentity<User, Role>(options =>
{
    options.User.RequireUniqueEmail = true;
    options.SignIn.RequireConfirmedEmail = true;
}).AddDefaultTokenProviders().AddEntityFrameworkStores<InventoryDbContext>();

builder.Services.AddAuthentication(options =>
    {
        options.DefaultScheme = CookieAuthenticationDefaults.AuthenticationScheme;
        options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
    })
    .AddJwtBearer(options =>
    {
        options.Authority = authority;
        options.TokenValidationParameters.ValidateAudience = false;
    });

builder.Services.ConfigureApplicationCookie(cookie =>
{
    cookie.Cookie.Name = "Authorization-Cookie";
    cookie.Cookie.SameSite = SameSiteMode.Strict;
    cookie.Cookie.SecurePolicy = CookieSecurePolicy.Always;
    cookie.Cookie.HttpOnly = true;
    cookie.Cookie.IsEssential = true;
    cookie.ExpireTimeSpan = TimeSpan.FromHours(2);
});

builder.Services.AddAuthorization(options =>
{
    options.AddPolicy("tenancy", policy => policy.RequireAuthenticatedUser().RequireClaim("tenancy"));
    options.AddPolicy("root", policy => policy.RequireAuthenticatedUser().RequireClaim("root"));
    options.AddPolicy("admin", policy => policy.RequireAuthenticatedUser().RequireClaim("admin"));
});

builder.Services.AddControllers();

builder.Services.AddCors(cors => cors.AddDefaultPolicy(policy
    => policy.WithOrigins(clientUri).WithMethods(HttpMethods.Get, HttpMethods.Post, HttpMethods.Put, HttpMethods.Delete)
        .WithHeaders("Accept-Language").AllowCredentials())).AddResponseCompression();

builder.Services.AddScoped<IProductService, ProductService<InventoryDbContext>>();
builder.Services.AddScoped<IUserService, UserService<InventoryDbContext>>();
builder.Services.AddScoped<IRoleService, RoleService<InventoryDbContext>>();
builder.Services.AddScoped<ICompanyService, CompanyService<InventoryDbContext>>();
builder.Services.AddScoped<IItemService, ItemService<InventoryDbContext>>();
builder.Services.AddScoped<IVendorService, VendorService<InventoryDbContext>>();
builder.Services.AddScoped<ICustomerService, CustomerService<InventoryDbContext>>();
builder.Services.AddScoped<IPurchaseOrdersService, PurchaseOrdersService<InventoryDbContext>>();
builder.Services.AddScoped<ISaleOrdersService, SaleOrdersService<InventoryDbContext>>();

// AppSettings
builder.Services.Configure<LocalSettings>(builder.Configuration.GetSection(nameof(LocalSettings)));

builder.Services.AddSpaStaticFiles(options => options.RootPath = "Client/dist");

var app = builder.Build();

app.UseRequestLocalization(localizationOptions);
if (app.Environment.IsDevelopment())
    app.UseDeveloperExceptionPage();
else app.UseHsts();

app.UseStaticFiles();
app.UseSpaStaticFiles();
app.UseHttpsRedirection();
app.UseResponseCompression();
app.UseAuthentication();
app.UseRouting();
app.UseAuthorization();

app.UseCors();

app.MapGet("/api/version", () =>
    new {
        Assembly.GetEntryAssembly()?.GetName().Version,
        Assembly.GetEntryAssembly()?.GetCustomAttribute<AssemblyInformationalVersionAttribute>()?.InformationalVersion
    }
).RequireCors(policy => policy.WithOrigins("itproaser.com").WithMethods(HttpMethods.Get).DisallowCredentials());

app.UseEndpoints(endpoints => endpoints.MapDefaultControllerRoute());

app.UseSpa(spa =>
{
    spa.Options.DevServerPort = 3014;
    spa.Options.SourcePath = "Client";
    if (app.Environment.IsDevelopment())
        spa.UseProxyToSpaDevelopmentServer("http://localhost:3014");
});

app.Run();