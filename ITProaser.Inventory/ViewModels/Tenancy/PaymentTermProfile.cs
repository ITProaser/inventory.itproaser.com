﻿namespace ITProaser.Inventory.ViewModels.Tenancy;

public record PaymentTermProfile(
    string Id,
    string Name,
    int Days
)
{
    public PaymentTermProfile(PaymentTerm paymentTerm) : this(paymentTerm.UId, paymentTerm.Label, paymentTerm.Days)
    {
    }
}