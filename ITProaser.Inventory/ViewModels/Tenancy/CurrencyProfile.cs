﻿namespace ITProaser.Inventory.ViewModels.Tenancy;

public record CurrencyProfile(
    string Id,
    string Name,
    string Code,
    string Symbol,
    int DecimalPlaces,
    bool IsDefault
)
{
    public CurrencyProfile(Currency currency) : this(currency.UId, currency.Name,
        currency.Code, currency.Symbol, currency.DecimalPlaces, currency.IsDefault) { }
};
