﻿namespace ITProaser.Inventory.ViewModels.Tenancy;

public record TaxProfile(
    string Id,
    string Name,
    decimal Rate,
    bool IsCompound
)
{
    public TaxProfile(Tax tax) : this(tax.UId, tax.Name, tax.Rate, tax.IsCompound) { }
}