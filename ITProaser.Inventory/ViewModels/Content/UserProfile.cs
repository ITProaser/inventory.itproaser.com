﻿using System.Security.Claims;

namespace ITProaser.Inventory.ViewModels.Content;

public record UserProfile(
    string Id,
    string UserName,
    string Email,
    string DisplayName,
    string? FirstName,
    string LastName,
    string? Tenant,
    List<Claim> Claims
);