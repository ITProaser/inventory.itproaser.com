﻿namespace ITProaser.Inventory.ViewModels.Content;

public record CompanyProfile(
    string Id,
    string Name,
    string Industry,
    string? CompanyId,
    AddressProfile? Address
);
