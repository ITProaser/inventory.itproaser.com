﻿namespace ITProaser.Inventory.ViewModels.Content;

public record ClientRoute(
    string Label,
    
    string Icon,
    
    string Path,
    
    bool Sidebar = false,
    
    string? Group = null,
    
    List<ClientRoute>? Children = null
);