﻿using System.ComponentModel.DataAnnotations;

namespace ITProaser.Inventory.ViewModels.Content;

public record AddressProfile(
    [Required] [MaxLength(32)] string? Country,

    [MaxLength(32)] string? State,

    [MaxLength(32)] string? City,

    [MaxLength(12)] string? ZipCode,

    [MaxLength(128)] string? Street
)
{
    public AddressProfile() : this(string.Empty, string.Empty, string.Empty, string.Empty, string.Empty) { }
    public AddressProfile(Address address) : this(address.Country ?? string.Empty, address.State, address.City, address.ZipCode, address.Street) { }
    public Address ToAddress() => new ()
    {
        Country = Country,
        State = State,
        City = City,
        ZipCode = ZipCode,
        Street = Street
    };
};
