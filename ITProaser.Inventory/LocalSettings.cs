﻿using System.ComponentModel.DataAnnotations;

namespace ITProaser.Inventory;

public sealed class LocalSettings
{
    [Required]
    [Url]
    public string Authority { get; set; } = string.Empty;
    
    [Required]
    public string ClientId { get; set; } = string.Empty;
    
    [Required]
    [Url]
    public string ClientUri { get; set; } = string.Empty;
    
    [Required]
    public string DatabaseEngine { get; set; } = null!;
}

public record DatabaseEngine
{
    public const string SqlServer = "mssql";
    
    public const string Postgres = "postgres";
}