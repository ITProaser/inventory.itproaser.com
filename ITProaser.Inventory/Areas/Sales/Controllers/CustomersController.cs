﻿using ITProaser.Inventory.Areas.Sales.ViewModels.Customers;

namespace ITProaser.Inventory.Areas.Sales.Controllers;

[Area("Sales")]
[ApiController]
[Route("api/[area]/[controller]/[action]")]
public sealed class CustomersController : ControllerBase
{
    private const string ResponseType = "application/json";
    private readonly IUserService _userService;
    private readonly ICustomerService _customerService;
    private readonly ICompanyService _companyService;

    public CustomersController(IUserService userService, ICustomerService customerService,
        ICompanyService companyService)
        => (_userService, _customerService, _companyService) = (userService, customerService, companyService);

    [Produces(ResponseType)]
    [HttpGet]
    public async Task<Result<List<CustomerProfile>>> Download()
    {
        try
        {
            if (User.Identity?.Name is null || !User.Identity.IsAuthenticated)
                return new Result<List<CustomerProfile>>(ResultCode.Unauthorized, "userNotAuthenticated");
            string? tenancy = await _userService.GetTenantAsync(User.Identity.Name);
            if (tenancy is null)
                return new Result<List<CustomerProfile>>(ResultCode.BadRequest, "companyNotRegistered");
            return new Result<List<CustomerProfile>>(
                await _customerService.GetCustomersAsync(tenancy, c => new CustomerProfile(c)),
                ResultCode.NoContent
            );
        }
        catch (Exception e)
        {
            return new Result<List<CustomerProfile>>(ResultCode.InternalServerError, e.Message);
        }
    }

    [Produces(ResponseType)]
    [HttpGet]
    public async Task<Result<bool>> List()
    {
        try
        {
            if (User.Identity?.Name is null || !User.Identity.IsAuthenticated)
                return new Result<bool>(ResultCode.Unauthorized, "userNotAuthenticated");
            if (!await _userService.CheckPermissionAsync(User.Identity.Name, "customers.read"))
                return new Result<bool>(ResultCode.Forbidden, "userNotAuthorized");
            string? tenancy = await _userService.GetTenantAsync(User.Identity.Name);
            return tenancy is null
                ? new Result<bool>(ResultCode.BadRequest, "companyNotRegistered")
                : new Result<bool>(true, ResultCode.Continue);
        }
        catch (Exception e)
        {
            return new Result<bool>(ResultCode.InternalServerError, e.Message);
        }
    }

    [Produces(ResponseType)]
    [HttpGet]
    public async Task<Result<bool>> Create()
    {
        try
        {
            if (User.Identity?.Name is null || !User.Identity.IsAuthenticated)
                return new Result<bool>(ResultCode.Unauthorized, "userNotAuthenticated");
            string? tenancy = await _userService.GetTenantAsync(User.Identity.Name);
            if (tenancy is null)
                return new Result<bool>(ResultCode.BadRequest, "companyNotRegistered");
            return await _userService.CheckPermissionAsync(User.Identity.Name, "customers.create")
                ? new Result<bool>(true, ResultCode.Continue)
                : new Result<bool>(ResultCode.Forbidden, "userNotAuthorized");
        }
        catch (Exception e)
        {
            return new Result<bool>(ResultCode.InternalServerError, e.Message);
        }
    }

    [Produces(ResponseType)]
    [HttpGet]
    [Route("{id}")]
    public async Task<Result<bool>> Edit([FromRoute] string? id)
    {
        try
        {
            if (User.Identity?.Name is null || !User.Identity.IsAuthenticated)
                return new Result<bool>(ResultCode.Unauthorized, "userNotAuthenticated");
            string? tenancy = await _userService.GetTenantAsync(User.Identity.Name);
            if (tenancy is null)
                return new Result<bool>(ResultCode.BadRequest, "companyNotRegistered");
            return await _userService.CheckPermissionAsync(User.Identity.Name, "customers.edit")
                ? new Result<bool>(true, ResultCode.Continue)
                : new Result<bool>(ResultCode.Forbidden, "userNotAuthorized");
        }
        catch (Exception e)
        {
            return new Result<bool>(ResultCode.InternalServerError, e.Message);
        }
    }

    [Produces(ResponseType)]
    [HttpPost]
    public async Task<Result<CustomerProfile>> Create([FromForm] CustomerCreate model)
    {
        try
        {
            if (User.Identity?.Name is null || !User.Identity.IsAuthenticated)
                return new Result<CustomerProfile>(ResultCode.Unauthorized, "userNotAuthenticated");
            string? tenancy = await _userService.GetTenantAsync(User.Identity.Name);
            if (tenancy is null)
                return new Result<CustomerProfile>(ResultCode.BadRequest, "companyNotRegistered");
            if (!await _userService.CheckPermissionAsync(User.Identity.Name, "customers.create"))
                return new Result<CustomerProfile>(ResultCode.Forbidden, "userNotAuthorized");
            string? currencyId = await _companyService.GetCurrencyAsync(tenancy, model.CurrencyId, c => c.Id);
            if (currencyId is null)
                return new Result<CustomerProfile>(ResultCode.BadRequest, "currencyNotFound");
            string? userId = await _userService.GetUserByNameAsync(User.Identity.Name, u => u.Id);
            if (userId is null)
                return new Result<CustomerProfile>(ResultCode.BadRequest, "userNotFound");
            string? taxId = model.TaxId is null
                ? null
                : await _companyService.GetTaxAsync(tenancy, model.TaxId, t => t.Id);
            string? paymentTermId = model.PaymentTermId is null
                ? null
                : await _companyService.GetPaymentTermAsync(tenancy, model.PaymentTermId, p => p.Id);
            var billingAddress = model.BillingAddress is null
                ? null
                : new Address
                {
                    Country = model.BillingAddress?.Country,
                    City = model.BillingAddress?.City,
                    Street = model.BillingAddress?.Street,
                    ZipCode = model.BillingAddress?.ZipCode,
                    State = model.BillingAddress?.State
                };
            var shippingAddress = billingAddress is not null && model.BillingIsShipping ? billingAddress :
                model.ShippingAddress is null ? null : new Address
                {
                    Country = model.ShippingAddress?.Country,
                    City = model.ShippingAddress?.City,
                    Street = model.ShippingAddress?.Street,
                    ZipCode = model.ShippingAddress?.ZipCode,
                    State = model.ShippingAddress?.State
                };
            var customer = new Customer
            {
                FirstName = model.FirstName, LastName = model.LastName, DisplayName = model.DisplayName,
                CompanyName = model.CompanyName ?? string.Empty,
                Email = model.Email, Phone = model.Phone, Website = model.Website, Remarks = model.Remarks,
                CurrencyId = currencyId, Type = model.Type,
                OwnerId = userId, BillingAddress = billingAddress, ShippingAddress = shippingAddress, TaxId = taxId,
                PaymentTermId = paymentTermId
            };
            return await _customerService.AddCustomerAsync(tenancy, customer)
                ? new Result<CustomerProfile>(new CustomerProfile(customer)
                {
                    CurrencyId = model.CurrencyId,
                    TaxId = model.TaxId,
                    PaymentTermId = model.PaymentTermId
                }, ResultCode.Created)
                : new Result<CustomerProfile>(ResultCode.BadRequest, "customerNotCreated");
        }
        catch (Exception e)
        {
            return new Result<CustomerProfile>(ResultCode.InternalServerError, e.Message);
        }
    }

    [Produces(ResponseType)]
    [HttpPut]
    [Route("{id}")]
    public async Task<Result<CustomerProfile>> Edit([FromRoute] string? id, [FromForm] CustomerCreate model)
    {
        try
        {
            if (id is null)
                return new Result<CustomerProfile>(ResultCode.BadRequest, "idNotProvided");
            if (User.Identity?.Name is null || !User.Identity.IsAuthenticated)
                return new Result<CustomerProfile>(ResultCode.Unauthorized, "userNotAuthenticated");
            string? tenancy = await _userService.GetTenantAsync(User.Identity.Name);
            if (tenancy is null)
                return new Result<CustomerProfile>(ResultCode.BadRequest, "companyNotRegistered");
            if (!await _userService.CheckPermissionAsync(User.Identity.Name, "customers.edit"))
                return new Result<CustomerProfile>(ResultCode.Forbidden, "userNotAuthorized");
            string? currencyId = await _companyService.GetCurrencyAsync(tenancy, model.CurrencyId, c => c.Id);
            if (currencyId is null)
                return new Result<CustomerProfile>(ResultCode.BadRequest, "currencyNotFound");
            string? userId = await _userService.GetUserByNameAsync(User.Identity.Name, u => u.Id);
            if (userId is null)
                return new Result<CustomerProfile>(ResultCode.BadRequest, "userNotFound");
            string? taxId = model.TaxId is null
                ? null
                : await _companyService.GetTaxAsync(tenancy, model.TaxId, t => t.Id);
            string? paymentTermId = model.PaymentTermId is null
                ? null
                : await _companyService.GetPaymentTermAsync(tenancy, model.PaymentTermId, p => p.Id);
            var billingAddress = model.BillingAddress is null
                ? null
                : new Address
                {
                    Country = model.BillingAddress?.Country,
                    City = model.BillingAddress?.City,
                    Street = model.BillingAddress?.Street,
                    ZipCode = model.BillingAddress?.ZipCode,
                    State = model.BillingAddress?.State
                };
            var shippingAddress = billingAddress is not null && model.BillingIsShipping ? billingAddress :
                model.ShippingAddress is null ? null : new Address
                {
                    Country = model.ShippingAddress?.Country,
                    City = model.ShippingAddress?.City,
                    Street = model.ShippingAddress?.Street,
                    ZipCode = model.ShippingAddress?.ZipCode,
                    State = model.ShippingAddress?.State
                };
            var customer = await _customerService.GetCustomerAsync(tenancy, id);
            if (customer is null)
                return new Result<CustomerProfile>(ResultCode.BadRequest, "customerNotFound");
            customer.Type = model.Type;
            customer.FirstName = model.FirstName;
            customer.LastName = model.LastName;
            customer.DisplayName = model.DisplayName;
            customer.CompanyName = model.CompanyName ?? string.Empty;
            customer.Email = model.Email;
            customer.Mobile = model.Mobile;
            customer.Phone = model.Phone;
            customer.Website = model.Website;
            customer.Remarks = model.Remarks;
            customer.CurrencyId = currencyId;
            customer.TaxId = taxId;
            customer.PaymentTermId = paymentTermId;
            customer.OwnerId = userId;
            if (customer.BillingAddress is not null)
            {
                customer.BillingAddress.Country = model.BillingAddress?.Country;
                customer.BillingAddress.City = model.BillingAddress?.City;
                customer.BillingAddress.Street = model.BillingAddress?.Street;
                customer.BillingAddress.ZipCode = model.BillingAddress?.ZipCode;
                customer.BillingAddress.State = model.BillingAddress?.State;
            }
            else customer.BillingAddress = billingAddress;
            if (customer.ShippingAddress is not null)
            {
                customer.ShippingAddress.Country = model.ShippingAddress?.Country;
                customer.ShippingAddress.City = model.ShippingAddress?.City;
                customer.ShippingAddress.Street = model.ShippingAddress?.Street;
                customer.ShippingAddress.ZipCode = model.ShippingAddress?.ZipCode;
                customer.ShippingAddress.State = model.ShippingAddress?.State;
            }
            else customer.ShippingAddress = shippingAddress;
            return await _customerService.UpdateCustomerAsync(tenancy, id, customer)
                ? new Result<CustomerProfile>(new CustomerProfile(customer)
                {
                    CurrencyId = model.CurrencyId,
                    TaxId = model.TaxId,
                    PaymentTermId = model.PaymentTermId
                }, ResultCode.NoContent)
                : new Result<CustomerProfile>(ResultCode.BadRequest, "customerNotUpdated");
        }
        catch (Exception e)
        {
            return new Result<CustomerProfile>(ResultCode.InternalServerError, e.Message);
        }
    }
}