﻿using System.ComponentModel.DataAnnotations;
using ITProaser.Inventory.ViewModels.Content;

namespace ITProaser.Inventory.Areas.Sales.ViewModels.Customers;

public record CustomerCreate(
    [Required]
    CustomerType Type,
    
    [MaxLength(64)]
    string? FirstName,
    
    [MaxLength(64)]
    [Required]
    string LastName,
    
    [MaxLength(64)]
    [Required]
    string DisplayName,
    
    [MaxLength(64)]
    string? CompanyName,
    
    [MaxLength(64)]
    [EmailAddress]
    string? Email,
    
    [MaxLength(64)]
    string? Title,
    
    [MaxLength(64)]
    [Phone]
    string? Phone,
    
    [MaxLength(64)]
    [Phone]
    string? Mobile,
    
    [MaxLength(64)]
    [Url]
    string? Website,
    
    [MaxLength(8)]
    [Required]
    string CurrencyId,

    [MaxLength(8)]
    string? TaxId,
    
    [MaxLength(8)]
    string? PaymentTermId,
    
    [MaxLength(255)]
    string? Remarks,
    
    AddressProfile? BillingAddress,
    
    AddressProfile? ShippingAddress,
    
    bool BillingIsShipping
);