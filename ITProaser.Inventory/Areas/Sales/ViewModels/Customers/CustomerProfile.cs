﻿using ITProaser.Inventory.ViewModels.Content;

namespace ITProaser.Inventory.Areas.Sales.ViewModels.Customers;

public record CustomerProfile(
    CustomerType Type,
    string Id,
    string? FirstName,
    string LastName,
    string DisplayName,
    string? Email,
    string? CompanyName,
    string? MobilePhone,
    string? WorkPhone,
    string? Website,
    string? Title,
    string? Remarks,
    AddressProfile? BillingAddress,
    AddressProfile? ShippingAddress
)
{
    public CustomerProfile(Customer customer) : this(
        customer.Type,
        customer.UId,
        customer.FirstName,
        customer.LastName,
        customer.DisplayName,
        customer.Email,
        customer.CompanyName,
        customer.Mobile,
        customer.Phone,
        customer.Website,
        customer.Title,
        customer.Remarks,
        customer.BillingAddress is null ? null : new AddressProfile(customer.BillingAddress),
        customer.ShippingAddress is null ? null : new AddressProfile(customer.ShippingAddress)
    )
    {
        CurrencyId = customer.Currency?.UId ?? string.Empty;
        TaxId = customer.Tax?.UId;
        PaymentTermId = customer.PaymentTerm?.UId;
    }
    
    public string CurrencyId { get; set; } = null!;
    
    public string? TaxId { get; set; }
    
    public string? PaymentTermId { get; set; }
}