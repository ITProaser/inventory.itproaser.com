﻿using System.ComponentModel.DataAnnotations;

namespace ITProaser.Inventory.Areas.Inventory.ViewModels.ItemManufacturers;

public record ManufacturerProfile(
    [Required]
    [MaxLength(32)]
    string Name,
    
    [MaxLength(64)]
    string? Description
);