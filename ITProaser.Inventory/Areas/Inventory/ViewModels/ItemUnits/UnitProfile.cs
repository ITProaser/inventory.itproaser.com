﻿using System.ComponentModel.DataAnnotations;

namespace ITProaser.Inventory.Areas.Inventory.ViewModels.ItemUnits;

public record UnitProfile(
    [Required]
    [MaxLength(8)]
    string Name,
    
    [MaxLength(128)]
    string? Description
);