﻿using System.ComponentModel.DataAnnotations;

namespace ITProaser.Inventory.Areas.Inventory.ViewModels.ItemBrands;

public record BrandProfile(
    [Required]
    [MaxLength(32)]
    string Name,
    
    [MaxLength(64)]
    string? Description
);