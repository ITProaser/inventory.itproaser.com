﻿using System.ComponentModel.DataAnnotations;

namespace ITProaser.Inventory.Areas.Inventory.ViewModels.Items;

public record CreateItemViewModel(
    [Required]
    ItemType Type,
    
    [Required]
    [MaxLength(64)]
    string Name,
    
    [MaxLength(32)]
    string? Sku,
    
    [MaxLength(8)]
    string? CategoryId,
    
    [MaxLength(8)]
    string? UnitId,
    
    [MaxLength(8)]
    string? ManufacturerId,
    
    [MaxLength(8)]
    string? BrandId,
    
    [MaxLength(12)]
    string? UniversalProductCode,
    
    [MaxLength(13)]
    string? InternationalArticleNumber,
    
    [MaxLength(64)]
    string? ManufacturerPartNumber,
    
    [MaxLength(64)]
    string? InternationalStandardBookNumber,
    
    [Required]
    bool IsReturnable,
    
    bool IncludePurchaseInformation,
    
    bool IncludeSalesInformation,
    
    [MaxLength(8)]
    string? PurchaseCurrencyId,
    
    decimal? PurchaseRate,
    
    [MaxLength(8)]
    string? PurchaseTaxId,
    
    [MaxLength(128)]
    string? PurchaseDescription,
    
    [MaxLength(8)]
    string? SalesCurrencyId,
    
    decimal? SalesRate,
    
    [MaxLength(8)]
    string? SalesTaxId,
    
    [MaxLength(128)]
    string? SalesDescription
);