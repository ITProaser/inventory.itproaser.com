﻿using System.ComponentModel.DataAnnotations;

namespace ITProaser.Inventory.Areas.Inventory.ViewModels.ItemGroups;

public record CreateItemGroup(
    [Required]
    [MaxLength(32)]
    string Name
);