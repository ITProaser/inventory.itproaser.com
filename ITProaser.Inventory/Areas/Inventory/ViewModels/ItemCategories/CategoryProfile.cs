﻿using System.ComponentModel.DataAnnotations;

namespace ITProaser.Inventory.Areas.Inventory.ViewModels.ItemCategories;

public record CategoryProfile(
    [Required]
    [MaxLength(32)]
    string Name,
    
    [MaxLength(64)]
    string? Description
);