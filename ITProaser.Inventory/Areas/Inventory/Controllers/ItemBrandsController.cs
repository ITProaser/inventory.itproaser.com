﻿using ITProaser.Inventory.Areas.Inventory.ViewModels.ItemBrands;

namespace ITProaser.Inventory.Areas.Inventory.Controllers;

[Area("Inventory")]
[ApiController]
[Route("api/[area]/item-brands/[action]")]
public sealed class ItemBrandsController : ControllerBase
{
    private const string ResponseType = "application/json";
    private readonly IUserService _userService;
    private readonly IItemService _itemService;
    public ItemBrandsController(IUserService userService, IItemService itemService)
        => (_userService, _itemService) = (userService, itemService);

    [Produces(ResponseType)]
    [HttpGet]
    public async Task<Result<List<ItemBrand>>> Download()
    {
        try
        {
            if (User.Identity?.Name is null || !User.Identity.IsAuthenticated)
                return new Result<List<ItemBrand>>(ResultCode.Unauthorized, "userNotAuthenticated");
            string? tenancy = await _userService.GetTenantAsync(User.Identity.Name);
            if (tenancy is null)
                return new Result<List<ItemBrand>>(ResultCode.BadRequest, "companyNotRegistered");
            return new Result<List<ItemBrand>>(
                await _itemService.GetItemBrandsAsync(tenancy),
                ResultCode.NoContent
            );
        }
        catch (Exception e)
        {
            return new Result<List<ItemBrand>>(ResultCode.InternalServerError, e.Message);
        }
    }
    
    [Produces(ResponseType)]
    [HttpGet]
    public async Task<Result<bool>> List()
    {
        try
        {
            if (User.Identity?.Name is null || !User.Identity.IsAuthenticated)
                return new Result<bool>(ResultCode.Unauthorized, "userNotAuthenticated");
            if (!await _userService.CheckPermissionAsync(User.Identity.Name, "item_brands.read"))
                return new Result<bool>(ResultCode.Forbidden, "userNotAuthorized");
            string? tenancy = await _userService.GetTenantAsync(User.Identity.Name);
            return tenancy is null
                ? new Result<bool>(ResultCode.BadRequest, "companyNotRegistered")
                : new Result<bool>(true, ResultCode.Continue);
        }
        catch (Exception e)
        {
            return new Result<bool>(ResultCode.InternalServerError, e.Message);
        }
    }

    [Produces(ResponseType)]
    [HttpGet]
    public async Task<Result<bool>> Create()
    {
        try
        {
            if (User.Identity?.Name is null || !User.Identity.IsAuthenticated)
                return new Result<bool>(ResultCode.Unauthorized, "userNotAuthenticated");
            string? tenancy = await _userService.GetTenantAsync(User.Identity.Name);
            if (tenancy is null)
                return new Result<bool>(ResultCode.BadRequest, "companyNotRegistered");
            return await _userService.CheckPermissionAsync(User.Identity.Name, "item_brands.create")
                ? new Result<bool>(true, ResultCode.Continue)
                : new Result<bool>(ResultCode.Forbidden, "userNotAuthorized");
        }
        catch (Exception e)
        {
            return new Result<bool>(ResultCode.InternalServerError, e.Message);
        }
    }

    [Produces(ResponseType)]
    [HttpPost]
    public async Task<Result<ItemBrand>> Create([FromForm] BrandProfile model)
    {
        try
        {
            if (User.Identity?.Name is null || !User.Identity.IsAuthenticated)
                return new Result<ItemBrand>(ResultCode.Unauthorized, "userNotAuthenticated");
            string? tenancy = await _userService.GetTenantAsync(User.Identity.Name);
            if (tenancy is null)
                return new Result<ItemBrand>(ResultCode.BadRequest, "companyNotRegistered");
            if (!await _userService.CheckPermissionAsync(User.Identity.Name, "item_brands.create"))
                return new Result<ItemBrand>(ResultCode.Forbidden, "userNotAuthorized");
            var brand = new ItemBrand(model.Name) { Description = model.Description };
            return await _itemService.CreateBrandAsync(tenancy, brand)
                ? new Result<ItemBrand>(brand, ResultCode.Created)
                : new Result<ItemBrand>(null, ResultCode.BadRequest, "brands.alreadyExists");
        }
        catch (Exception e)
        {
            return new Result<ItemBrand>(ResultCode.InternalServerError, e.Message);
        }
    }

    [Produces(ResponseType)]
    [HttpGet]
    [Route("{id}")]
    public async Task<Result<bool>> Edit([FromRoute] string? id)
    {
        try
        {
            if (User.Identity?.Name is null || !User.Identity.IsAuthenticated)
                return new Result<bool>(ResultCode.Unauthorized, "userNotAuthenticated");
            if (id is null)
                return new Result<bool>(ResultCode.BadRequest, "idNotProvided");
            string? tenancy = await _userService.GetTenantAsync(User.Identity.Name);
            if (tenancy is null)
                return new Result<bool>(ResultCode.BadRequest, "companyNotRegistered");
            if (!await _userService.CheckPermissionAsync(User.Identity.Name, "item_brands.edit"))
                return new Result<bool>(ResultCode.Forbidden, "userNotAuthorized");
            return await _itemService.CheckItemBrandAsync(tenancy, id)
                ? new Result<bool>(true, ResultCode.Continue)
                : new Result<bool>(ResultCode.BadRequest, "brands.notFound");
        }
        catch (Exception e)
        {
            return new Result<bool>(ResultCode.InternalServerError, e.Message);
        }
    }
    
    [Produces(ResponseType)]
    [HttpPut]
    [Route("{id}")]
    public async Task<Result<BrandProfile>> Edit([FromRoute] string? id, [FromForm] BrandProfile model)
    {
        try
        {
            if (User.Identity?.Name is null || !User.Identity.IsAuthenticated)
                return new Result<BrandProfile>(ResultCode.Unauthorized, "userNotAuthenticated");
            if (id is null)
                return new Result<BrandProfile>(ResultCode.BadRequest, "idNotProvided");
            string? tenancy = await _userService.GetTenantAsync(User.Identity.Name);
            if (tenancy is null)
                return new Result<BrandProfile>(ResultCode.BadRequest, "companyNotRegistered");
            if (!await _userService.CheckPermissionAsync(User.Identity.Name, "item_brands.edit"))
                return new Result<BrandProfile>(ResultCode.Forbidden, "userNotAuthorized");
            var brand = new ItemBrand(model.Name) { Description = model.Description };
            return await _itemService.UpdateItemBrandAsync(tenancy, id, brand)
                ? new Result<BrandProfile>(new BrandProfile(brand.Name, brand.Description), ResultCode.NoContent)
                : new Result<BrandProfile>(null, ResultCode.BadRequest, "brands.notUpdated");
        }
        catch (Exception e)
        {
            return new Result<BrandProfile>(ResultCode.InternalServerError, e.Message);
        }
    }
}