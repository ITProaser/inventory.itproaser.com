﻿using ITProaser.Inventory.Areas.Inventory.ViewModels.Items;

namespace ITProaser.Inventory.Areas.Inventory.Controllers;

[Area("Inventory")]
[ApiController]
[Route("api/[area]/[controller]/[action]")]
public sealed class ItemsController : ControllerBase
{
    private const string ResponseType = "application/json";
    private readonly IItemService _itemService;
    private readonly IUserService _userService;
    private readonly ICompanyService _companyService;

    public ItemsController(IItemService itemService, IUserService userService, ICompanyService companyService)
        => (_itemService, _userService, _companyService) = (itemService, userService, companyService);

    [Produces(ResponseType)]
    [HttpGet]
    public async Task<Result<List<Item>>> Download()
    {
        try
        {
            if (User.Identity?.Name is null || !User.Identity.IsAuthenticated)
                return new Result<List<Item>>(ResultCode.Unauthorized, "UserNotAuthenticated");
            string? tenancy = await _userService.GetTenantAsync(User.Identity.Name);
            return tenancy is null
                ? new Result<List<Item>>(new List<Item>(), ResultCode.BadRequest, "companyNotRegistered")
                : new Result<List<Item>>(await _itemService.GetItemsAsync(tenancy), ResultCode.NoContent);
        }
        catch (Exception e)
        {
            return new Result<List<Item>>(ResultCode.InternalServerError, e.Message);
        }
    }

    [Produces(ResponseType)]
    [HttpGet]
    public async Task<Result<bool>> List()
    {
        try
        {
            if (User.Identity?.Name is null || !User.Identity.IsAuthenticated)
                return new Result<bool>(ResultCode.Unauthorized, "UserNotAuthenticated");
            string? tenancy = await _userService.GetTenantAsync(User.Identity.Name);
            return tenancy is null
                ? new Result<bool>(ResultCode.BadRequest, "companyNotRegistered")
                : new Result<bool>(true, ResultCode.Continue);
        }
        catch (Exception e)
        {
            return new Result<bool>(ResultCode.InternalServerError, e.Message);
        }
    }

    [Produces(ResponseType)]
    [HttpGet]
    [Route("{id}")]
    public async Task<Result<Item>> Details(string id)
    {
        try
        {
            if (User.Identity?.Name is null || !User.Identity.IsAuthenticated)
                return new Result<Item>(ResultCode.Unauthorized, "UserNotAuthenticated");
            string? tenancy = await _userService.GetTenantAsync(User.Identity.Name);
            return tenancy is null
                ? new Result<Item>(ResultCode.BadRequest, "CompanyNotRegistered")
                : new Result<Item>(await _itemService.GetItemAsync(id, tenancy), ResultCode.Ok);
        }
        catch (Exception e)
        {
            return new Result<Item>(ResultCode.InternalServerError, e.Message);
        }
    }

    [Produces(ResponseType)]
    [HttpGet]
    public async Task<Result<bool>> Create()
    {
        try
        {
            if (User.Identity?.Name is null || !User.Identity.IsAuthenticated)
                return new Result<bool>(ResultCode.Unauthorized, "UserNotAuthenticated");
            string? tenancy = await _userService.GetTenantAsync(User.Identity.Name);
            if (tenancy is null)
                return new Result<bool>(ResultCode.BadRequest, "CompanyNotRegistered");
            return await _userService.CheckPermissionAsync(User.Identity.Name, "items.create")
                ? new Result<bool>(true, ResultCode.Continue)
                : new Result<bool>(ResultCode.Forbidden, "UserNotAuthorized");
        }
        catch (Exception e)
        {
            return new Result<bool>(ResultCode.InternalServerError, e.Message);
        }
    }

    [Produces(ResponseType)]
    [HttpPost]
    public async Task<Result<Item>> Create([FromForm] CreateItemViewModel model)
    {
        try
        {
            if (User.Identity?.Name is null || !User.Identity.IsAuthenticated)
                return new Result<Item>(ResultCode.Unauthorized, "UserNotAuthenticated");
            string? user =
                await _userService.GetUserByNameAsync(User.Identity.Name, selector => selector.UId);
            if (user is null)
                return new Result<Item>(ResultCode.BadRequest, "UserNotRegistered");
            string? tenancy = await _userService.GetTenantAsync(user);
            if (tenancy is null)
                return new Result<Item>(ResultCode.InternalServerError, "TenancyNotFound");
            if (!await _userService.CheckPermissionAsync(user, "items.create"))
                return new Result<Item>(ResultCode.Forbidden, "UserNotAuthorized");
            if (await _itemService.CheckItemByNameAsync(tenancy, model.Name))
                return new Result<Item>(ResultCode.BadRequest, "ItemAlreadyExists");
            var item = new Item(model.Type, model.Name)
            {
                Sku = model.Sku,
                UniversalProductCode = model.UniversalProductCode,
                InternationalArticleNumber = model.InternationalArticleNumber,
                ManufacturerPartNumber = model.ManufacturerPartNumber,
                InternationalStandardBookNumber = model.InternationalStandardBookNumber,
                IsReturnable = model.IsReturnable,
                BrandId = model.BrandId is null
                    ? null
                    : await _itemService.GetItemBrandAsync(tenancy, model.BrandId, b => b.Id),
                CategoryId = model.CategoryId is null
                    ? null
                    : await _itemService.GetItemCategoryAsync(tenancy, model.CategoryId, c => c.Id),
                ManufacturerId = model.ManufacturerId is null
                    ? null
                    : await _itemService.GetItemManufacturerAsync(tenancy, model.ManufacturerId, m => m.Id),
                UnitId = model.UnitId is null
                    ? null
                    : await _itemService.GetItemUnitAsync(tenancy, model.UnitId, u => u.Id)
            };
            if (model.IncludePurchaseInformation)
            {
                if (model.PurchaseRate is not null)
                    item.PurchaseInformation = new ItemTransactionInfo
                    {
                        Rate = (decimal)model.PurchaseRate,
                        TaxId = model.PurchaseTaxId is null ? null : await _companyService.GetTaxAsync(tenancy, model.PurchaseTaxId, t => t.Id),
                        Description = model.PurchaseDescription
                    };
            }
            if (!model.IncludeSalesInformation) return new Result<Item>(item, ResultCode.Created);
            if (model.SalesRate is not null)
                item.SalesInformation = new ItemTransactionInfo
                {
                    Rate = (decimal)model.SalesRate,
                    TaxId = model.SalesTaxId is null ? null : await _companyService.GetTaxAsync(tenancy, model.SalesTaxId, t => t.Id),
                    Description = model.SalesDescription
                };
            return await _itemService.CreateItemAsync(tenancy, item)
                ? new Result<Item>(item, ResultCode.Created)
                : new Result<Item>(ResultCode.InternalServerError, "ItemNotCreated");
        }
        catch (Exception e)
        {
            return new Result<Item>(ResultCode.InternalServerError, e.Message);
        }
    }
}