﻿namespace ITProaser.Inventory.Areas.Inventory.Controllers;

[Area("Inventory")]
[ApiController]
[Route("api/[area]/item-groups")]
public sealed class ItemGroups : ControllerBase
{
    private const string ResponseType = "application/json";
    private readonly IUserService _userService;

    public ItemGroups(IUserService userService) => _userService = userService;

    [Produces(ResponseType)]
    [HttpGet]
    [Route("[action]")]
    public async Task<Result<bool>> List()
    {
        try
        {
            if (User.Identity?.Name is null || !User.Identity.IsAuthenticated)
                return new Result<bool>(ResultCode.Unauthorized, "userNotAuthenticated");
            string? tenancy = await _userService.GetTenantAsync(User.Identity.Name);
            return tenancy is null
                ? new Result<bool>(ResultCode.BadRequest, "companyNotRegistered")
                : new Result<bool>(true, ResultCode.Continue);
        }
        catch (Exception e)
        {
            return new Result<bool>(ResultCode.InternalServerError, e.Message);
        }
    }
}