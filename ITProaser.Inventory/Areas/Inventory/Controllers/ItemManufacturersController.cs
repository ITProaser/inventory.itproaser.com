﻿using ITProaser.Inventory.Areas.Inventory.ViewModels.ItemManufacturers;

namespace ITProaser.Inventory.Areas.Inventory.Controllers;

[Area("Inventory")]
[ApiController]
[Route("api/[area]/item-manufacturers/[action]")]
public sealed class ItemManufacturersController : ControllerBase
{
    private const string ResponseType = "application/json";
    private readonly IUserService _userService;
    private readonly IItemService _itemService;
    public ItemManufacturersController(IUserService userService, IItemService itemService)
        => (_userService, _itemService) = (userService, itemService);

    [Produces(ResponseType)]
    [HttpGet]
    public async Task<Result<List<ItemManufacturer>>> Download()
    {
        try
        {
            if (User.Identity?.Name is null || !User.Identity.IsAuthenticated)
                return new Result<List<ItemManufacturer>>(ResultCode.Unauthorized, "userNotAuthenticated");
            string? tenancy = await _userService.GetTenantAsync(User.Identity.Name);
            if (tenancy is null)
                return new Result<List<ItemManufacturer>>(ResultCode.BadRequest, "companyNotRegistered");
            return new Result<List<ItemManufacturer>>(
                await _itemService.GetItemManufacturersAsync(tenancy),
                ResultCode.NoContent
            );
        }
        catch (Exception e)
        {
            return new Result<List<ItemManufacturer>>(ResultCode.InternalServerError, e.Message);
        }
    }

    [Produces(ResponseType)]
    [HttpGet]
    public async Task<Result<bool>> List()
    {
        try
        {
            if (User.Identity?.Name is null || !User.Identity.IsAuthenticated)
                return new Result<bool>(ResultCode.Unauthorized, "userNotAuthenticated");
            if (!await _userService.CheckPermissionAsync(User.Identity.Name, "item_manufacturers.read"))
                return new Result<bool>(ResultCode.Forbidden, "userNotAuthorized");
            string? tenancy = await _userService.GetTenantAsync(User.Identity.Name);
            return tenancy is null
                ? new Result<bool>(ResultCode.BadRequest, "companyNotRegistered")
                : new Result<bool>(true, ResultCode.Continue);
        }
        catch (Exception e)
        {
            return new Result<bool>(ResultCode.InternalServerError, e.Message);
        }
    }
    
    [Produces(ResponseType)]
    [HttpGet]
    public async Task<Result<bool>> Create()
    {
        try
        {
            if (User.Identity?.Name is null || !User.Identity.IsAuthenticated)
                return new Result<bool>(ResultCode.Unauthorized, "userNotAuthenticated");
            string? tenancy = await _userService.GetTenantAsync(User.Identity.Name);
            if (tenancy is null)
                return new Result<bool>(ResultCode.BadRequest, "companyNotRegistered");
            return await _userService.CheckPermissionAsync(User.Identity.Name, "item_manufacturers.create")
                ? new Result<bool>(true, ResultCode.Continue)
                : new Result<bool>(ResultCode.Forbidden, "userNotAuthorized");
        }
        catch (Exception e)
        {
            return new Result<bool>(ResultCode.InternalServerError, e.Message);
        }
    }

    [Produces(ResponseType)]
    [HttpPost]
    public async Task<Result<ItemManufacturer>> Create([FromForm] ManufacturerProfile model)
    {
        try
        {
            if (User.Identity?.Name is null || !User.Identity.IsAuthenticated)
                return new Result<ItemManufacturer>(ResultCode.Unauthorized, "userNotAuthenticated");
            string? tenancy = await _userService.GetTenantAsync(User.Identity.Name);
            if (tenancy is null)
                return new Result<ItemManufacturer>(ResultCode.BadRequest, "companyNotRegistered");
            if (!await _userService.CheckPermissionAsync(User.Identity.Name, "Inventory.Manage"))
                return new Result<ItemManufacturer>(ResultCode.Forbidden, "userNotAuthorized");
            var manufacturer = new ItemManufacturer(model.Name) { Description = model.Description };
            if (!await _userService.CheckPermissionAsync(User.Identity.Name, "item_manufacturers.create"))
                return new Result<ItemManufacturer>(ResultCode.Forbidden, "userNotAuthorized");
            return await _itemService.CreateManufacturerAsync(tenancy, manufacturer)
                ? new Result<ItemManufacturer>(manufacturer, ResultCode.Created)
                : new Result<ItemManufacturer>(null, ResultCode.InternalServerError, "manufacturers.alreadyExists");
        }
        catch (Exception e)
        {
            return new Result<ItemManufacturer>(ResultCode.InternalServerError, e.Message);
        }
    }
    
    [Produces(ResponseType)]
    [HttpGet]
    [Route("{id}")]
    public async Task<Result<bool>> Edit([FromRoute] string? id)
    {
        try
        {
            if (User.Identity?.Name is null || !User.Identity.IsAuthenticated)
                return new Result<bool>(ResultCode.Unauthorized, "userNotAuthenticated");
            if (id is null)
                return new Result<bool>(ResultCode.BadRequest, "idNotProvided");
            string? tenancy = await _userService.GetTenantAsync(User.Identity.Name);
            if (tenancy is null)
                return new Result<bool>(ResultCode.BadRequest, "companyNotRegistered");
            if (!await _userService.CheckPermissionAsync(User.Identity.Name, "item_manufacturers.edit"))
                return new Result<bool>(ResultCode.Forbidden, "userNotAuthorized");
            return await _itemService.CheckItemManufacturerAsync(tenancy, id)
                ? new Result<bool>(true, ResultCode.Continue)
                : new Result<bool>(ResultCode.BadRequest, "brands.notFound");
        }
        catch (Exception e)
        {
            return new Result<bool>(ResultCode.InternalServerError, e.Message);
        }
    }
    
    [Produces(ResponseType)]
    [HttpPut]
    [Route("{id}")]
    public async Task<Result<ManufacturerProfile>> Edit([FromRoute] string? id, [FromForm] ManufacturerProfile model)
    {
        try
        {
            if (User.Identity?.Name is null || !User.Identity.IsAuthenticated)
                return new Result<ManufacturerProfile>(ResultCode.Unauthorized, "userNotAuthenticated");
            if (id is null)
                return new Result<ManufacturerProfile>(ResultCode.BadRequest, "idNotProvided");
            string? tenancy = await _userService.GetTenantAsync(User.Identity.Name);
            if (tenancy is null)
                return new Result<ManufacturerProfile>(ResultCode.BadRequest, "companyNotRegistered");
            if (!await _userService.CheckPermissionAsync(User.Identity.Name, "item_manufacturers.edit"))
                return new Result<ManufacturerProfile>(ResultCode.Forbidden, "userNotAuthorized");
            var manufacturer = new ItemManufacturer(model.Name) { Description = model.Description };
            return await _itemService.UpdateItemManufacturerAsync(tenancy, id, manufacturer)
                ? new Result<ManufacturerProfile>(new ManufacturerProfile(manufacturer.Name, manufacturer.Description), ResultCode.NoContent)
                : new Result<ManufacturerProfile>(null, ResultCode.BadRequest, "manufacturers.notUpdated");
        }
        catch (Exception e)
        {
            return new Result<ManufacturerProfile>(ResultCode.InternalServerError, e.Message);
        }
    }
}