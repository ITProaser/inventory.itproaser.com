﻿using ITProaser.Inventory.Areas.Inventory.ViewModels.ItemUnits;

namespace ITProaser.Inventory.Areas.Inventory.Controllers;

[Area("Inventory")]
[ApiController]
[Route("api/[area]/item-units/[action]")]
public sealed class ItemUnitsController : ControllerBase
{
    private const string ResponseType = "application/json";
    private readonly IUserService _userService;
    private readonly IItemService _itemService;

    public ItemUnitsController(IItemService itemService, IUserService userService)
        => (_itemService, _userService) = (itemService, userService);

    [Produces(ResponseType)]
    [HttpGet]
    public async Task<Result<List<ItemUnit>>> Download()
    {
        try
        {
            if (User.Identity?.Name is null || !User.Identity.IsAuthenticated)
                return new Result<List<ItemUnit>>(ResultCode.Unauthorized, "UserNotAuthenticated");
            string? tenancy = await _userService.GetTenantAsync(User.Identity.Name);
            if (tenancy is null)
                return new Result<List<ItemUnit>>(ResultCode.BadRequest, "CompanyNotRegistered");
            return new Result<List<ItemUnit>>(
                await _itemService.GetItemUnitsAsync(tenancy),
                ResultCode.NoContent
            );
        }
        catch (Exception e)
        {
            return new Result<List<ItemUnit>>(ResultCode.InternalServerError, e.Message);
        }
    }

    [Produces(ResponseType)]
    [HttpGet]
    public async Task<Result<bool>> List()
    {
        try
        {
            if (User.Identity?.Name is null || !User.Identity.IsAuthenticated)
                return new Result<bool>(ResultCode.Unauthorized, "userNotAuthenticated");
            if (!await _userService.CheckPermissionAsync(User.Identity.Name, "item_units.read"))
                return new Result<bool>(ResultCode.Forbidden, "userNotAuthorized");
            string? tenancy = await _userService.GetTenantAsync(User.Identity.Name);
            return tenancy is null
                ? new Result<bool>(ResultCode.BadRequest, "companyNotRegistered")
                : new Result<bool>(true, ResultCode.Continue);
        }
        catch (Exception e)
        {
            return new Result<bool>(ResultCode.InternalServerError, e.Message);
        }
    }

    [Produces(ResponseType)]
    [HttpGet]
    public async Task<Result<bool>> Create()
    {
        try
        {
            if (User.Identity?.Name is null || !User.Identity.IsAuthenticated)
                return new Result<bool>(ResultCode.Unauthorized, "userNotAuthenticated");
            string? tenancy = await _userService.GetTenantAsync(User.Identity.Name);
            if (tenancy is null)
                return new Result<bool>(ResultCode.BadRequest, "companyNotRegistered");
            return await _userService.CheckPermissionAsync(User.Identity.Name, "item_units.create")
                ? new Result<bool>(true, ResultCode.Continue)
                : new Result<bool>(ResultCode.Forbidden, "userNotAuthorized");
        }
        catch (Exception e)
        {
            return new Result<bool>(ResultCode.InternalServerError, e.Message);
        }
    }
    
    [Produces(ResponseType)]
    [HttpPost]
    public async Task<Result<ItemUnit>> Create([FromForm] UnitProfile model)
    {
        try
        {
            if (User.Identity?.Name is null || !User.Identity.IsAuthenticated)
                return new Result<ItemUnit>(ResultCode.Unauthorized, "UserNotAuthenticated");
            string? tenancy = await _userService.GetTenantAsync(User.Identity.Name);
            if (tenancy is null)
                return new Result<ItemUnit>(ResultCode.BadRequest, "CompanyNotRegistered");
            var unit = new ItemUnit(model.Name) { Description = model.Description };
            if (!await _userService.CheckPermissionAsync(User.Identity.Name, "item_units.create"))
                return new Result<ItemUnit>(ResultCode.Forbidden, "userNotAuthorized");
            return await _itemService.CreateUnitAsync(tenancy, unit)
                ? new Result<ItemUnit>(unit, ResultCode.Created, "done")
                : new Result<ItemUnit>(ResultCode.BadRequest, "units.alreadyExists");
        }
        catch (Exception e)
        {
            return new Result<ItemUnit>(ResultCode.InternalServerError, e.Message);
        }
    }

    [Produces(ResponseType)]
    [HttpGet]
    [Route("{id}")]
    public async Task<Result<bool>> Edit([FromRoute] string? id)
    {
        try
        {
            if (User.Identity?.Name is null || !User.Identity.IsAuthenticated)
                return new Result<bool>(ResultCode.Unauthorized, "userNotAuthenticated");
            if (id is null)
                return new Result<bool>(ResultCode.BadRequest, "idNotProvided");
            string? tenancy = await _userService.GetTenantAsync(User.Identity.Name);
            if (tenancy is null)
                return new Result<bool>(ResultCode.BadRequest, "companyNotRegistered");
            if (!await _userService.CheckPermissionAsync(User.Identity.Name, "item_units.edit"))
                return new Result<bool>(ResultCode.Forbidden, "userNotAuthorized");
            return await _itemService.CheckItemUnitAsync(tenancy, id)
                ? new Result<bool>(true, ResultCode.Continue)
                : new Result<bool>(ResultCode.BadRequest, "brands.notFound");
        }
        catch (Exception e)
        {
            return new Result<bool>(ResultCode.InternalServerError, e.Message);
        }
    }
    
    [Produces(ResponseType)]
    [HttpPut]
    [Route("{id}")]
    public async Task<Result<UnitProfile>> Edit([FromRoute] string? id, [FromForm] UnitProfile model)
    {
        try
        {
            if (User.Identity?.Name is null || !User.Identity.IsAuthenticated)
                return new Result<UnitProfile>(ResultCode.Unauthorized, "userNotAuthenticated");
            if (id is null)
                return new Result<UnitProfile>(ResultCode.BadRequest, "idNotProvided");
            string? tenancy = await _userService.GetTenantAsync(User.Identity.Name);
            if (tenancy is null)
                return new Result<UnitProfile>(ResultCode.BadRequest, "companyNotRegistered");
            if (!await _userService.CheckPermissionAsync(User.Identity.Name, "item_units.edit"))
                return new Result<UnitProfile>(ResultCode.Forbidden, "userNotAuthorized");
            var unit = new ItemUnit(model.Name) { Description = model.Description };
            return await _itemService.UpdateItemUnitAsync(tenancy, id, unit)
                ? new Result<UnitProfile>(new UnitProfile(unit.Name, unit.Description), ResultCode.NoContent)
                : new Result<UnitProfile>(null, ResultCode.BadRequest, "units.notUpdated");
        }
        catch (Exception e)
        {
            return new Result<UnitProfile>(ResultCode.InternalServerError, e.Message);
        }
    }
}