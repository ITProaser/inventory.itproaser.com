﻿using ITProaser.Inventory.Areas.Inventory.ViewModels.ItemCategories;

namespace ITProaser.Inventory.Areas.Inventory.Controllers;

[Area("Inventory")]
[ApiController]
[Route("api/[area]/item-categories/[action]")]
public sealed class ItemCategoriesController : ControllerBase
{
    private const string ResponseType = "application/json";
    private readonly IUserService _userService;
    private readonly IItemService _itemService;
    
    public ItemCategoriesController(IUserService userService, IItemService itemService)
        => (_userService, _itemService) = (userService, itemService);
    

    [Produces(ResponseType)]
    [HttpGet]
    public async Task<Result<List<ItemCategory>>> Download()
    {
        try
        {
            if (User.Identity?.Name is null || !User.Identity.IsAuthenticated)
                return new Result<List<ItemCategory>>(ResultCode.Unauthorized, "UserNotAuthenticated");
            string? tenancy = await _userService.GetTenantAsync(User.Identity.Name);
            if (tenancy is null)
                return new Result<List<ItemCategory>>(ResultCode.BadRequest, "CompanyNotRegistered");
            return new Result<List<ItemCategory>>(
                await _itemService.GetItemCategoriesAsync(tenancy),
                ResultCode.NoContent
            );
        }
        catch (Exception e)
        {
            return new Result<List<ItemCategory>>(ResultCode.InternalServerError, e.Message);
        }
    }
    
    [Produces(ResponseType)]
    [HttpGet]
    public async Task<Result<bool>> List()
    {
        try
        {
            if (User.Identity?.Name is null || !User.Identity.IsAuthenticated)
                return new Result<bool>(ResultCode.Unauthorized, "userNotAuthenticated");
            if (!await _userService.CheckPermissionAsync(User.Identity.Name, "item_categories.read"))
                return new Result<bool>(ResultCode.Forbidden, "userNotAuthorized");
            string? tenancy = await _userService.GetTenantAsync(User.Identity.Name);
            return tenancy is null
                ? new Result<bool>(ResultCode.BadRequest, "companyNotRegistered")
                : new Result<bool>(true, ResultCode.Continue);
        }
        catch (Exception e)
        {
            return new Result<bool>(ResultCode.InternalServerError, e.Message);
        }
    }
    
    [Produces(ResponseType)]
    [HttpGet]
    public async Task<Result<bool>> Create()
    {
        try
        {
            if (User.Identity?.Name is null || !User.Identity.IsAuthenticated)
                return new Result<bool>(ResultCode.Unauthorized, "userNotAuthenticated");
            string? tenancy = await _userService.GetTenantAsync(User.Identity.Name);
            if (tenancy is null)
                return new Result<bool>(ResultCode.BadRequest, "companyNotRegistered");
            return await _userService.CheckPermissionAsync(User.Identity.Name, "item_categories.create")
                ? new Result<bool>(true, ResultCode.Continue)
                : new Result<bool>(ResultCode.Forbidden, "userNotAuthorized");
        }
        catch (Exception e)
        {
            return new Result<bool>(ResultCode.InternalServerError, e.Message);
        }
    }

    [Produces(ResponseType)]
    [HttpPost]
    public async Task<Result<ItemCategory>> Create([FromForm] CategoryProfile model)
    {
        try
        {
            if (User.Identity?.Name is null || !User.Identity.IsAuthenticated)
                return new Result<ItemCategory>(ResultCode.Unauthorized, "userNotAuthenticated");
            string? tenancy = await _userService.GetTenantAsync(User.Identity.Name);
            if (tenancy is null)
                return new Result<ItemCategory>(ResultCode.BadRequest, "companyNotRegistered");
            if (!await _userService.CheckPermissionAsync(User.Identity.Name, "item_categories.create"))
                return new Result<ItemCategory>(ResultCode.Forbidden, "userNotAuthorized");
            var category = new ItemCategory(model.Name) { Description = model.Description };
            return await _itemService.CreateCategoryAsync(tenancy, category)
                ? new Result<ItemCategory>(category, ResultCode.Created)
                : new Result<ItemCategory>(null, ResultCode.InternalServerError, "categories.alreadyExists");
        }
        catch (Exception e)
        {
            return new Result<ItemCategory>(ResultCode.InternalServerError, e.Message);
        }
    }
    
    [Produces(ResponseType)]
    [HttpGet]
    [Route("{id}")]
    public async Task<Result<bool>> Edit([FromRoute] string? id)
    {
        try
        {
            if (User.Identity?.Name is null || !User.Identity.IsAuthenticated)
                return new Result<bool>(ResultCode.Unauthorized, "userNotAuthenticated");
            if (id is null)
                return new Result<bool>(ResultCode.BadRequest, "idNotProvided");
            string? tenancy = await _userService.GetTenantAsync(User.Identity.Name);
            if (tenancy is null)
                return new Result<bool>(ResultCode.BadRequest, "companyNotRegistered");
            if (!await _userService.CheckPermissionAsync(User.Identity.Name, "item_categories.edit"))
                return new Result<bool>(ResultCode.Forbidden, "userNotAuthorized");
            return await _itemService.CheckItemCategoryAsync(tenancy, id)
                ? new Result<bool>(true, ResultCode.Continue)
                : new Result<bool>(ResultCode.BadRequest, "brands.notFound");
        }
        catch (Exception e)
        {
            return new Result<bool>(ResultCode.InternalServerError, e.Message);
        }
    }

    [Produces(ResponseType)]
    [HttpPut]
    [Route("{id}")]
    public async Task<Result<CategoryProfile>> Edit([FromRoute] string? id, [FromForm] CategoryProfile model)
    {
        try
        {
            if (User.Identity?.Name is null || !User.Identity.IsAuthenticated)
                return new Result<CategoryProfile>(ResultCode.Unauthorized, "userNotAuthenticated");
            if (id is null)
                return new Result<CategoryProfile>(ResultCode.BadRequest, "idNotProvided");
            string? tenancy = await _userService.GetTenantAsync(User.Identity.Name);
            if (tenancy is null)
                return new Result<CategoryProfile>(ResultCode.BadRequest, "companyNotRegistered");
            if (!await _userService.CheckPermissionAsync(User.Identity.Name, "item_categories.edit"))
                return new Result<CategoryProfile>(ResultCode.Forbidden, "userNotAuthorized");
            var category = new ItemCategory(model.Name) { Description = model.Description };
            return await _itemService.UpdateItemCategoryAsync(tenancy, id, category)
                ? new Result<CategoryProfile>(new CategoryProfile(category.Name, category.Description), ResultCode.NoContent)
                : new Result<CategoryProfile>(null, ResultCode.BadRequest, "categories.notUpdated");
        }
        catch (Exception e)
        {
            return new Result<CategoryProfile>(ResultCode.InternalServerError, e.Message);
        }
    }
}

