﻿using System.ComponentModel.DataAnnotations;

namespace ITProaser.Inventory.Areas.Purchases.ViewModels.Orders;

public record OrderCreate(
    [Required]
    [MaxLength(32)]
    string Number,
    
    [MaxLength(32)]
    string? Reference,
    
    [DataType(DataType.Date)]
    DateOnly? ExpectedDate,
    
    [Required]
    DiscountType DiscountType,
    
    [Required]
    DiscountLevel DiscountLevel,
    
    [Required]
    decimal Adjustment,
    
    [MaxLength(256)]
    string? CustomerNotes,
    
    [MaxLength(256)]
    string? TermsAndConditions,
    
    [Required]
    [MaxLength(8)]
    string VendorId,
    
    OrderLine[] OrderLines
);