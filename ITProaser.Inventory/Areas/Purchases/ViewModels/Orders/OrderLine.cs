﻿using System.ComponentModel.DataAnnotations;

namespace ITProaser.Inventory.Areas.Purchases.ViewModels.Orders;

public record OrderLine(
    [Required]
    int Line,
    
    [Required]
    [MaxLength(8)]
    string ItemId,
    
    [Required]
    decimal Quantity,
    
    [Required]
    decimal Price,
    
    [Required]
    DiscountType DiscountType,
    
    [Required]
    decimal Discount,
    
    [MaxLength(256)]
    string? Notes
);