﻿using ITProaser.Inventory.ViewModels.Content;

namespace ITProaser.Inventory.Areas.Purchases.ViewModels.Vendors;

public record VendorProfile(
    string Id,
    string? FirstName,
    string LastName,
    string DisplayName,
    string? Email,
    string? CompanyName,
    string? MobilePhone,
    string? WorkPhone,
    string? Website,
    string? Title,
    string? Remarks,
    AddressProfile? BillingAddress,
    AddressProfile? ShippingAddress
)
{
    public VendorProfile(Vendor vendor) : this(
        vendor.UId,
        vendor.FirstName,
        vendor.LastName,
        vendor.DisplayName,
        vendor.Email,
        vendor.CompanyName,
        vendor.Mobile,
        vendor.Phone,
        vendor.Website,
        vendor.Title,
        vendor.Remarks,
        vendor.BillingAddress is null ? null : new AddressProfile(vendor.BillingAddress),
        vendor.ShippingAddress is null ? null : new AddressProfile(vendor.ShippingAddress)
    )
    {
        CurrencyId = vendor.Currency?.UId ?? string.Empty;
        TaxId = vendor.Tax?.UId;
        PaymentTermId = vendor.PaymentTerm?.UId;
    }

    public string CurrencyId { get; set; } = null!;
    
    public string? TaxId { get; set; }
    
    public string? PaymentTermId { get; set; }
}