﻿namespace ITProaser.Inventory.Areas.Purchases.Controllers;

[Area("Purchases")]
[ApiController]
[Route("api/[area]/[controller]/[action]")]
public sealed class OrdersController : ControllerBase
{
    private const string ResponseType = "application/json";
    private readonly IUserService _userService;
    private readonly IPurchaseOrdersService _purchaseOrdersService;
    
    public OrdersController(IUserService userService, IPurchaseOrdersService purchaseOrdersService)
        => (_userService, _purchaseOrdersService) = (userService, purchaseOrdersService);

    [Produces(ResponseType)]
    [HttpGet]
    public async Task<Result<List<PurchaseOrder>>> Download()
    {
        try
        {
            if (User.Identity?.Name is null || !User.Identity.IsAuthenticated)
                return new Result<List<PurchaseOrder>>(ResultCode.Unauthorized, "userNotAuthenticated");
            string? tenancy = await _userService.GetTenantAsync(User.Identity.Name);
            if (tenancy is null)
                return new Result<List<PurchaseOrder>>(ResultCode.BadRequest, "companyNotRegistered");
            return new Result<List<PurchaseOrder>>(
                await _purchaseOrdersService.GetOrdersAsync(tenancy),
                ResultCode.NoContent
            );
        }
        catch (Exception e)
        {
            return new Result<List<PurchaseOrder>>(ResultCode.InternalServerError, e.Message);
        }
    }
    
    [Produces(ResponseType)]
    [HttpGet]
    public async Task<Result<bool>> List()
    {
        try
        {
            if (User.Identity?.Name is null || !User.Identity.IsAuthenticated)
                return new Result<bool>(ResultCode.Unauthorized, "userNotAuthenticated");
            if (!await _userService.CheckPermissionAsync(User.Identity.Name, "purchase_orders.read"))
                return new Result<bool>(ResultCode.Forbidden, "userNotAuthorized");
            string? tenancy = await _userService.GetTenantAsync(User.Identity.Name);
            return tenancy is null
                ? new Result<bool>(ResultCode.BadRequest, "companyNotRegistered")
                : new Result<bool>(true, ResultCode.Continue);
        }
        catch (Exception e)
        {
            return new Result<bool>(ResultCode.InternalServerError, e.Message);
        }
    }

    [Produces(ResponseType)]
    [HttpGet]
    public async Task<Result<bool>> Create()
    {
        try
        {
            if (User.Identity?.Name is null || !User.Identity.IsAuthenticated)
                return new Result<bool>(ResultCode.Unauthorized, "userNotAuthenticated");
            string? tenancy = await _userService.GetTenantAsync(User.Identity.Name);
            if (tenancy is null)
                return new Result<bool>(ResultCode.BadRequest, "companyNotRegistered");
            return await _userService.CheckPermissionAsync(User.Identity.Name, "purchase_orders.create")
                ? new Result<bool>(true, ResultCode.Continue)
                : new Result<bool>(ResultCode.Forbidden, "userNotAuthorized");
        }
        catch (Exception e)
        {
            return new Result<bool>(ResultCode.InternalServerError, e.Message);
        }
    }

    [Produces(ResponseType)]
    [HttpGet]
    [Route("{id}")]
    public async Task<Result<bool>> Edit([FromRoute] string? id)
    {
        try
        {
            if (id is null)
                return new Result<bool>(ResultCode.BadRequest, "idNotProvided");
            if (User.Identity?.Name is null || !User.Identity.IsAuthenticated)
                return new Result<bool>(ResultCode.Unauthorized, "userNotAuthenticated");
            string? tenancy = await _userService.GetTenantAsync(User.Identity.Name);
            if (tenancy is null)
                return new Result<bool>(ResultCode.BadRequest, "companyNotRegistered");
            if (!await _userService.CheckPermissionAsync(User.Identity.Name, "purchase_orders.edit"))
                return new Result<bool>(ResultCode.Forbidden, "userNotAuthorized");
            return await _purchaseOrdersService.CheckOrderAsync(tenancy, id)
                ? new Result<bool>(true, ResultCode.Continue)
                : new Result<bool>(ResultCode.NotFound, "orderNotFound");
        }
        catch (Exception e)
        {
            return new Result<bool>(ResultCode.InternalServerError, e.Message);
        }
    }
}