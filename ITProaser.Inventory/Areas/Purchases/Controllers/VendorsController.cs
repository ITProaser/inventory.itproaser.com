﻿using ITProaser.Inventory.Areas.Purchases.ViewModels.Vendors;

namespace ITProaser.Inventory.Areas.Purchases.Controllers;

[Area("Purchases")]
[ApiController]
[Route("api/[area]/[controller]/[action]")]
public sealed class VendorsController : ControllerBase
{
    private const string ResponseType = "application/json";
    private readonly IUserService _userService;
    private readonly IVendorService _vendorService;
    private readonly ICompanyService _companyService;

    public VendorsController(IUserService userService, IVendorService vendorService, ICompanyService companyService)
        => (_userService, _vendorService, _companyService) = (userService, vendorService, companyService);

    [Produces(ResponseType)]
    [HttpGet]
    public async Task<Result<List<VendorProfile>>> Download()
    {
        try
        {
            if (User.Identity?.Name is null || !User.Identity.IsAuthenticated)
                return new Result<List<VendorProfile>>(ResultCode.Unauthorized, "userNotAuthenticated");
            string? tenancy = await _userService.GetTenantAsync(User.Identity.Name);
            if (tenancy is null)
                return new Result<List<VendorProfile>>(ResultCode.BadRequest, "companyNotRegistered");
            return new Result<List<VendorProfile>>(
                await _vendorService.GetVendorsAsync(tenancy, v => new VendorProfile(v)),
                ResultCode.NoContent
            );
        }
        catch (Exception e)
        {
            return new Result<List<VendorProfile>>(ResultCode.InternalServerError, e.Message);
        }
    }

    [Produces(ResponseType)]
    [HttpGet]
    public async Task<Result<bool>> List()
    {
        try
        {
            if (User.Identity?.Name is null || !User.Identity.IsAuthenticated)
                return new Result<bool>(ResultCode.Unauthorized, "userNotAuthenticated");
            if (!await _userService.CheckPermissionAsync(User.Identity.Name, "vendors.read"))
                return new Result<bool>(ResultCode.Forbidden, "userNotAuthorized");
            string? tenancy = await _userService.GetTenantAsync(User.Identity.Name);
            return tenancy is null
                ? new Result<bool>(ResultCode.BadRequest, "companyNotRegistered")
                : new Result<bool>(true, ResultCode.Continue);
        }
        catch (Exception e)
        {
            return new Result<bool>(ResultCode.InternalServerError, e.Message);
        }
    }

    [Produces(ResponseType)]
    [HttpGet]
    public async Task<Result<bool>> Create()
    {
        try
        {
            if (User.Identity?.Name is null || !User.Identity.IsAuthenticated)
                return new Result<bool>(ResultCode.Unauthorized, "userNotAuthenticated");
            string? tenancy = await _userService.GetTenantAsync(User.Identity.Name);
            if (tenancy is null)
                return new Result<bool>(ResultCode.BadRequest, "companyNotRegistered");
            return await _userService.CheckPermissionAsync(User.Identity.Name, "vendors.create")
                ? new Result<bool>(true, ResultCode.Continue)
                : new Result<bool>(ResultCode.Forbidden, "userNotAuthorized");
        }
        catch (Exception e)
        {
            return new Result<bool>(ResultCode.InternalServerError, e.Message);
        }
    }

    [Produces(ResponseType)]
    [HttpGet]
    [Route("{id}")]
    public async Task<Result<bool>> Edit([FromRoute] string? id)
    {
        try
        {
            if (User.Identity?.Name is null || !User.Identity.IsAuthenticated)
                return new Result<bool>(ResultCode.Unauthorized, "userNotAuthenticated");
            string? tenancy = await _userService.GetTenantAsync(User.Identity.Name);
            if (tenancy is null)
                return new Result<bool>(ResultCode.BadRequest, "companyNotRegistered");
            return await _userService.CheckPermissionAsync(User.Identity.Name, "vendors.edit")
                ? new Result<bool>(true, ResultCode.Continue)
                : new Result<bool>(ResultCode.Forbidden, "userNotAuthorized");
        }
        catch (Exception e)
        {
            return new Result<bool>(ResultCode.InternalServerError, e.Message);
        }
    }

    [Produces(ResponseType)]
    [HttpPost]
    public async Task<Result<VendorProfile>> Create([FromForm] VendorCreate model)
    {
        try
        {
            if (User.Identity?.Name is null || !User.Identity.IsAuthenticated)
                return new Result<VendorProfile>(ResultCode.Unauthorized, "userNotAuthenticated");
            string? tenancy = await _userService.GetTenantAsync(User.Identity.Name);
            if (tenancy is null)
                return new Result<VendorProfile>(ResultCode.BadRequest, "companyNotRegistered");
            if (!await _userService.CheckPermissionAsync(User.Identity.Name, "vendors.create"))
                return new Result<VendorProfile>(ResultCode.Forbidden, "userNotAuthorized");
            string? currencyId = await _companyService.GetCurrencyAsync(tenancy, model.CurrencyId, c => c.Id);
            if (currencyId is null)
                return new Result<VendorProfile>(ResultCode.BadRequest, "currencyNotFound");
            string? taxId = model.TaxId is null
                ? null
                : await _companyService.GetTaxAsync(tenancy, model.TaxId, t => t.Id);
            string? paymentTermId = model.PaymentTermId is null
                ? null
                : await _companyService.GetPaymentTermAsync(tenancy, model.PaymentTermId, p => p.Id);
            var billingAddress = model.BillingAddress is null ? null : new Address
            {
                Country = model.BillingAddress?.Country,
                City = model.BillingAddress?.City,
                Street = model.BillingAddress?.Street,
                ZipCode = model.BillingAddress?.ZipCode,
                State = model.BillingAddress?.State
            };
            var shippingAddress = billingAddress is not null && model.BillingIsShipping ? billingAddress : model.ShippingAddress is null ? null : new Address
            {
                Country = model.ShippingAddress?.Country,
                City = model.ShippingAddress?.City,
                Street = model.ShippingAddress?.Street,
                ZipCode = model.ShippingAddress?.ZipCode,
                State = model.ShippingAddress?.State
            };
            var vendor = new Vendor
            {
                FirstName = model.FirstName, LastName = model.LastName, DisplayName = model.DisplayName,
                CompanyName = model.CompanyName ?? string.Empty,
                Email = model.Email, Phone = model.Phone, Website = model.Website, Remarks = model.Remarks,
                CurrencyId = currencyId, TaxId = taxId, PaymentTermId = paymentTermId,
                BillingAddress = billingAddress, ShippingAddress = shippingAddress
            };
            return await _vendorService.AddVendorAsync(tenancy, vendor)
                ? new Result<VendorProfile>(new VendorProfile(vendor)
                {
                    CurrencyId = model.CurrencyId,
                    TaxId = model.TaxId,
                    PaymentTermId = model.PaymentTermId
                }, ResultCode.Created)
                : new Result<VendorProfile>(ResultCode.BadRequest, "vendorNotCreated");
        }
        catch (Exception e)
        {
            return new Result<VendorProfile>(ResultCode.InternalServerError, e.Message);
        }
    }

    [Produces(ResponseType)]
    [HttpPut]
    [Route("{id}")]
    public async Task<Result<VendorProfile>> Edit([FromRoute] string? id, [FromForm] VendorCreate model)
    {
        try
        {
            if (id is null)
                return new Result<VendorProfile>(ResultCode.BadRequest, "idNotProvided");
            if (User.Identity?.Name is null || !User.Identity.IsAuthenticated)
                return new Result<VendorProfile>(ResultCode.Unauthorized, "userNotAuthenticated");
            string? tenancy = await _userService.GetTenantAsync(User.Identity.Name);
            if (tenancy is null)
                return new Result<VendorProfile>(ResultCode.BadRequest, "companyNotRegistered");
            if (!await _userService.CheckPermissionAsync(User.Identity.Name, "vendors.edit"))
                return new Result<VendorProfile>(ResultCode.Forbidden, "userNotAuthorized");
            string? currencyId = await _companyService.GetCurrencyAsync(tenancy, model.CurrencyId, c => c.Id);
            if (currencyId is null)
                return new Result<VendorProfile>(ResultCode.BadRequest, "currencyNotFound");
            string? taxId = model.TaxId is null
                ? null
                : await _companyService.GetTaxAsync(tenancy, model.TaxId, t => t.Id);
            string? paymentTermId = model.PaymentTermId is null
                ? null
                : await _companyService.GetPaymentTermAsync(tenancy, model.PaymentTermId, p => p.Id);
            var billingAddress = model.BillingAddress is null
                ? null
                : new Address
                {
                    Country = model.BillingAddress?.Country,
                    City = model.BillingAddress?.City,
                    Street = model.BillingAddress?.Street,
                    ZipCode = model.BillingAddress?.ZipCode,
                    State = model.BillingAddress?.State
                };
            var shippingAddress = billingAddress is not null && model.BillingIsShipping ? billingAddress :
                model.ShippingAddress is null ? null : new Address
                {
                    Country = model.ShippingAddress?.Country,
                    City = model.ShippingAddress?.City,
                    Street = model.ShippingAddress?.Street,
                    ZipCode = model.ShippingAddress?.ZipCode,
                    State = model.ShippingAddress?.State
                };
            var vendor = await _vendorService.GetVendorAsync(tenancy, id);
            if (vendor is null)
                return new Result<VendorProfile>(ResultCode.BadRequest, "vendorNotFound");
            vendor.FirstName = model.FirstName;
            vendor.LastName = model.LastName;
            vendor.DisplayName = model.DisplayName;
            vendor.CompanyName = model.CompanyName ?? string.Empty;
            vendor.Email = model.Email;
            vendor.Phone = model.Phone;
            vendor.Website = model.Website;
            vendor.Remarks = model.Remarks;
            vendor.CurrencyId = currencyId;
            vendor.TaxId = taxId;
            vendor.PaymentTermId = paymentTermId;
            if (vendor.BillingAddress is not null)
            {
                vendor.BillingAddress.Country = model.BillingAddress?.Country;
                vendor.BillingAddress.City = model.BillingAddress?.City;
                vendor.BillingAddress.Street = model.BillingAddress?.Street;
                vendor.BillingAddress.ZipCode = model.BillingAddress?.ZipCode;
                vendor.BillingAddress.State = model.BillingAddress?.State;
            }
            else vendor.BillingAddress = billingAddress;
            if (vendor.ShippingAddress is not null)
            {
                vendor.ShippingAddress.Country = model.ShippingAddress?.Country;
                vendor.ShippingAddress.City = model.ShippingAddress?.City;
                vendor.ShippingAddress.Street = model.ShippingAddress?.Street;
                vendor.ShippingAddress.ZipCode = model.ShippingAddress?.ZipCode;
                vendor.ShippingAddress.State = model.ShippingAddress?.State;
            }
            else vendor.ShippingAddress = shippingAddress;
            return await _vendorService.UpdateVendorAsync(tenancy, id, vendor)
                ? new Result<VendorProfile>(new VendorProfile(vendor)
                {
                    CurrencyId = model.CurrencyId,
                    TaxId = model.TaxId,
                    PaymentTermId = model.PaymentTermId
                }, ResultCode.NoContent)
                : new Result<VendorProfile>(ResultCode.BadRequest, "vendorNotUpdated");
        }
        catch (Exception e)
        {
            return new Result<VendorProfile>(ResultCode.InternalServerError, e.Message);
        }
    }
}