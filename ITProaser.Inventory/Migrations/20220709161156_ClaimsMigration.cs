﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ITProaser.Inventory.Migrations
{
    public partial class ClaimsMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Company.Services_Clients_ClientId",
                schema: "Organization",
                table: "Company.Services");

            migrationBuilder.DropForeignKey(
                name: "FK_Company.Services_Companies_CompanyId",
                schema: "Organization",
                table: "Company.Services");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Company.Services",
                schema: "Organization",
                table: "Company.Services");

            migrationBuilder.RenameTable(
                name: "Company.Services",
                schema: "Organization",
                newName: "Companies.Services",
                newSchema: "Organization");

            migrationBuilder.RenameIndex(
                name: "IX_Company.Services_CompanyId",
                schema: "Organization",
                table: "Companies.Services",
                newName: "IX_Companies.Services_CompanyId");

            migrationBuilder.RenameIndex(
                name: "IX_Company.Services_ClientId",
                schema: "Organization",
                table: "Companies.Services",
                newName: "IX_Companies.Services_ClientId");

            migrationBuilder.AddColumn<bool>(
                name: "IsProtected",
                schema: "Security",
                table: "Users.Claims",
                type: "boolean",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsAction",
                schema: "Security",
                table: "Roles.Claims",
                type: "boolean",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsProtected",
                schema: "Security",
                table: "Roles",
                type: "boolean",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Companies.Services",
                schema: "Organization",
                table: "Companies.Services",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_CompaniesServices_UId",
                schema: "Organization",
                table: "Companies.Services",
                column: "UId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Companies.Services_Clients_ClientId",
                schema: "Organization",
                table: "Companies.Services",
                column: "ClientId",
                principalSchema: "Services",
                principalTable: "Clients",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Companies.Services_Companies_CompanyId",
                schema: "Organization",
                table: "Companies.Services",
                column: "CompanyId",
                principalSchema: "Organization",
                principalTable: "Companies",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Companies.Services_Clients_ClientId",
                schema: "Organization",
                table: "Companies.Services");

            migrationBuilder.DropForeignKey(
                name: "FK_Companies.Services_Companies_CompanyId",
                schema: "Organization",
                table: "Companies.Services");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Companies.Services",
                schema: "Organization",
                table: "Companies.Services");

            migrationBuilder.DropIndex(
                name: "IX_CompaniesServices_UId",
                schema: "Organization",
                table: "Companies.Services");

            migrationBuilder.DropColumn(
                name: "IsProtected",
                schema: "Security",
                table: "Users.Claims");

            migrationBuilder.DropColumn(
                name: "IsAction",
                schema: "Security",
                table: "Roles.Claims");

            migrationBuilder.DropColumn(
                name: "IsProtected",
                schema: "Security",
                table: "Roles");

            migrationBuilder.RenameTable(
                name: "Companies.Services",
                schema: "Organization",
                newName: "Company.Services",
                newSchema: "Organization");

            migrationBuilder.RenameIndex(
                name: "IX_Companies.Services_CompanyId",
                schema: "Organization",
                table: "Company.Services",
                newName: "IX_Company.Services_CompanyId");

            migrationBuilder.RenameIndex(
                name: "IX_Companies.Services_ClientId",
                schema: "Organization",
                table: "Company.Services",
                newName: "IX_Company.Services_ClientId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Company.Services",
                schema: "Organization",
                table: "Company.Services",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Company.Services_Clients_ClientId",
                schema: "Organization",
                table: "Company.Services",
                column: "ClientId",
                principalSchema: "Services",
                principalTable: "Clients",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Company.Services_Companies_CompanyId",
                schema: "Organization",
                table: "Company.Services",
                column: "CompanyId",
                principalSchema: "Organization",
                principalTable: "Companies",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
