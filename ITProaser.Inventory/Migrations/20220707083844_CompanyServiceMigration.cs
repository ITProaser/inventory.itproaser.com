﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ITProaser.Inventory.Migrations
{
    public partial class CompanyServiceMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "Services");

            migrationBuilder.CreateTable(
                name: "Clients",
                schema: "Services",
                columns: table => new
                {
                    Id = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    ClientId = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    Name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    Description = table.Column<string>(type: "character varying(128)", maxLength: 128, nullable: true),
                    ClientUri = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    RedirectUris = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: false),
                    UId = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Clients", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Company.Services",
                schema: "Organization",
                columns: table => new
                {
                    Id = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    ClientId = table.Column<string>(type: "character varying(64)", nullable: false),
                    UId = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: false),
                    CompanyId = table.Column<string>(type: "character varying(64)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Company.Services", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Company.Services_Clients_ClientId",
                        column: x => x.ClientId,
                        principalSchema: "Services",
                        principalTable: "Clients",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Company.Services_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalSchema: "Organization",
                        principalTable: "Companies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ServiceClients_UId",
                schema: "Services",
                table: "Clients",
                columns: new[] { "UId", "ClientId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Company.Services_ClientId",
                schema: "Organization",
                table: "Company.Services",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_Company.Services_CompanyId",
                schema: "Organization",
                table: "Company.Services",
                column: "CompanyId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Company.Services",
                schema: "Organization");

            migrationBuilder.DropTable(
                name: "Clients",
                schema: "Services");
        }
    }
}
