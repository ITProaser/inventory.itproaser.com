﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ITProaser.Inventory.Migrations
{
    public partial class PurchaseSalesMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "Sales");

            migrationBuilder.EnsureSchema(
                name: "Purchases");

            migrationBuilder.CreateTable(
                name: "PaymentTerms",
                schema: "Finance",
                columns: table => new
                {
                    Id = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    Label = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    NormalizedLabel = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    Days = table.Column<int>(type: "integer", nullable: false),
                    IsDefault = table.Column<bool>(type: "boolean", nullable: false),
                    UId = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: false),
                    CompanyId = table.Column<string>(type: "character varying(64)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PaymentTerms", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PaymentTerms_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalSchema: "Organization",
                        principalTable: "Companies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Customer",
                schema: "Sales",
                columns: table => new
                {
                    Id = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    Type = table.Column<int>(type: "integer", nullable: false),
                    Remarks = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: true),
                    Source = table.Column<int>(type: "integer", nullable: false),
                    Birthdate = table.Column<DateOnly>(type: "date", nullable: true),
                    Assistant = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    AssistantPhone = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    OwnerId = table.Column<string>(type: "text", nullable: false),
                    PaymentTermId = table.Column<string>(type: "character varying(64)", nullable: true),
                    CurrencyId = table.Column<string>(type: "character varying(64)", nullable: true),
                    TaxId = table.Column<string>(type: "character varying(64)", nullable: true),
                    BillingAddressId = table.Column<string>(type: "character varying(64)", nullable: true),
                    ShippingAddressId = table.Column<string>(type: "character varying(64)", nullable: true),
                    UId = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: false),
                    CompanyId = table.Column<string>(type: "character varying(64)", nullable: false),
                    FirstName = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    NormalizedFirstName = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    LastName = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    NormalizedLastName = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    DisplayName = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    NormalizedDisplayName = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    CompanyName = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    Title = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    Email = table.Column<string>(type: "character varying(128)", maxLength: 128, nullable: true),
                    NormalizedEmail = table.Column<string>(type: "character varying(128)", maxLength: 128, nullable: true),
                    Phone = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    Mobile = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    Website = table.Column<string>(type: "character varying(128)", maxLength: 128, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Customer", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Customer_Addresses_BillingAddressId",
                        column: x => x.BillingAddressId,
                        principalSchema: "Contacts",
                        principalTable: "Addresses",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Customer_Addresses_ShippingAddressId",
                        column: x => x.ShippingAddressId,
                        principalSchema: "Contacts",
                        principalTable: "Addresses",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Customer_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalSchema: "Organization",
                        principalTable: "Companies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Customer_Currencies_CurrencyId",
                        column: x => x.CurrencyId,
                        principalSchema: "Finance",
                        principalTable: "Currencies",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Customer_PaymentTerms_PaymentTermId",
                        column: x => x.PaymentTermId,
                        principalSchema: "Finance",
                        principalTable: "PaymentTerms",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Customer_Taxes_TaxId",
                        column: x => x.TaxId,
                        principalSchema: "Finance",
                        principalTable: "Taxes",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Customer_Users_OwnerId",
                        column: x => x.OwnerId,
                        principalSchema: "Security",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Vendors",
                schema: "Purchases",
                columns: table => new
                {
                    Id = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    Remarks = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: true),
                    PaymentTermId = table.Column<string>(type: "character varying(64)", nullable: true),
                    CurrencyId = table.Column<string>(type: "character varying(64)", nullable: true),
                    TaxId = table.Column<string>(type: "character varying(64)", nullable: true),
                    BillingAddressId = table.Column<string>(type: "character varying(64)", nullable: true),
                    ShippingAddressId = table.Column<string>(type: "character varying(64)", nullable: true),
                    UId = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: false),
                    CompanyId = table.Column<string>(type: "character varying(64)", nullable: false),
                    FirstName = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    NormalizedFirstName = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    LastName = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    NormalizedLastName = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    DisplayName = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    NormalizedDisplayName = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    CompanyName = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    Title = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    Email = table.Column<string>(type: "character varying(128)", maxLength: 128, nullable: true),
                    NormalizedEmail = table.Column<string>(type: "character varying(128)", maxLength: 128, nullable: true),
                    Phone = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    Mobile = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    Website = table.Column<string>(type: "character varying(128)", maxLength: 128, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Vendors", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Vendors_Addresses_BillingAddressId",
                        column: x => x.BillingAddressId,
                        principalSchema: "Contacts",
                        principalTable: "Addresses",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Vendors_Addresses_ShippingAddressId",
                        column: x => x.ShippingAddressId,
                        principalSchema: "Contacts",
                        principalTable: "Addresses",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Vendors_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalSchema: "Organization",
                        principalTable: "Companies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Vendors_Currencies_CurrencyId",
                        column: x => x.CurrencyId,
                        principalSchema: "Finance",
                        principalTable: "Currencies",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Vendors_PaymentTerms_PaymentTermId",
                        column: x => x.PaymentTermId,
                        principalSchema: "Finance",
                        principalTable: "PaymentTerms",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Vendors_Taxes_TaxId",
                        column: x => x.TaxId,
                        principalSchema: "Finance",
                        principalTable: "Taxes",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "Orders",
                schema: "Sales",
                columns: table => new
                {
                    Id = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    CustomerId = table.Column<string>(type: "character varying(64)", nullable: false),
                    UId = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: false),
                    CompanyId = table.Column<string>(type: "character varying(64)", nullable: false),
                    Number = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    Reference = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    Date = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    ExpectedDate = table.Column<DateOnly>(type: "date", nullable: true),
                    Subtotal = table.Column<decimal>(type: "numeric(12,4)", precision: 12, scale: 4, nullable: false),
                    Discount = table.Column<decimal>(type: "numeric(12,4)", precision: 12, scale: 4, nullable: false),
                    DiscountTotal = table.Column<decimal>(type: "numeric(12,4)", precision: 12, scale: 4, nullable: false),
                    TaxTotal = table.Column<decimal>(type: "numeric(12,4)", precision: 12, scale: 4, nullable: false),
                    DiscountType = table.Column<int>(type: "integer", nullable: false),
                    DiscountLevel = table.Column<int>(type: "integer", nullable: false),
                    Adjustment = table.Column<decimal>(type: "numeric(12,4)", precision: 12, scale: 4, nullable: false),
                    Total = table.Column<decimal>(type: "numeric(12,4)", precision: 12, scale: 4, nullable: false),
                    Status = table.Column<int>(type: "integer", nullable: false),
                    CustomerNotes = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
                    TermsAndConditions = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
                    CurrencyId = table.Column<string>(type: "character varying(64)", nullable: true),
                    PaymentTermId = table.Column<string>(type: "character varying(64)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Orders", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Orders_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalSchema: "Organization",
                        principalTable: "Companies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Orders_Currencies_CurrencyId",
                        column: x => x.CurrencyId,
                        principalSchema: "Finance",
                        principalTable: "Currencies",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Orders_Customer_CustomerId",
                        column: x => x.CustomerId,
                        principalSchema: "Sales",
                        principalTable: "Customer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Orders_PaymentTerms_PaymentTermId",
                        column: x => x.PaymentTermId,
                        principalSchema: "Finance",
                        principalTable: "PaymentTerms",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "Orders",
                schema: "Purchases",
                columns: table => new
                {
                    Id = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    VendorId = table.Column<string>(type: "character varying(64)", nullable: false),
                    UId = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: false),
                    CompanyId = table.Column<string>(type: "character varying(64)", nullable: false),
                    Number = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    Reference = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    Date = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    ExpectedDate = table.Column<DateOnly>(type: "date", nullable: true),
                    Subtotal = table.Column<decimal>(type: "numeric(12,4)", precision: 12, scale: 4, nullable: false),
                    Discount = table.Column<decimal>(type: "numeric(12,4)", precision: 12, scale: 4, nullable: false),
                    DiscountTotal = table.Column<decimal>(type: "numeric(12,4)", precision: 12, scale: 4, nullable: false),
                    TaxTotal = table.Column<decimal>(type: "numeric(12,4)", precision: 12, scale: 4, nullable: false),
                    DiscountType = table.Column<int>(type: "integer", nullable: false),
                    DiscountLevel = table.Column<int>(type: "integer", nullable: false),
                    Adjustment = table.Column<decimal>(type: "numeric(12,4)", precision: 12, scale: 4, nullable: false),
                    Total = table.Column<decimal>(type: "numeric(12,4)", precision: 12, scale: 4, nullable: false),
                    Status = table.Column<int>(type: "integer", nullable: false),
                    CustomerNotes = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
                    TermsAndConditions = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
                    CurrencyId = table.Column<string>(type: "character varying(64)", nullable: true),
                    PaymentTermId = table.Column<string>(type: "character varying(64)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Orders", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Orders_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalSchema: "Organization",
                        principalTable: "Companies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Orders_Currencies_CurrencyId",
                        column: x => x.CurrencyId,
                        principalSchema: "Finance",
                        principalTable: "Currencies",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Orders_PaymentTerms_PaymentTermId",
                        column: x => x.PaymentTermId,
                        principalSchema: "Finance",
                        principalTable: "PaymentTerms",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Orders_Vendors_VendorId",
                        column: x => x.VendorId,
                        principalSchema: "Purchases",
                        principalTable: "Vendors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Orders.Lines",
                schema: "Sales",
                columns: table => new
                {
                    Id = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    SaleOrderId = table.Column<string>(type: "character varying(64)", nullable: false),
                    UId = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: false),
                    Line = table.Column<int>(type: "integer", nullable: false),
                    Quantity = table.Column<decimal>(type: "numeric(12,4)", precision: 12, scale: 4, nullable: false),
                    Price = table.Column<decimal>(type: "numeric(12,4)", precision: 12, scale: 4, nullable: false),
                    TaxTotal = table.Column<decimal>(type: "numeric(12,4)", precision: 12, scale: 4, nullable: false),
                    DiscountType = table.Column<int>(type: "integer", nullable: false),
                    Discount = table.Column<decimal>(type: "numeric(12,4)", precision: 12, scale: 4, nullable: false),
                    DiscountTotal = table.Column<decimal>(type: "numeric(12,4)", precision: 12, scale: 4, nullable: false),
                    Subtotal = table.Column<decimal>(type: "numeric(12,4)", precision: 12, scale: 4, nullable: false),
                    Total = table.Column<decimal>(type: "numeric(12,4)", precision: 12, scale: 4, nullable: false),
                    Notes = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
                    ItemId = table.Column<string>(type: "character varying(64)", nullable: false),
                    TaxId = table.Column<string>(type: "character varying(64)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Orders.Lines", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Orders.Lines_Items_ItemId",
                        column: x => x.ItemId,
                        principalSchema: "Inventory",
                        principalTable: "Items",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Orders.Lines_Orders_SaleOrderId",
                        column: x => x.SaleOrderId,
                        principalSchema: "Sales",
                        principalTable: "Orders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Orders.Lines_Taxes_TaxId",
                        column: x => x.TaxId,
                        principalSchema: "Finance",
                        principalTable: "Taxes",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "Orders.Lines",
                schema: "Purchases",
                columns: table => new
                {
                    Id = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    PurchaseOrderId = table.Column<string>(type: "character varying(64)", nullable: false),
                    UId = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: false),
                    Line = table.Column<int>(type: "integer", nullable: false),
                    Quantity = table.Column<decimal>(type: "numeric(12,4)", precision: 12, scale: 4, nullable: false),
                    Price = table.Column<decimal>(type: "numeric(12,4)", precision: 12, scale: 4, nullable: false),
                    TaxTotal = table.Column<decimal>(type: "numeric(12,4)", precision: 12, scale: 4, nullable: false),
                    DiscountType = table.Column<int>(type: "integer", nullable: false),
                    Discount = table.Column<decimal>(type: "numeric(12,4)", precision: 12, scale: 4, nullable: false),
                    DiscountTotal = table.Column<decimal>(type: "numeric(12,4)", precision: 12, scale: 4, nullable: false),
                    Subtotal = table.Column<decimal>(type: "numeric(12,4)", precision: 12, scale: 4, nullable: false),
                    Total = table.Column<decimal>(type: "numeric(12,4)", precision: 12, scale: 4, nullable: false),
                    Notes = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
                    ItemId = table.Column<string>(type: "character varying(64)", nullable: false),
                    TaxId = table.Column<string>(type: "character varying(64)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Orders.Lines", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Orders.Lines_Items_ItemId",
                        column: x => x.ItemId,
                        principalSchema: "Inventory",
                        principalTable: "Items",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Orders.Lines_Orders_PurchaseOrderId",
                        column: x => x.PurchaseOrderId,
                        principalSchema: "Purchases",
                        principalTable: "Orders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Orders.Lines_Taxes_TaxId",
                        column: x => x.TaxId,
                        principalSchema: "Finance",
                        principalTable: "Taxes",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateIndex(
                name: "IX_Contacts_UId",
                schema: "Sales",
                table: "Customer",
                columns: new[] { "UId", "NormalizedDisplayName", "Source" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Customer_BillingAddressId",
                schema: "Sales",
                table: "Customer",
                column: "BillingAddressId");

            migrationBuilder.CreateIndex(
                name: "IX_Customer_CompanyId",
                schema: "Sales",
                table: "Customer",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_Customer_CurrencyId",
                schema: "Sales",
                table: "Customer",
                column: "CurrencyId");

            migrationBuilder.CreateIndex(
                name: "IX_Customer_OwnerId",
                schema: "Sales",
                table: "Customer",
                column: "OwnerId");

            migrationBuilder.CreateIndex(
                name: "IX_Customer_PaymentTermId",
                schema: "Sales",
                table: "Customer",
                column: "PaymentTermId");

            migrationBuilder.CreateIndex(
                name: "IX_Customer_ShippingAddressId",
                schema: "Sales",
                table: "Customer",
                column: "ShippingAddressId");

            migrationBuilder.CreateIndex(
                name: "IX_Customer_TaxId",
                schema: "Sales",
                table: "Customer",
                column: "TaxId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_CompanyId",
                schema: "Purchases",
                table: "Orders",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_CurrencyId",
                schema: "Purchases",
                table: "Orders",
                column: "CurrencyId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_PaymentTermId",
                schema: "Purchases",
                table: "Orders",
                column: "PaymentTermId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_VendorId",
                schema: "Purchases",
                table: "Orders",
                column: "VendorId");

            migrationBuilder.CreateIndex(
                name: "IX_PurchaseOrders_UId",
                schema: "Purchases",
                table: "Orders",
                column: "UId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Orders_CompanyId1",
                schema: "Sales",
                table: "Orders",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_CurrencyId1",
                schema: "Sales",
                table: "Orders",
                column: "CurrencyId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_CustomerId",
                schema: "Sales",
                table: "Orders",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_PaymentTermId1",
                schema: "Sales",
                table: "Orders",
                column: "PaymentTermId");

            migrationBuilder.CreateIndex(
                name: "IX_SalesOrders_UId",
                schema: "Sales",
                table: "Orders",
                column: "UId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Orders.Lines_ItemId",
                schema: "Purchases",
                table: "Orders.Lines",
                column: "ItemId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders.Lines_PurchaseOrderId",
                schema: "Purchases",
                table: "Orders.Lines",
                column: "PurchaseOrderId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders.Lines_TaxId",
                schema: "Purchases",
                table: "Orders.Lines",
                column: "TaxId");

            migrationBuilder.CreateIndex(
                name: "IX_PurchaseOrderLines_UId",
                schema: "Purchases",
                table: "Orders.Lines",
                columns: new[] { "UId", "Line" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Orders.Lines_ItemId1",
                schema: "Sales",
                table: "Orders.Lines",
                column: "ItemId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders.Lines_SaleOrderId",
                schema: "Sales",
                table: "Orders.Lines",
                column: "SaleOrderId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders.Lines_TaxId1",
                schema: "Sales",
                table: "Orders.Lines",
                column: "TaxId");

            migrationBuilder.CreateIndex(
                name: "IX_SalesOrderLines_UId",
                schema: "Sales",
                table: "Orders.Lines",
                columns: new[] { "UId", "Line" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_PaymentTerms_CompanyId",
                schema: "Finance",
                table: "PaymentTerms",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_PaymentTerms_UId",
                schema: "Finance",
                table: "PaymentTerms",
                columns: new[] { "UId", "NormalizedLabel" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Vendors_BillingAddressId",
                schema: "Purchases",
                table: "Vendors",
                column: "BillingAddressId");

            migrationBuilder.CreateIndex(
                name: "IX_Vendors_CompanyId",
                schema: "Purchases",
                table: "Vendors",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_Vendors_CurrencyId",
                schema: "Purchases",
                table: "Vendors",
                column: "CurrencyId");

            migrationBuilder.CreateIndex(
                name: "IX_Vendors_PaymentTermId",
                schema: "Purchases",
                table: "Vendors",
                column: "PaymentTermId");

            migrationBuilder.CreateIndex(
                name: "IX_Vendors_ShippingAddressId",
                schema: "Purchases",
                table: "Vendors",
                column: "ShippingAddressId");

            migrationBuilder.CreateIndex(
                name: "IX_Vendors_TaxId",
                schema: "Purchases",
                table: "Vendors",
                column: "TaxId");

            migrationBuilder.CreateIndex(
                name: "IX_Vendors_UId",
                schema: "Purchases",
                table: "Vendors",
                columns: new[] { "UId", "NormalizedDisplayName" },
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Orders.Lines",
                schema: "Purchases");

            migrationBuilder.DropTable(
                name: "Orders.Lines",
                schema: "Sales");

            migrationBuilder.DropTable(
                name: "Orders",
                schema: "Purchases");

            migrationBuilder.DropTable(
                name: "Orders",
                schema: "Sales");

            migrationBuilder.DropTable(
                name: "Vendors",
                schema: "Purchases");

            migrationBuilder.DropTable(
                name: "Customer",
                schema: "Sales");

            migrationBuilder.DropTable(
                name: "PaymentTerms",
                schema: "Finance");
        }
    }
}
