﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace ITProaser.Inventory.Migrations
{
    public partial class InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "Contacts");

            migrationBuilder.EnsureSchema(
                name: "Organization");

            migrationBuilder.EnsureSchema(
                name: "Finance");

            migrationBuilder.EnsureSchema(
                name: "Inventory");

            migrationBuilder.EnsureSchema(
                name: "Security");

            migrationBuilder.CreateTable(
                name: "Addresses",
                schema: "Contacts",
                columns: table => new
                {
                    Id = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    Street = table.Column<string>(type: "character varying(128)", maxLength: 128, nullable: true),
                    State = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    City = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    Country = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    ZipCode = table.Column<string>(type: "character varying(12)", maxLength: 12, nullable: true),
                    UId = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Addresses", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DataProtectionKeys",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    FriendlyName = table.Column<string>(type: "text", nullable: true),
                    Xml = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DataProtectionKeys", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Companies",
                schema: "Organization",
                columns: table => new
                {
                    Id = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    Name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    NormalizedName = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    CompanyId = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    Industry = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    AddressId = table.Column<string>(type: "character varying(64)", nullable: false),
                    NormalizedIndustry = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    UId = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Companies", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Companies_Addresses_AddressId",
                        column: x => x.AddressId,
                        principalSchema: "Contacts",
                        principalTable: "Addresses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Currencies",
                schema: "Finance",
                columns: table => new
                {
                    Id = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    Name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    NormalizedName = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    Symbol = table.Column<string>(type: "character varying(3)", maxLength: 3, nullable: false),
                    Code = table.Column<string>(type: "character varying(3)", maxLength: 3, nullable: false),
                    DecimalPlaces = table.Column<int>(type: "integer", nullable: false),
                    IsDefault = table.Column<bool>(type: "boolean", nullable: false),
                    UId = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: false),
                    CompanyId = table.Column<string>(type: "character varying(64)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Currencies", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Currencies_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalSchema: "Organization",
                        principalTable: "Companies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Items.Brands",
                schema: "Inventory",
                columns: table => new
                {
                    Id = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    Name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    NormalizedName = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    Description = table.Column<string>(type: "character varying(128)", maxLength: 128, nullable: true),
                    UId = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: false),
                    CompanyId = table.Column<string>(type: "character varying(64)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Items.Brands", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Items.Brands_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalSchema: "Organization",
                        principalTable: "Companies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Items.Categories",
                schema: "Inventory",
                columns: table => new
                {
                    Id = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    Name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    NormalizedName = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    Description = table.Column<string>(type: "character varying(128)", maxLength: 128, nullable: true),
                    UId = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: false),
                    CompanyId = table.Column<string>(type: "character varying(64)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Items.Categories", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Items.Categories_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalSchema: "Organization",
                        principalTable: "Companies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Items.Manufacturers",
                schema: "Inventory",
                columns: table => new
                {
                    Id = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    Name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    NormalizedName = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    Description = table.Column<string>(type: "character varying(128)", maxLength: 128, nullable: true),
                    UId = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: false),
                    CompanyId = table.Column<string>(type: "character varying(64)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Items.Manufacturers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Items.Manufacturers_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalSchema: "Organization",
                        principalTable: "Companies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Items.Units",
                schema: "Inventory",
                columns: table => new
                {
                    Id = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    Name = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: false),
                    NormalizedName = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: false),
                    Description = table.Column<string>(type: "character varying(128)", maxLength: 128, nullable: true),
                    UId = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: false),
                    CompanyId = table.Column<string>(type: "character varying(64)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Items.Units", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Items.Units_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalSchema: "Organization",
                        principalTable: "Companies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Roles",
                schema: "Security",
                columns: table => new
                {
                    Id = table.Column<string>(type: "text", nullable: false),
                    UId = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: false),
                    Description = table.Column<string>(type: "text", nullable: true),
                    CompanyId = table.Column<string>(type: "character varying(64)", nullable: true),
                    Name = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Roles", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Roles_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalSchema: "Organization",
                        principalTable: "Companies",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "Taxes.Groups",
                schema: "Finance",
                columns: table => new
                {
                    Id = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    Name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    NormalizedName = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    Description = table.Column<string>(type: "character varying(128)", maxLength: 128, nullable: true),
                    UId = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: false),
                    CompanyId = table.Column<string>(type: "character varying(64)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Taxes.Groups", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Taxes.Groups_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalSchema: "Organization",
                        principalTable: "Companies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                schema: "Security",
                columns: table => new
                {
                    Id = table.Column<string>(type: "text", nullable: false),
                    UId = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: false),
                    FirstName = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    NormalizedFirstName = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    LastName = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    NormalizedLastName = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    DisplayName = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    NormalizedDisplayName = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    CompanyId = table.Column<string>(type: "character varying(64)", nullable: true),
                    UserName = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
                    Email = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(type: "boolean", nullable: false),
                    PasswordHash = table.Column<string>(type: "text", nullable: true),
                    SecurityStamp = table.Column<string>(type: "text", nullable: true),
                    ConcurrencyStamp = table.Column<string>(type: "text", nullable: true),
                    PhoneNumber = table.Column<string>(type: "text", nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(type: "boolean", nullable: false),
                    TwoFactorEnabled = table.Column<bool>(type: "boolean", nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true),
                    LockoutEnabled = table.Column<bool>(type: "boolean", nullable: false),
                    AccessFailedCount = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Users_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalSchema: "Organization",
                        principalTable: "Companies",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "Roles.Claims",
                schema: "Security",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    RoleId = table.Column<string>(type: "text", nullable: false),
                    ClaimType = table.Column<string>(type: "text", nullable: true),
                    ClaimValue = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Roles.Claims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Roles.Claims_Roles_RoleId",
                        column: x => x.RoleId,
                        principalSchema: "Security",
                        principalTable: "Roles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Taxes",
                schema: "Finance",
                columns: table => new
                {
                    Id = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    Name = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    NormalizedName = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    Rate = table.Column<decimal>(type: "numeric(12,4)", precision: 12, scale: 4, nullable: false),
                    Description = table.Column<string>(type: "character varying(128)", maxLength: 128, nullable: true),
                    IsCompound = table.Column<bool>(type: "boolean", nullable: false),
                    IsGroup = table.Column<bool>(type: "boolean", nullable: false),
                    TaxGroupId = table.Column<string>(type: "character varying(64)", nullable: true),
                    UId = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: false),
                    CompanyId = table.Column<string>(type: "character varying(64)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Taxes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Taxes_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalSchema: "Organization",
                        principalTable: "Companies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Taxes_Taxes.Groups_TaxGroupId",
                        column: x => x.TaxGroupId,
                        principalSchema: "Finance",
                        principalTable: "Taxes.Groups",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "Users.Claims",
                schema: "Security",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    UserId = table.Column<string>(type: "text", nullable: false),
                    ClaimType = table.Column<string>(type: "text", nullable: true),
                    ClaimValue = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users.Claims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Users.Claims_Users_UserId",
                        column: x => x.UserId,
                        principalSchema: "Security",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Users.Logins",
                schema: "Security",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(type: "text", nullable: false),
                    ProviderKey = table.Column<string>(type: "text", nullable: false),
                    ProviderDisplayName = table.Column<string>(type: "text", nullable: true),
                    UserId = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users.Logins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_Users.Logins_Users_UserId",
                        column: x => x.UserId,
                        principalSchema: "Security",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Users.Roles",
                schema: "Security",
                columns: table => new
                {
                    UserId = table.Column<string>(type: "text", nullable: false),
                    RoleId = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users.Roles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_Users.Roles_Roles_RoleId",
                        column: x => x.RoleId,
                        principalSchema: "Security",
                        principalTable: "Roles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Users.Roles_Users_UserId",
                        column: x => x.UserId,
                        principalSchema: "Security",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Users.Tokens",
                schema: "Security",
                columns: table => new
                {
                    UserId = table.Column<string>(type: "text", nullable: false),
                    LoginProvider = table.Column<string>(type: "text", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: false),
                    Value = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users.Tokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_Users.Tokens_Users_UserId",
                        column: x => x.UserId,
                        principalSchema: "Security",
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Items.Groups",
                schema: "Inventory",
                columns: table => new
                {
                    Id = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    Type = table.Column<int>(type: "integer", nullable: false),
                    Name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    NormalizedName = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    Description = table.Column<string>(type: "character varying(128)", maxLength: 128, nullable: true),
                    UnitId = table.Column<string>(type: "character varying(64)", nullable: true),
                    ManufacturerId = table.Column<string>(type: "character varying(64)", nullable: true),
                    BrandId = table.Column<string>(type: "character varying(64)", nullable: true),
                    TaxId = table.Column<string>(type: "character varying(64)", nullable: true),
                    UId = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: false),
                    CompanyId = table.Column<string>(type: "character varying(64)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Items.Groups", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Items.Groups_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalSchema: "Organization",
                        principalTable: "Companies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Items.Groups_Items.Brands_BrandId",
                        column: x => x.BrandId,
                        principalSchema: "Inventory",
                        principalTable: "Items.Brands",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Items.Groups_Items.Manufacturers_ManufacturerId",
                        column: x => x.ManufacturerId,
                        principalSchema: "Inventory",
                        principalTable: "Items.Manufacturers",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Items.Groups_Items.Units_UnitId",
                        column: x => x.UnitId,
                        principalSchema: "Inventory",
                        principalTable: "Items.Units",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Items.Groups_Taxes_TaxId",
                        column: x => x.TaxId,
                        principalSchema: "Finance",
                        principalTable: "Taxes",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "Items.TransactionInfo",
                schema: "Inventory",
                columns: table => new
                {
                    Id = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    CurrencyId = table.Column<string>(type: "character varying(64)", nullable: false),
                    Rate = table.Column<decimal>(type: "numeric(12,4)", precision: 12, scale: 4, nullable: false),
                    TaxId = table.Column<string>(type: "character varying(64)", nullable: true),
                    Description = table.Column<string>(type: "character varying(128)", maxLength: 128, nullable: true),
                    UId = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: false),
                    CompanyId = table.Column<string>(type: "character varying(64)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Items.TransactionInfo", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Items.TransactionInfo_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalSchema: "Organization",
                        principalTable: "Companies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Items.TransactionInfo_Currencies_CurrencyId",
                        column: x => x.CurrencyId,
                        principalSchema: "Finance",
                        principalTable: "Currencies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Items.TransactionInfo_Taxes_TaxId",
                        column: x => x.TaxId,
                        principalSchema: "Finance",
                        principalTable: "Taxes",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "Taxes.Groups.Lines",
                schema: "Finance",
                columns: table => new
                {
                    Id = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    Line = table.Column<int>(type: "integer", nullable: false),
                    TaxId = table.Column<string>(type: "character varying(64)", nullable: false),
                    TaxGroupId = table.Column<string>(type: "character varying(64)", nullable: false),
                    UId = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: false),
                    CompanyId = table.Column<string>(type: "character varying(64)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Taxes.Groups.Lines", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Taxes.Groups.Lines_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalSchema: "Organization",
                        principalTable: "Companies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Taxes.Groups.Lines_Taxes_TaxId",
                        column: x => x.TaxId,
                        principalSchema: "Finance",
                        principalTable: "Taxes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Taxes.Groups.Lines_Taxes.Groups_TaxGroupId",
                        column: x => x.TaxGroupId,
                        principalSchema: "Finance",
                        principalTable: "Taxes.Groups",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Items",
                schema: "Inventory",
                columns: table => new
                {
                    Id = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    Type = table.Column<int>(type: "integer", nullable: false),
                    Name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    NormalizedName = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    Sku = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    UPC = table.Column<string>(type: "character varying(12)", maxLength: 12, nullable: true),
                    EAN = table.Column<string>(type: "character varying(13)", maxLength: 13, nullable: true),
                    MPN = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    ISBN = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    IsReturnable = table.Column<bool>(type: "boolean", nullable: false),
                    Notes = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: true),
                    CategoryId = table.Column<string>(type: "character varying(64)", nullable: true),
                    UnitId = table.Column<string>(type: "character varying(64)", nullable: true),
                    ManufacturerId = table.Column<string>(type: "character varying(64)", nullable: true),
                    BrandId = table.Column<string>(type: "character varying(64)", nullable: true),
                    GroupId = table.Column<string>(type: "character varying(64)", nullable: true),
                    PurchaseInformationId = table.Column<string>(type: "character varying(64)", nullable: true),
                    SalesInformationId = table.Column<string>(type: "character varying(64)", nullable: true),
                    UId = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: false),
                    CompanyId = table.Column<string>(type: "character varying(64)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Items", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Items_Companies_CompanyId",
                        column: x => x.CompanyId,
                        principalSchema: "Organization",
                        principalTable: "Companies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Items_Items.Brands_BrandId",
                        column: x => x.BrandId,
                        principalSchema: "Inventory",
                        principalTable: "Items.Brands",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Items_Items.Categories_CategoryId",
                        column: x => x.CategoryId,
                        principalSchema: "Inventory",
                        principalTable: "Items.Categories",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Items_Items.Groups_GroupId",
                        column: x => x.GroupId,
                        principalSchema: "Inventory",
                        principalTable: "Items.Groups",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Items_Items.Manufacturers_ManufacturerId",
                        column: x => x.ManufacturerId,
                        principalSchema: "Inventory",
                        principalTable: "Items.Manufacturers",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Items_Items.TransactionInfo_PurchaseInformationId",
                        column: x => x.PurchaseInformationId,
                        principalSchema: "Inventory",
                        principalTable: "Items.TransactionInfo",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Items_Items.TransactionInfo_SalesInformationId",
                        column: x => x.SalesInformationId,
                        principalSchema: "Inventory",
                        principalTable: "Items.TransactionInfo",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Items_Items.Units_UnitId",
                        column: x => x.UnitId,
                        principalSchema: "Inventory",
                        principalTable: "Items.Units",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateIndex(
                name: "IX_Addresses_UId",
                schema: "Contacts",
                table: "Addresses",
                column: "UId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Companies_AddressId",
                schema: "Organization",
                table: "Companies",
                column: "AddressId");

            migrationBuilder.CreateIndex(
                name: "IX_Companies_UId",
                schema: "Organization",
                table: "Companies",
                columns: new[] { "UId", "NormalizedName", "NormalizedIndustry" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Currencies_CompanyId",
                schema: "Finance",
                table: "Currencies",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_Currencies_UId",
                schema: "Finance",
                table: "Currencies",
                columns: new[] { "UId", "NormalizedName", "Code", "IsDefault" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Items_BrandId",
                schema: "Inventory",
                table: "Items",
                column: "BrandId");

            migrationBuilder.CreateIndex(
                name: "IX_Items_CategoryId",
                schema: "Inventory",
                table: "Items",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Items_CompanyId",
                schema: "Inventory",
                table: "Items",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_Items_GroupId",
                schema: "Inventory",
                table: "Items",
                column: "GroupId");

            migrationBuilder.CreateIndex(
                name: "IX_Items_ManufacturerId",
                schema: "Inventory",
                table: "Items",
                column: "ManufacturerId");

            migrationBuilder.CreateIndex(
                name: "IX_Items_PurchaseInformationId",
                schema: "Inventory",
                table: "Items",
                column: "PurchaseInformationId");

            migrationBuilder.CreateIndex(
                name: "IX_Items_SalesInformationId",
                schema: "Inventory",
                table: "Items",
                column: "SalesInformationId");

            migrationBuilder.CreateIndex(
                name: "IX_Items_UId",
                schema: "Inventory",
                table: "Items",
                columns: new[] { "UId", "Type", "NormalizedName" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Items_UnitId",
                schema: "Inventory",
                table: "Items",
                column: "UnitId");

            migrationBuilder.CreateIndex(
                name: "IX_Items.Brands_CompanyId",
                schema: "Inventory",
                table: "Items.Brands",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_ItemsBrands_UId",
                schema: "Inventory",
                table: "Items.Brands",
                columns: new[] { "UId", "NormalizedName" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Items.Categories_CompanyId",
                schema: "Inventory",
                table: "Items.Categories",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_ItemsCategories_UId",
                schema: "Inventory",
                table: "Items.Categories",
                columns: new[] { "UId", "NormalizedName" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_ItemGroups_UId",
                schema: "Inventory",
                table: "Items.Groups",
                columns: new[] { "UId", "NormalizedName" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Items.Groups_BrandId",
                schema: "Inventory",
                table: "Items.Groups",
                column: "BrandId");

            migrationBuilder.CreateIndex(
                name: "IX_Items.Groups_CompanyId",
                schema: "Inventory",
                table: "Items.Groups",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_Items.Groups_ManufacturerId",
                schema: "Inventory",
                table: "Items.Groups",
                column: "ManufacturerId");

            migrationBuilder.CreateIndex(
                name: "IX_Items.Groups_TaxId",
                schema: "Inventory",
                table: "Items.Groups",
                column: "TaxId");

            migrationBuilder.CreateIndex(
                name: "IX_Items.Groups_UnitId",
                schema: "Inventory",
                table: "Items.Groups",
                column: "UnitId");

            migrationBuilder.CreateIndex(
                name: "IX_Items.Manufacturers_CompanyId",
                schema: "Inventory",
                table: "Items.Manufacturers",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_ItemsManufacturers_UId",
                schema: "Inventory",
                table: "Items.Manufacturers",
                columns: new[] { "UId", "NormalizedName" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Items.TransactionInfo_CompanyId",
                schema: "Inventory",
                table: "Items.TransactionInfo",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_Items.TransactionInfo_CurrencyId",
                schema: "Inventory",
                table: "Items.TransactionInfo",
                column: "CurrencyId");

            migrationBuilder.CreateIndex(
                name: "IX_Items.TransactionInfo_TaxId",
                schema: "Inventory",
                table: "Items.TransactionInfo",
                column: "TaxId");

            migrationBuilder.CreateIndex(
                name: "IX_TransactionInfo_UId",
                schema: "Inventory",
                table: "Items.TransactionInfo",
                column: "UId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Items.Units_CompanyId",
                schema: "Inventory",
                table: "Items.Units",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_ItemsUnits_UId",
                schema: "Inventory",
                table: "Items.Units",
                columns: new[] { "UId", "NormalizedName" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Roles_CompanyId",
                schema: "Security",
                table: "Roles",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                schema: "Security",
                table: "Roles",
                column: "NormalizedName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Roles.Claims_RoleId",
                schema: "Security",
                table: "Roles.Claims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_Taxes_CompanyId",
                schema: "Finance",
                table: "Taxes",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_Taxes_TaxGroupId",
                schema: "Finance",
                table: "Taxes",
                column: "TaxGroupId");

            migrationBuilder.CreateIndex(
                name: "IX_Taxes_UId",
                schema: "Finance",
                table: "Taxes",
                columns: new[] { "UId", "NormalizedName" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Taxes.Groups_CompanyId",
                schema: "Finance",
                table: "Taxes.Groups",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_TaxesGroups_UId",
                schema: "Finance",
                table: "Taxes.Groups",
                columns: new[] { "UId", "NormalizedName" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Taxes.Groups.Lines_CompanyId",
                schema: "Finance",
                table: "Taxes.Groups.Lines",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_Taxes.Groups.Lines_TaxGroupId",
                schema: "Finance",
                table: "Taxes.Groups.Lines",
                column: "TaxGroupId");

            migrationBuilder.CreateIndex(
                name: "IX_Taxes.Groups.Lines_TaxId",
                schema: "Finance",
                table: "Taxes.Groups.Lines",
                column: "TaxId");

            migrationBuilder.CreateIndex(
                name: "IX_TaxesGroupsLines_UId",
                schema: "Finance",
                table: "Taxes.Groups.Lines",
                columns: new[] { "UId", "Line" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                schema: "Security",
                table: "Users",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "IX_Users_CompanyId",
                schema: "Security",
                table: "Users",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                schema: "Security",
                table: "Users",
                column: "NormalizedUserName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Users.Claims_UserId",
                schema: "Security",
                table: "Users.Claims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Users.Logins_UserId",
                schema: "Security",
                table: "Users.Logins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Users.Roles_RoleId",
                schema: "Security",
                table: "Users.Roles",
                column: "RoleId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DataProtectionKeys");

            migrationBuilder.DropTable(
                name: "Items",
                schema: "Inventory");

            migrationBuilder.DropTable(
                name: "Roles.Claims",
                schema: "Security");

            migrationBuilder.DropTable(
                name: "Taxes.Groups.Lines",
                schema: "Finance");

            migrationBuilder.DropTable(
                name: "Users.Claims",
                schema: "Security");

            migrationBuilder.DropTable(
                name: "Users.Logins",
                schema: "Security");

            migrationBuilder.DropTable(
                name: "Users.Roles",
                schema: "Security");

            migrationBuilder.DropTable(
                name: "Users.Tokens",
                schema: "Security");

            migrationBuilder.DropTable(
                name: "Items.Categories",
                schema: "Inventory");

            migrationBuilder.DropTable(
                name: "Items.Groups",
                schema: "Inventory");

            migrationBuilder.DropTable(
                name: "Items.TransactionInfo",
                schema: "Inventory");

            migrationBuilder.DropTable(
                name: "Roles",
                schema: "Security");

            migrationBuilder.DropTable(
                name: "Users",
                schema: "Security");

            migrationBuilder.DropTable(
                name: "Items.Brands",
                schema: "Inventory");

            migrationBuilder.DropTable(
                name: "Items.Manufacturers",
                schema: "Inventory");

            migrationBuilder.DropTable(
                name: "Items.Units",
                schema: "Inventory");

            migrationBuilder.DropTable(
                name: "Currencies",
                schema: "Finance");

            migrationBuilder.DropTable(
                name: "Taxes",
                schema: "Finance");

            migrationBuilder.DropTable(
                name: "Taxes.Groups",
                schema: "Finance");

            migrationBuilder.DropTable(
                name: "Companies",
                schema: "Organization");

            migrationBuilder.DropTable(
                name: "Addresses",
                schema: "Contacts");
        }
    }
}
