﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ITProaser.Inventory.Migrations
{
    public partial class PaymentTermsMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateOnly>(
                name: "DeletedAt",
                schema: "Finance",
                table: "Taxes",
                type: "date",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                schema: "Finance",
                table: "Taxes",
                type: "boolean",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateOnly>(
                name: "DeletedAt",
                schema: "Finance",
                table: "PaymentTerms",
                type: "date",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                schema: "Finance",
                table: "PaymentTerms",
                type: "boolean",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateOnly>(
                name: "DeletedAt",
                schema: "Finance",
                table: "Currencies",
                type: "date",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                schema: "Finance",
                table: "Currencies",
                type: "boolean",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DeletedAt",
                schema: "Finance",
                table: "Taxes");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                schema: "Finance",
                table: "Taxes");

            migrationBuilder.DropColumn(
                name: "DeletedAt",
                schema: "Finance",
                table: "PaymentTerms");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                schema: "Finance",
                table: "PaymentTerms");

            migrationBuilder.DropColumn(
                name: "DeletedAt",
                schema: "Finance",
                table: "Currencies");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                schema: "Finance",
                table: "Currencies");
        }
    }
}
