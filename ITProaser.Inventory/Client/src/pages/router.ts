/** @format */
import errorStore from '@/stores/errorStore';

const ErrorsView = () => import('@/pages/errors/ErrorsView.vue');
const NotFound = () => import('@/pages/errors/NotFound.vue');
const Forbidden = () => import('@/pages/errors/Forbidden.vue');

const AccountView = () => import('@/pages/account/AccountView.vue');
const AccountSignIn = () => import('@/pages/account/AccountSignIn.vue');
const AccountSignOut = () => import('@/pages/account/AccountSignOut.vue');
const ProfileView = () => import('@/pages/profile/ProfileView.vue');
/*
 * Company
 * */
const CompanyView = () => import('@/pages/company/CompanyView.vue');
const ItemsView = () => import('@/pages/items/ItemsView.vue');
const ItemCreate = () => import('@/pages/items/ItemCreate.vue');
const ItemList = () => import('@/pages/items/ItemList.vue');
/*
 * Item Groups
 * */
const ItemGroupsView = () => import('@/pages/item-groups/GroupsView.vue');
const ItemGroupList = () => import('@/pages/item-groups/GroupList.vue');
/*
 * Item Categories
 * */
const ItemCategoriesView = () =>
  import('@/pages/item-categories/CategoriesView.vue');
const ItemCategoryCreate = () =>
  import('@/pages/item-categories/CategoryCreate.vue');
const ItemCategoryList = () =>
  import('@/pages/item-categories/CategoryList.vue');
const ItemCategoryEdit = () =>
  import('@/pages/item-categories/CategoryEdit.vue');
/*
 * Item Brands
 * */
const ItemBrandsView = () => import('@/pages/item-brands/BrandsView.vue');
const ItemBrandCreate = () => import('@/pages/item-brands/BrandCreate.vue');
const ItemBrandList = () => import('@/pages/item-brands/BrandList.vue');
const ItemBrandEdit = () => import('@/pages/item-brands/BrandEdit.vue');
/*
 * Item Units
 * */
const ItemUnitsView = () => import('@/pages/item-units/UnitsView.vue');
const ItemUnitList = () => import('@/pages/item-units/UnitList.vue');
const ItemUnitCreate = () => import('@/pages/item-units/UnitCreate.vue');
const ItemUnitEdit = () => import('@/pages/item-units/UnitEdit.vue');
/*
 * Item Manufacturers
 * */
const ItemManufacturersView = () =>
  import('@/pages/item-manufacturers/ManufacturersView.vue');
const ItemManufacturerCreate = () =>
  import('@/pages/item-manufacturers/ManufacturerCreate.vue');
const ItemManufacturerList = () =>
  import('@/pages/item-manufacturers/ManufacturerList.vue');
const ItemManufacturerEdit = () =>
  import('@/pages/item-manufacturers/ManufacturerEdit.vue');
const IndexView = () => import('@/pages/IndexView.vue');

/*
 * Sales
 * */
const CustomersView = () => import('@/pages/sales/customers/CustomersView.vue');
const CustomerList = () => import('@/pages/sales/customers/CustomerList.vue');
const CustomerCreate = () =>
  import('@/pages/sales/customers/CustomerCreate.vue');
const CustomerEdit = () => import('@/pages/sales/customers/CustomerEdit.vue');

/*
 * Purchases
 * */
const VendorsView = () => import('@/pages/purchases/vendors/VendorsView.vue');
const VendorList = () => import('@/pages/purchases/vendors/VendorList.vue');
const VendorCreate = () => import('@/pages/purchases/vendors/VendorCreate.vue');
const VendorEdit = () => import('@/pages/purchases/vendors/VendorEdit.vue');
const PurchaseOrderView = () =>
  import('@/pages/purchases/orders/OrdersView.vue');
const PurchaseOrderList = () =>
  import('@/pages/purchases/orders/OrderList.vue');
const PurchaseOrderCreate = () =>
  import('@/pages/purchases/orders/OrderCreate.vue');

import { createRouter, createWebHistory } from 'vue-router';
import userStore from '@/stores/userStore';

const routes = [
  {
    path: '/',
    name: 'index',
    component: IndexView,
    meta: { claim: null }
  },
  {
    path: '/errors',
    name: 'errors',
    component: ErrorsView,
    redirect: '/errors/not-found',
    meta: { claim: null },
    children: [
      {
        path: 'forbidden',
        name: 'errors-forbidden',
        component: Forbidden,
        meta: { claim: null }
      },
      {
        path: '/not-found',
        name: 'errors-not-found',
        component: NotFound,
        meta: { claim: null }
      }
    ]
  },
  {
    path: '/:pathMatch(.*)*',
    name: '404',
    redirect: '/errors/not-found',
    meta: { claim: null }
  },
  {
    path: '/account',
    name: 'account',
    component: AccountView,
    children: [
      {
        path: 'sign-in',
        name: 'account-sign-in',
        component: AccountSignIn,
        meta: { claim: null }
      },
      {
        path: 'sign-out',
        name: 'account-sign-out',
        component: AccountSignOut,
        meta: { claim: null }
      }
    ]
  },
  {
    path: '/company',
    name: 'company',
    component: CompanyView,
    meta: { claim: 'organization' }
  },
  {
    path: '/dashboard',
    name: 'dashboard',
    redirect: '/',
    meta: { claim: null }
  },
  {
    path: '/items',
    name: 'items',
    component: ItemsView,
    redirect: '/items/list',
    meta: { claim: 'items.list' },
    children: [
      {
        path: 'list',
        name: 'items-list',
        component: ItemList,
        meta: { claim: 'items.list' }
      },
      {
        path: 'create',
        name: 'items-create',
        component: ItemCreate,
        meta: { claim: 'items.create' }
      }
    ]
  },
  {
    path: '/item-groups',
    name: 'item-groups',
    component: ItemGroupsView,
    redirect: '/item-groups/list',
    meta: { claim: 'item_groups.list' },
    children: [
      {
        path: 'list',
        name: 'item-groups-list',
        component: ItemGroupList,
        meta: { claim: 'item_groups.list' }
      }
    ]
  },
  {
    path: '/item-brands',
    name: 'item-brands',
    component: ItemBrandsView,
    redirect: '/item-brands/list',
    meta: { claim: 'item_brands.list' },
    children: [
      {
        path: 'create',
        name: 'item-brands-create',
        component: ItemBrandCreate,
        meta: { claim: 'item_brands.create' }
      },
      {
        path: 'edit/:id',
        name: 'item-brands-edit',
        component: ItemBrandEdit,
        meta: { claim: 'item_brands.edit' }
      },
      {
        path: 'list',
        name: 'item-brands-list',
        component: ItemBrandList,
        meta: { claim: 'item_brands.list' }
      }
    ]
  },
  {
    path: '/item-categories',
    name: 'item-categories',
    component: ItemCategoriesView,
    redirect: '/item-categories/list',
    meta: { claim: 'item_categories.list' },
    children: [
      {
        path: 'create',
        name: 'item-categories-create',
        component: ItemCategoryCreate,
        meta: { claim: 'item_categories.create' }
      },
      {
        path: 'edit/:id',
        name: 'edit',
        component: ItemCategoryEdit,
        meta: { claim: 'item_categories.edit' }
      },
      {
        path: 'list',
        name: 'item-categories-list',
        component: ItemCategoryList,
        meta: { claim: 'item_categories.list' }
      }
    ]
  },
  {
    path: '/item-units',
    name: 'item-units',
    redirect: '/item-units/list',
    component: ItemUnitsView,
    meta: { skipsAuth: true, area: null },
    children: [
      {
        path: 'create',
        name: 'item-units-create',
        component: ItemUnitCreate,
        meta: { skipsAuth: false, area: 'inventory' }
      },
      {
        path: 'edit/:id',
        name: 'item-units-edit',
        component: ItemUnitEdit,
        meta: { skipsAuth: false, area: 'inventory' }
      },
      {
        path: 'list',
        name: 'item-units-list',
        component: ItemUnitList,
        meta: { skipsAuth: false, area: 'inventory' }
      }
    ]
  },
  {
    path: '/item-manufacturers',
    name: 'item-manufacturers',
    component: ItemManufacturersView,
    redirect: '/item-manufacturers/list',
    meta: { skipsAuth: true, area: null },
    children: [
      {
        path: 'create',
        name: 'item-manufacturers-create',
        component: ItemManufacturerCreate,
        meta: { skipsAuth: false, area: 'inventory' }
      },
      {
        path: 'edit/:id',
        name: 'item-manufacturers-edit',
        component: ItemManufacturerEdit,
        meta: { skipsAuth: false, area: 'inventory' }
      },
      {
        path: 'list',
        name: 'item-manufacturers-list',
        component: ItemManufacturerList,
        meta: { skipsAuth: false, area: 'inventory' }
      }
    ]
  },
  {
    path: '/sales/customers',
    name: 'sales-customers',
    component: CustomersView,
    redirect: '/sales/customers/list',
    meta: { skipsAuth: true, area: null },
    children: [
      {
        path: 'list',
        name: 'sales-customers-list',
        component: CustomerList,
        meta: { skipsAuth: false, area: null }
      },
      {
        path: 'create',
        name: 'sales-customers-create',
        component: CustomerCreate,
        meta: { skipsAuth: false, area: null }
      },
      {
        path: 'edit/:id',
        name: 'sales-customers-edit',
        component: CustomerEdit,
        meta: { skipsAuth: false, area: null }
      }
    ]
  },
  {
    path: '/purchases',
    name: 'purchases',
    redirect: '/purchases/orders/list',
    meta: { skipsAuth: true, area: null },
    children: [
      {
        path: 'vendors',
        name: 'purchases-vendors',
        component: VendorsView,
        redirect: '/purchases/vendors/list',
        meta: { skipsAuth: true, area: null },
        children: [
          {
            path: 'list',
            name: 'purchases-vendors-list',
            component: VendorList,
            meta: { skipsAuth: false, area: null }
          },
          {
            path: 'create',
            name: 'purchases-vendors-create',
            component: VendorCreate,
            meta: { skipsAuth: false, area: null }
          },
          {
            path: 'edit/:id',
            name: 'purchases-vendors-edit',
            component: VendorEdit,
            meta: { skipsAuth: false, area: null }
          }
        ]
      },
      {
        path: 'orders',
        name: 'purchase-orders',
        component: PurchaseOrderView,
        meta: { skipsAuth: true, area: null },
        redirect: '/purchases/orders/list',
        children: [
          {
            path: 'list',
            name: 'purchase-orders-list',
            component: PurchaseOrderList,
            meta: { skipsAuth: false, area: null }
          },
          {
            path: 'create',
            name: 'purchase-orders-create',
            component: PurchaseOrderCreate,
            meta: { skipsAuth: false, area: null }
          }
        ]
      }
    ]
  },
  {
    path: '/profile',
    name: 'profile',
    component: ProfileView,
    meta: { authSkip: false, area: 'preferences' }
  }
];

const router = createRouter({
  history: createWebHistory(),
  routes
});

router.beforeEach((to, from, next) => {
  const { claim } = to.meta;
  if (to.meta.claim === null) return next();
  const store = errorStore();
  const user = userStore();
  try {
    if (typeof claim !== 'string') return next(from.path);
    if (user.hasClaim(claim)) return next();
    else if (!user.exists) return next({ name: 'account-login' });
    else return next({ name: 'account-denied' });
  } catch (e: any) {
    store.addError(e.message);
    if (from.name) return next({ name: from.name });
    else return next('/');
  }
});

export default router;
