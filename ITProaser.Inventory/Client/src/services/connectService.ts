/** @format */

import axios, { AxiosRequestHeaders } from "axios";
import sessionStore from "@/stores/sessionStore";
import errorStore from "@/stores/errorStore";

export const endpoints = {
  Auth: {
    Login: '/api/auth/login',
    Logout: '/api/auth/logout'
  },
  Content: {
    Locale: '/api/content/locale',
    Profile: '/api/content/profile',
    Route: '/api/content/routes',
    Company: '/api/content/company'
  },
  Tenancy: {
    Currencies: '/api/tenancy/currencies',
    Taxes: '/api/tenancy/taxes',
    PaymentTerms: '/api/tenancy/paymentTerms'
  },
  Inventory: {
    ItemBrands: {
      Create: '/api/inventory/item-brands/create',
      Download: '/api/inventory/item-brands/download',
      Edit: (id: string) => `/api/inventory/item-brands/edit/${id}`
    },
    ItemCategories: {
      Create: '/api/inventory/item-categories/create',
      Download: '/api/inventory/item-categories/download',
      Edit: (id: string) => `/api/inventory/item-categories/edit/${id}`
    },
    ItemManufacturers: {
      Create: '/api/inventory/item-manufacturers/create',
      Download: '/api/inventory/item-manufacturers/download',
      Edit: (id: string) => `/api/inventory/item-manufacturers/edit/${id}`
    },
    ItemUnits: {
      Create: '/api/inventory/item-units/create',
      Download: '/api/inventory/item-units/download',
      Edit: (id: string) => `/api/inventory/item-units/edit/${id}`
    },
    Items: {
      Create: '/api/inventory/items/create',
      Download: '/api/inventory/items/download',
      Edit: (id: string) => `/api/inventory/items/edit/${id}`
    }
  },
  Purchases: {
    Vendors: {
      Create: '/api/purchases/vendors/create',
      Download: '/api/purchases/vendors/download',
      Edit: (id: string) => `/api/purchases/vendors/edit/${id}`
    }
  },
  Sales: {
    Customers: {
      Create: '/api/sales/customers/create',
      Download: '/api/sales/customers/download',
      Edit: (id: string) => `/api/sales/customers/edit/${id}`
    }
  }
};

enum ResultCode {
  Continue = 100,
  Ok = 200,
  Created = 201,
  NoContent = 204,
  Found = 302,
  BadRequest = 400,
  Unauthorized = 401,
  Forbidden = 403,
  NotFound = 404
}

interface IResult<TType> {
  statusCode: ResultCode;
  message: string | null;
  redirect: string | null;
  timeStamp: Date;
  content: TType | null;
}

export default async function connectService<TContent>(
  endpoint: string,
  {
    method,
    data,
    access_token
  }: {
    method: 'GET' | 'POST' | 'PUT' | 'DELETE';
    data: FormData | null;
    access_token?: string;
  } = {
    method: 'GET',
    data: null
  }
): Promise<IResult<TContent>> {
  try {
    const url = endpoint;
    const headers: AxiosRequestHeaders = {};
    const session = sessionStore();
    const user = await session.getUser();
    if (access_token !== undefined)
      headers.Authorization = `Bearer ${access_token}`;
    else if (user && !user.expired)
      headers.Authorization = `${user.token_type} ${user.access_token}`;
    const response = await axios({
      url,
      method,
      data,
      headers
    });
    return response.data;
  } catch (e: any) {
    const errors = errorStore();
    errors.addError(e.message);
    return {
      statusCode: ResultCode.BadRequest,
      message: e.message,
      redirect: null,
      timeStamp: new Date(),
      content: null
    };
  }
}