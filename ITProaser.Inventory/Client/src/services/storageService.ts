/** @format */

export default function defineStorage<Store>(
  name: string,
  store: Store | null = null
) {
  const __checkStorage = (type: 'localStorage' | 'sessionStorage') => {
    try {
      const storage: Storage = window[type];
      const x = '__storage_test__';
      storage.setItem(x, x);
      storage.removeItem(x);
      return true;
    } catch (e) {
      return false;
    }
  };

  const setContent = (_content: Store | null): void => {
    if (!__checkStorage('localStorage')) return;
    if (_content === null) return;
    localStorage[name] = JSON.stringify(_content);
  };

  const getContent = (): Store | null => {
    if (!__checkStorage('localStorage')) return null;
    if (localStorage[name] === undefined) return null;
    return JSON.parse(localStorage[name]);
  };

  const dispose = () => {
    if (!__checkStorage('localStorage')) return;
    if (localStorage[name] === undefined) return;
    localStorage.removeItem(name);
  };

  if (store !== null && store !== undefined) setContent(store);

  return {
    /**
     * Gets the content stored in storage.
     * @function
     */
    content: getContent,
    /**
     * Disposes the current object store.
     * @function
     */
    dispose,
    /**
     * Sets a new value in storage for the entity.
     * @function
     */
    setContent
  };
}
