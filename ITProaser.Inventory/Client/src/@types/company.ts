/** @format */

type Company = {
  id: string | null;
  name: string | null;
  industry: string | null;
  companyId: string | null;
  address: Address | null;
};
