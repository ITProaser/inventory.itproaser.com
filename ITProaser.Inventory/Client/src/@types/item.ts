/** @format */

type ItemUnit = {
  uId: string;
  name: string;
  normalizedName: string;
  description: string | null;
};

type ItemManufacturer = {
  uId: string;
  name: string;
  normalizedName: string;
  description: string | null;
};

type ItemBrand = {
  uId: string;
  name: string;
  normalizedName: string;
  description: string | null;
};

type ItemCategory = {
  uId: string;
  name: string;
  normalizedName: string;
  description: string | null;
};

type Item = {
  uId: string;
  type: 0 | 1;
  name: string | null;
  normalizedName: string | null;
  sku: string | null;
  category: ItemCategory | null;
  unit: ItemUnit | null;
  manufacturer: ItemManufacturer | null;
  brand: ItemBrand | null;
  universalProductCode: string | null;
  internationalArticleNumber: string | null;
  manufacturerPartNumber: string | null;
  internationalStandardBookNumber: string | null;
  isReturnable: boolean;
  purchaseInformation: TransactionInfo | null;
  salesInformation: TransactionInfo | null;
  notes: string | null;
};

type TransactionInfo = {
  id: string | null;
  currency: Currency | null;
  rate: number;
  description: string | null;
};
