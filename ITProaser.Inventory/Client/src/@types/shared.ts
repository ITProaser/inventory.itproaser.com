/** @format */

type ActionResponse = {
  success: boolean;
  message: string | null;
};

type Currency = {
  id: string | null;
  name: string | null;
  normalizedName: string | null;
  symbol: string | null;
  code: string | null;
  decimalPlaces: number;
  isDefault: boolean;
};

type Address = {
  street: string | null;
  state: string | null;
  city: string | null;
  country: string | null;
  zipCode: string | null;
};

type Tax = {
  id: string | null;
  name: string | null;
  rate: number;
  isCompound: boolean;
};

type PaymentTerm = {
  id: string | null;
  name: string | null;
  days: number;
};
