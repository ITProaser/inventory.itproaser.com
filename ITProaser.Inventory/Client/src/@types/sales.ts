/** @format */

type Customer = {
  type: 0 | 1;
  id: string | null;
  firstName: string | null;
  lastName: string | null;
  displayName: string | null;
  email: string | null;
  companyName: string | null;
  title: string | null;
  mobilePhone: string | null;
  workPhone: string | null;
  remarks: string | null;
  currencyId: string | null;
  taxId: string | null;
  paymentTermId: string | null;
  website: string | null;
  billingAddress: Address | null;
  shippingAddress: Address | null;
  ownerId: string | null;
};
