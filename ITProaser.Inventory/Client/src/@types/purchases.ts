/** @format */

type Vendor = {
  id: string | null;
  firstName: string | null;
  lastName: string | null;
  displayName: string | null;
  email: string | null;
  companyName: string | null;
  title: string | null;
  mobilePhone: string | null;
  workPhone: string | null;
  remarks: string | null;
  currencyId: string | null;
  taxId: string | null;
  paymentTermId: string | null;
  website: string | null;
  billingAddress: Address | null;
  shippingAddress: Address | null;
};

type PurchaseOrder = {
  uId: string | null;
  vendorId: string | null;
  number: string | null;
  reference: string | null;
  expectedDate: Date | null;
  date: Date | null;
  orderLines: OrderLine[];
  subtotal: number;
  total: number;
};

type SaleOrder = {
  uId: string | null;
  customerId: string | null;
  number: string | null;
  reference: string | null;
  expectedDate: Date | null;
  date: Date | null;
  orderLines: OrderLine[];
  subtotal: number;
  total: number;
};

type OrderLine = {
  uId: string | null;
  line: number;
  itemId: string | null;
  quantity: number;
  price: number;
  discount: number;
  discountType: 0 | 1;
  notes: string | null;
  tax: string | null;
};
