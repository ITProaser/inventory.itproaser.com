/** @format */

type Route = {
  path: string;
  label: string;
  icon: string;
  group: string | null;
  sidebar: boolean;
  children: Route[] | null;
};
