/** @format */

type UserProfile = {
  id: string | null;
  userName: string | null;
  email: string | null;
  displayName: string | null;
  firstName: string | null;
  lastName: string | null;
  tenant: string | null;
  claims: UserClaim[];
};

type UserPreferences = {
  culture: string;
  theme: string;
  layout: {
    order: -1 | 0;
    rail: boolean;
    fixed: boolean;
    hover: boolean;
    density: 'compact' | 'prominent' | 'default';
    temporary: boolean;
  };
};

type UserClaim = {
  issuer: 'user' | 'role';
  originalIssuer: 'user' | 'role';
  properties: object;
  subject: null;
  type: 'role';
  value: string;
  valueType: 'http://www.w3.org/2001/XMLSchema#string';
};

