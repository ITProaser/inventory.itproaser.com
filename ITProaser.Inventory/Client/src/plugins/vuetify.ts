/** @format */

// Styles
import '@mdi/font/css/materialdesignicons.css';
import 'vuetify/styles';

// Vuetify
import { createVuetify } from 'vuetify';
import { en, es } from 'vuetify/locale';
import { aliases, mdi } from 'vuetify/iconsets/mdi';
import defineStorage from '@/services/storageService';

const preferences = defineStorage<UserPreferences>('preferences');

const colors = {
  dark: {
    background: '#1C1B1F',
    'on-background': '#E7E3FC',
    surface: '#2C2340',
    'on-surface': '#E7E3FC',
    primary: '#7C4DFF',
    'primary-darken-1': '#6200EA',
    secondary: '#512DA8',
    'secondary-darken-1': '#311B92',
    error: '#FF4C51',
    'on-error': '#601410',
    info: '#16B1FF',
    success: '#56CA00',
    warning: '#FFB400'
  },
  light: {
    background: '#FFFBFE',
    'on-background': '#3A3541',
    surface: '#ECE1FE',
    'on-surface': '#3A3541',
    primary: '#7C4DFF',
    'primary-darken-1': '#6200EA',
    secondary: '#512DA8',
    'secondary-darken-1': '#311B92',
    error: '#FF4C51',
    'on-error': '#FFFFFF',
    info: '#32BAFF',
    success: '#56CA00',
    warning: '#FFB400'
  }
};
const theme =
  preferences.content()?.theme ||
  (window.matchMedia('(prefers-color-scheme: dark)').matches
    ? 'dark'
    : 'light');

export default createVuetify(
  // https://vuetifyjs.com/en/introduction/why-vuetify/#feature-guides
  {
    icons: {
      defaultSet: 'mdi',
      aliases,
      sets: {
        mdi
      }
    },
    locale: {
      defaultLocale: 'en',
      fallbackLocale: 'en',
      messages: { en, es }
    },
    theme: {
      defaultTheme: theme,
      themes: {
        dark: {
          dark: true,
          colors: { ...colors.dark }
        },
        light: {
          dark: false,
          colors: { ...colors.light }
        }
      }
    }
  }
);
