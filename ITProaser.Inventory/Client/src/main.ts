/** @format */

import { createApp } from 'vue';
import App from '@/App.vue';
import router from '@/pages/router';
import vuetify from '@/plugins/vuetify';
import { loadFonts } from '@/plugins/webfontloader';
import { createPinia } from 'pinia';
import { createI18n } from 'vue-i18n';
import axios from 'axios';
import defineStorage from '@/services/storageService';

await loadFonts();
const store = createPinia();

const localeStored = defineStorage<UserPreferences>('preferences');
const locale = localeStored.content()?.culture ?? 'en';
axios.defaults.headers.common['Accept-Language'] = locale;
const i18n = createI18n({
  locale
});

createApp(App).use(vuetify).use(router).use(store).use(i18n).mount('#app');

export { i18n };
