/** @format */

import defineStorage from '@/services/storageService';
import { defineStore } from 'pinia';
import connectService, { endpoints } from '@/services/connectService';

type CustomerStore = {
  customers: Customer[];
  patch: Date | null;
};

const store = 'customers';
const storage = defineStorage<CustomerStore>(store);

const customerStore = defineStore(store, {
  state: (): CustomerStore =>
    storage.content() ?? {
      customers: [],
      patch: null
    },
  actions: {
    async create(data: FormData) {
      const result: ActionResponse = { message: null, success: false };
      try {
        const response = await connectService<Customer>(
          endpoints.Sales.Customers.Create,
          { method: 'POST', data }
        );
        result.message = response.message;
        if (response.statusCode !== 201) return result;
        result.success = true;
        this.patch = response.timeStamp;
        if (response.content !== null) this.customers.push(response.content);
        storage.setContent({ customers: this.customers, patch: this.patch });
      } catch (error: any) {
        result.message = error.message;
      }
      return result;
    },
    async edit(id: string, data: FormData) {
      const result: ActionResponse = { message: null, success: false };
      try {
        const response = await connectService<Customer>(
          endpoints.Sales.Customers.Edit(id),
          { method: 'PUT', data }
        );
        result.message = response.message;
        if (response.statusCode !== 204) return result;
        result.success = true;
        this.patch = response.timeStamp;
        const index = this.customers.findIndex(customer => customer.id === id);
        if (index === -1) return result;
        if (response.content !== null) this.customers[index] = response.content;
        storage.setContent({ customers: this.customers, patch: this.patch });
      } catch (error: any) {
        result.message = error.message;
      }
      return result;
    },
    async download() {
      const result: ActionResponse = { message: null, success: false };
      try {
        const response = await connectService<Customer[]>(
          endpoints.Sales.Customers.Download
        );
        result.message = response.message;
        if (response.statusCode !== 204) return result;
        result.success = true;
        this.patch = response.timeStamp;
        if (response.content !== null) this.customers = response.content;
        storage.setContent({ customers: this.customers, patch: this.patch });
      } catch (error: any) {
        result.message = error.message;
      }
      return result;
    },
    get(id: string) {
      const customer = this.customers.find(customer => customer.id === id);
      if (customer === undefined) return null;
      return { ...customer };
    },
    delete() {
      this.customers = [];
      this.patch = null;
      storage.dispose();
    }
  },
  getters: {
    refresh: state => state.patch === null,
    any: state => state.customers.length !== 0
  }
});

export default customerStore;
