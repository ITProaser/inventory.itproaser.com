/** @format */

import { defineStore } from 'pinia';
import connectService, { endpoints } from '../services/connectService';
import companyStore from './companyStore';
import defineStorage from '@/services/storageService';
const store = 'user';
const storage = defineStorage<UserProfile>(store);

const userStore = defineStore(store, {
  state: (): UserProfile =>
    storage.content() ?? {
      userName: null,
      email: null,
      id: null,
      displayName: null,
      firstName: null,
      lastName: null,
      tenant: null,
      claims: []
    },
  actions: {
    logOut() {
      companyStore().delete();
      this.delete();
    },
    async request(access_token: string) {
      try {
        const request = await connectService<UserProfile>(
          endpoints.Content.Profile,
          { method: 'GET', data: null, access_token }
        );
        if (request.statusCode !== 200 || request.content === null) return;
        this.userName = request.content.userName;
        this.email = request.content.email;
        this.id = request.content.id;
        this.displayName = request.content.displayName;
        this.firstName = request.content.firstName;
        this.lastName = request.content.lastName;
        this.tenant = request.content.tenant;
        this.claims = request.content.claims;
        storage.setContent(request.content);
      } catch (e) {
        console.log(e);
      }
    },
    hasClaim(claim: string[] | string) {
      return (
        this.id !== null &&
        this.claims.find(
          c =>
            c.value === 'root' ||
            (typeof claim === 'string' && c.value === claim) ||
            (typeof claim !== 'string' && claim.includes(c.value))
        ) !== undefined
      );
    },
    delete() {
      this.userName = null;
      this.email = null;
      this.id = null;
      this.displayName = null;
      this.firstName = null;
      this.lastName = null;
      this.tenant = null;
      this.claims = [];
      storage.dispose();
    }
  },
  getters: {
    exists: state => state.id !== null,
    isRoot: state =>
      state.id !== null &&
      state.claims.find(c => c.issuer === 'user' && c.value === 'root') !==
        undefined,
    isAdmin: state =>
      state.id !== null &&
      state.claims.find(c => c.issuer === 'role' && c.value === 'admin') !==
        undefined
  }
});

export default userStore;
