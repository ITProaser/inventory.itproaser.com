/** @format */

import { defineStore } from 'pinia';
import connectService, { endpoints } from '@/services/connectService';
import defineStorage from '@/services/storageService';

type RouteStore = {
  routes: Route[];
  patch: Date | null;
};

const store = 'routes';
const storage = defineStorage<RouteStore>(store);

const getGroups = (routes: Route[]) =>
  routes.reduce((acc: string[], route) => {
    if (route.sidebar && route.group !== null && !acc.includes(route.group))
      acc.push(route.group);
    return acc;
  }, []);

const routesStore = defineStore(store, {
  state: (): RouteStore =>
    storage.content() ?? {
      routes: [],
      patch: null
    },
  actions: {
    async download() {
      const response = await connectService<Route[]>(endpoints.Content.Route);
      if (response.statusCode !== 204) {
        this.routes = [];
        return;
      }
      this.patch = response.timeStamp;
      this.routes = response.content!;
      storage.setContent({
        patch: response.timeStamp,
        routes: response.content!
      });
    },
    delete() {
      this.patch = null;
      this.routes = [];
      storage.dispose();
    }
  },
  getters: {
    refresh: state => state.patch === null,
    sidebarGroups: state => getGroups(state.routes),
    routesSidebarNotGrouped: state =>
      state.routes.filter(route => route.sidebar && route.group === null),
    routesSidebarGrouped: state =>
      getGroups(state.routes).map(group => ({
        group,
        routes: state.routes.filter(
          route => route.sidebar && route.group === group
        )
      }))
  }
});

export default routesStore;
