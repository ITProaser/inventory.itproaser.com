/** @format */

import { defineStore } from 'pinia';
import { i18n } from '@/main';
import defineStorage from '@/services/storageService';

const store = 'preferences';

const storage = defineStorage<UserPreferences>(store);

const supportedCultures: string[] = ['en', 'es'];

const supportedUICultures = {
  en: 'English',
  es: 'Español'
};

const navigatorCulture = navigator.language.split('-')[0];
const defaultLayout: {
  order: -1 | 0;
  rail: boolean;
  fixed: boolean;
  hover: boolean;
  density: 'compact' | 'prominent' | 'default';
  temporary: boolean;
} = {
  order: -1,
  rail: false,
  fixed: false,
  hover: false,
  density: 'default',
  temporary: false
};

const preferencesStore = defineStore(store, {
  state: (): UserPreferences =>
    storage.content() ?? {
      culture: supportedCultures.includes(navigatorCulture)
        ? navigatorCulture
        : 'en',
      theme: window.matchMedia('(prefers-color-scheme: dark)').matches
        ? 'dark'
        : 'light',
      layout: defaultLayout
    },
  actions: {
    async changeCulture(culture: string) {
      if (!supportedCultures.includes(culture)) return;
      // @ts-ignore
      i18n.global.locale.value = culture;
      document.querySelector('html')?.setAttribute('lang', culture);
      this.culture = culture;
      storage.setContent({ culture, theme: this.theme, layout: this.layout });
    },
    setTheme(theme: string) {
      if (theme !== 'light' && theme !== 'dark') return;
      this.theme = theme;
      document
        .querySelectorAll('meta[name="theme-color"]')
        .forEach(query =>
          query.setAttribute(
            'content',
            theme === 'dark' ? '#1C1B1F' : '#FFFBFE'
          )
        );
      storage.setContent({ culture: this.culture, theme, layout: this.layout });
    },
    toggleLayoutOrder() {
      if (this.layout === undefined) this.layout = defaultLayout;
      this.layout.order = this.layout.order === 0 ? -1 : 0;
      storage.setContent({
        culture: this.culture,
        theme: this.theme,
        layout: this.layout
      });
    },
    toggleLayoutRail() {
      if (this.layout === undefined) this.layout = defaultLayout;
      this.layout.rail = !this.layout.rail;
      storage.setContent({
        culture: this.culture,
        theme: this.theme,
        layout: this.layout
      });
    },
    toggleLayoutFixed() {
      if (this.layout === undefined) this.layout = defaultLayout;
      this.layout.fixed = !this.layout.fixed;
      storage.setContent({
        culture: this.culture,
        theme: this.theme,
        layout: this.layout
      });
    },
    toggleLayoutHover() {
      if (this.layout === undefined) this.layout = defaultLayout;
      this.layout.hover = !this.layout.hover;
      storage.setContent({
        culture: this.culture,
        theme: this.theme,
        layout: this.layout
      });
    },
    setLayoutDensity(density: 'compact' | 'prominent' | 'default') {
      if (this.layout === undefined) this.layout = defaultLayout;
      this.layout.density = density;
      storage.setContent({
        culture: this.culture,
        theme: this.theme,
        layout: this.layout
      });
    },
    toggleLayoutTemporary() {
      if (this.layout === undefined) this.layout = defaultLayout;
      this.layout.temporary = !this.layout.temporary;
      storage.setContent({
        culture: this.culture,
        theme: this.theme,
        layout: this.layout
      });
    }
  },
  getters: {
    supportedCultures: () => supportedCultures,
    supportedUICultures: () => supportedUICultures,
    getLayout: state =>
      state.layout?.order === null || state.layout?.order === undefined
        ? defaultLayout
        : state.layout
  }
});

export default preferencesStore;
