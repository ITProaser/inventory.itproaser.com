/** @format */

import { defineStore } from 'pinia';
import defineStorage from '@/services/storageService';
import connectService, { endpoints } from '@/services/connectService';

type CompanyStore = {
  profile: Company;
  patch: Date | null;
  currencies:
    | {
        list: Currency[];
        patch: Date | null;
      }
    | undefined;
  taxes:
    | {
        list: Tax[];
        patch: Date | null;
      }
    | undefined;
  paymentTerms:
    | {
        list: PaymentTerm[];
        patch: Date | null;
      }
    | undefined;
};

const defaultCurrencies = {
  list: [],
  patch: null
};
const defaultTaxes = {
  list: [],
  patch: null
};
const defaultPaymentTerms = {
  list: [],
  patch: null
};

const store = 'company';
const storage = defineStorage<CompanyStore>(store);

const companyStore = defineStore(store, {
  state: (): CompanyStore =>
    storage.content() ?? {
      profile: {
        id: null,
        name: null,
        industry: null,
        companyId: null,
        address: null
      },
      patch: null,
      currencies: defaultCurrencies,
      taxes: defaultTaxes,
      paymentTerms: defaultPaymentTerms
    },
  actions: {
    delete() {
      this.profile = {
        id: null,
        name: null,
        industry: null,
        companyId: null,
        address: null
      };
      this.patch = null;
      storage.dispose();
    },
    async download() {
      const result: ActionResponse = { success: false, message: null };
      try {
        const response = await connectService<Company>(
          endpoints.Content.Company
        );
        result.message = response.message;
        if (response.statusCode !== 204) return result;
        result.success = true;
        this.patch = response.timeStamp;
        if (response.content !== null) this.profile = response.content;
        this.save();
      } catch (e: any) {
        result.message = e.message;
      }
      return result;
    },
    async downloadCurrencies() {
      const result: ActionResponse = { success: false, message: null };
      try {
        const response = await connectService<Currency[]>(
          endpoints.Tenancy.Currencies
        );
        result.message = response.message;
        if (response.statusCode !== 204) return result;
        result.success = true;
        if (this.currencies === undefined) this.currencies = defaultCurrencies;
        this.currencies.patch = response.timeStamp;
        if (response.content !== null) this.currencies.list = response.content;
        this.save();
      } catch (e: any) {
        result.message = e.message;
      }
      return result;
    },
    async downloadTaxes() {
      const result: ActionResponse = { success: false, message: null };
      try {
        const response = await connectService<Tax[]>(endpoints.Tenancy.Taxes);
        result.message = response.message;
        if (response.statusCode !== 204) return result;
        result.success = true;
        if (this.taxes === undefined) this.taxes = defaultTaxes;
        this.taxes.patch = response.timeStamp;
        if (response.content !== null) this.taxes.list = response.content;
        this.save();
      } catch (e: any) {
        result.message = e.message;
      }
      return result;
    },
    async downloadPaymentTerms() {
      const result: ActionResponse = { success: false, message: null };
      try {
        const response = await connectService<PaymentTerm[]>(
          endpoints.Tenancy.PaymentTerms
        );
        result.message = response.message;
        if (response.statusCode !== 204) return result;
        result.success = true;
        if (this.paymentTerms === undefined)
          this.paymentTerms = defaultPaymentTerms;
        this.paymentTerms.patch = response.timeStamp;
        if (response.content !== null)
          this.paymentTerms.list = response.content;
        this.save();
      } catch (e: any) {
        result.message = e.message;
      }
      return result;
    },
    getCurrency(id: string) {
      const currency = this.currencies?.list.find(c => c.id === id);
      if (currency === undefined) return null;
      return { ...currency };
    },
    getTax(id: string) {
      const tax = this.taxes?.list.find(t => t.id === id);
      if (tax === undefined) return null;
      return { ...tax };
    },
    getPaymentTerm(id: string) {
      const paymentTerm = this.paymentTerms?.list.find(
        t => t.id === id
      );
      if (paymentTerm === undefined) return null;
      return { ...paymentTerm };
    },
    save() {
      storage.setContent({
        patch: this.patch,
        profile: this.profile,
        currencies: this.currencies,
        taxes: this.taxes,
        paymentTerms: this.paymentTerms
      });
    }
  },
  getters: {
    refreshCurrencies: state => state.currencies?.patch === null,
    refreshTaxes: state => state.taxes?.patch === null,
    refreshPaymentTerms: state => state.paymentTerms?.patch === null,
    refresh: state =>
      state.patch === null ||
      state.taxes?.patch === null ||
      state.currencies?.patch === null ||
      state.paymentTerms?.patch === null,
    hasProfile: state => state.profile.id !== null,
    defaultCurrency: state =>
      (state.currencies ?? defaultCurrencies).list.find(
        currency => currency.isDefault
      ) ?? null,
    currenciesToSelect: state =>
      (state.currencies ?? defaultCurrencies).list.map(currency => ({
        value: currency.id,
        title: `${currency.code} - ${currency.name}`
      })),
    taxesToSelect: state =>
      (state.taxes ?? defaultTaxes).list.map(tax => ({
        value: tax.id,
        title: `${tax.name} - ${tax.rate}%`
      })),
    paymentTermsToSelect: state =>
      (state.paymentTerms ?? defaultPaymentTerms).list.map(paymentTerm => ({
        value: paymentTerm.id,
        title: paymentTerm.name
      })),
    countriesToSelect: () =>
      ['Argentina', 'Chile', 'Colombia', 'Nicaragua', 'United States'].map(
        v => ({ value: v, title: v })
      )
  }
});

export default companyStore;
