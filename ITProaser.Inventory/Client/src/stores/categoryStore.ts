/** @format */

import { defineStore } from 'pinia';
import connectService, { endpoints } from '@/services/connectService';
import defineStorage from '@/services/storageService';

type CategoryStore = {
  categories: ItemCategory[];
  patch: Date | null;
};

const store = 'item-categories';
const storage = defineStorage<CategoryStore>(store);

const categoryStore = defineStore(store, {
  state: (): CategoryStore =>
    storage.content() ?? {
      categories: [],
      patch: null
    },
  actions: {
    async create(data: FormData) {
      const result: ActionResponse = { success: false, message: null };
      try {
        const response = await connectService<ItemCategory>(
          endpoints.Inventory.ItemCategories.Create,
          { method: 'POST', data }
        );
        result.message = response.message;
        if (response.statusCode !== 201) return result;
        this.categories.push(response.content!);
        result.success = true;
        storage.setContent({
          categories: this.categories,
          patch: response.timeStamp
        });
      } catch (e) {
        result.message = 'default';
      }
      return result;
    },
    delete() {
      this.categories = [];
      this.patch = null;
      storage.dispose();
    },
    async download() {
      const response = await connectService<ItemCategory[]>(
        endpoints.Inventory.ItemCategories.Download
      );
      if (response.statusCode !== 204) {
        this.categories = [];
        return;
      }
      this.patch = response.timeStamp;
      this.categories = response.content!;
      storage.setContent({
        categories: this.categories,
        patch: response.timeStamp
      });
    },
    async edit(id: string, data: FormData) {
      const result: ActionResponse = { success: false, message: null };
      try {
        const response = await connectService<ItemCategory>(
          endpoints.Inventory.ItemCategories.Edit(id),
          { method: 'PUT', data }
        );
        result.message = response.message;
        if (response.statusCode !== 204) return result;
        result.success = true;
        const index = this.categories.findIndex(
          category => category.uId === id
        );
        if (index === -1) return result;
        this.categories[index].description = response.content!.description;
        this.categories[index].name = response.content!.name;
        this.categories[index].normalizedName =
          response.content!.name.toUpperCase();
        storage.setContent({
          categories: this.categories,
          patch: response.timeStamp
        });
      } catch (e) {
        result.message = 'default';
      }
      return result;
    },
    get(id: string) {
      return this.categories.find(category => category.uId === id);
    }
  },
  getters: {
    any: state => state.patch !== null && state.categories.length > 0,
    refresh: state => state.patch === null,
    toSelect: state =>
      state.categories.map(category => ({
        value: category.uId,
        title: category.name
      })),
    toTable: state =>
      state.categories.map(category => ({
        id: category.uId,
        name: category.name,
        description: category.description
      }))
  }
});

export default categoryStore;
