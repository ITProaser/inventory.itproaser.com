/** @format */

import { defineStore } from 'pinia';
import connectService, { endpoints } from '@/services/connectService';
import defineStorage from '@/services/storageService';

type BrandStore = {
  brands: ItemBrand[];
  patch: Date | null;
};

const store = 'item-brands';
const storage = defineStorage<BrandStore>(store);

const brandStore = defineStore(store, {
  state: (): BrandStore => storage.content() ?? { brands: [], patch: null },
  actions: {
    async create(data: FormData) {
      const result: ActionResponse = { success: false, message: null };
      try {
        const response = await connectService<ItemBrand>(
          endpoints.Inventory.ItemBrands.Create,
          { method: 'POST', data }
        );
        result.message = response.message;
        if (response.statusCode !== 201) return result;
        result.success = true;
        this.brands.push(response.content!);
        storage.setContent({ brands: this.brands, patch: response.timeStamp });
      } catch (e) {
        result.message = 'default';
      }
      return result;
    },
    delete() {
      this.brands = [];
      this.patch = null;
      storage.dispose();
    },
    async download() {
      const response = await connectService<ItemBrand[]>(
        endpoints.Inventory.ItemBrands.Download
      );
      if (response.statusCode !== 204) return;
      this.brands = response.content!;
      this.patch = response.timeStamp;
      storage.setContent({ patch: response.timeStamp, brands: this.brands });
    },
    async edit(id: string, data: FormData) {
      const result: ActionResponse = { success: false, message: null };
      try {
        const response = await connectService<ItemBrand>(
          endpoints.Inventory.ItemBrands.Edit(id),
          { method: 'PUT', data }
        );
        result.message = response.message;
        if (response.statusCode !== 204) return result;
        result.success = true;
        const index = this.brands.findIndex(brand => brand.uId === id);
        if (index === -1) return result;
        this.brands[index].description = response.content!.description;
        this.brands[index].name = response.content!.name;
        this.brands[index].normalizedName =
          response.content!.name.toUpperCase();
        storage.setContent({ brands: this.brands, patch: response.timeStamp });
      } catch (e) {
        result.message = 'default';
      }
      return result;
    },
    get(id: string) {
      return this.brands.find(brand => brand.uId === id);
    }
  },
  getters: {
    refresh: state => state.patch === null,
    any: state => !state.patch !== null && state.brands.length > 0,
    toSelect: state =>
      state.brands.map(brand => ({
        value: brand.uId,
        title: brand.name
      })),
    toTable: state =>
      state.brands.map(brand => ({
        id: brand.uId,
        name: brand.name,
        description: brand.description
      }))
  }
});

export default brandStore;
