/** @format */

import { defineStore } from 'pinia';
import defineStorage from '@/services/storageService';
import connectService, { endpoints } from '@/services/connectService';

type ItemStore = {
  items: Item[];
  patch: Date | null;
};

const store = 'items';
const storage = defineStorage<ItemStore>(store);

const itemsStore = defineStore(store, {
  state: (): ItemStore =>
    storage.content() ?? {
      items: [],
      patch: null
    },
  actions: {
    async create(data: FormData) {
      const result: ActionResponse = { success: false, message: null };
      try {
        const response = await connectService<Item>(
          endpoints.Inventory.Items.Create,
          { method: 'POST', data }
        );
        result.message = response.message;
        if (response.statusCode !== 201) return result;
        result.success = true;
        this.items.push(response.content!);
        this.patch = response.timeStamp;
        storage.setContent({
          items: this.items,
          patch: response.timeStamp
        });
      } catch (e: any) {
        result.message = e.message;
      }
      return result;
    },
    delete() {
      this.items = [];
      this.patch = null;
      storage.dispose();
    },
    async download() {
      try {
        const response = await connectService<Item[]>(
          endpoints.Inventory.Items.Download
        );
        if (response.statusCode !== 204) return;
        this.patch = response.timeStamp;
        this.items = response.content!;
        storage.setContent({
          items: this.items,
          patch: response.timeStamp
        });
      } catch (e: any) {}
    }
  },
  getters: {
    refresh: state => state.patch === null,
    any: state => state.items.length > 0,
    toTable: state =>
      state.items.map(item => ({
        id: item.uId,
        type: item.type,
        name: item.name,
        sku: item.sku,
        category: item.category,
        unit: item.unit,
        universalProductCode: item.universalProductCode,
        internationalArticleNumber: item.internationalArticleNumber,
        manufacturerPartNumber: item.manufacturerPartNumber,
        internationalStandardBookNumber: item.internationalStandardBookNumber,
        isReturnable: item.isReturnable,
        purchaseInformation: item.purchaseInformation,
        salesInformation: item.salesInformation
      })),
    toSelect: state =>
      state.items.map(item => ({
        title: item.name,
        value: item.uId
      }))
  }
});

export default itemsStore;
