/** @format */

import defineStorage from '@/services/storageService';
import { defineStore } from 'pinia';
import connectService, { endpoints } from '@/services/connectService';

type VendorStore = {
  vendors: Vendor[];
  patch: Date | null;
};

const store = 'vendors';
const storage = defineStorage<VendorStore>(store);

const vendorStore = defineStore(store, {
  state: (): VendorStore =>
    storage.content() ?? {
      vendors: [],
      patch: null
    },
  actions: {
    async create(data: FormData) {
      const result: ActionResponse = { message: null, success: false };
      try {
        const response = await connectService<Vendor>(
          endpoints.Purchases.Vendors.Create,
          { method: 'POST', data }
        );
        result.message = response.message;
        if (response.statusCode !== 201) return result;
        result.success = true;
        if (response.content !== null) this.vendors.push(response.content);
        this.patch = response.timeStamp;
        storage.setContent({
          vendors: this.vendors,
          patch: response.timeStamp
        });
      } catch (error: any) {
        result.message = error.message;
      }
      return result;
    },
    async edit(id: string, data: FormData) {
      const result: ActionResponse = { message: null, success: false };
      try {
        const response = await connectService<Vendor>(
          endpoints.Purchases.Vendors.Edit(id),
          { method: 'PUT', data }
        );
        result.message = response.message;
        if (response.statusCode !== 204) return result;
        result.success = true;
        const index = this.vendors.findIndex(vendor => vendor.id === id);
        if (index === -1) return result;
        if (response.content !== null) this.vendors[index] = response.content;
        this.patch = response.timeStamp;
        storage.setContent({
          vendors: this.vendors,
          patch: response.timeStamp
        });
      } catch (error: any) {
        result.message = error.message;
      }
      return result;
    },
    async download() {
      const result: ActionResponse = { message: null, success: false };
      try {
        const response = await connectService<Vendor[]>(
          endpoints.Purchases.Vendors.Download
        );
        result.message = response.message;
        if (response.statusCode !== 204) return result;
        result.success = true;
        this.patch = response.timeStamp;
        if (response.content !== null) this.vendors = response.content;
        storage.setContent({ vendors: this.vendors, patch: this.patch });
      } catch (error: any) {
        result.message = error.message;
      }
      return result;
    },
    get(id: string) {
      const vendor = this.vendors.find(vendor => vendor.id === id);
      if (vendor === undefined) return null;
      return { ...vendor };
    },
    delete() {
      this.vendors = [];
      this.patch = null;
      storage.dispose();
    }
  },
  getters: {
    refresh: state => state.patch === null,
    any: state => state.vendors.length !== 0,
    toSelect: state =>
      state.vendors.map(vendor => ({
        title: vendor.displayName,
        value: vendor.id
      }))
  }
});

export default vendorStore;
