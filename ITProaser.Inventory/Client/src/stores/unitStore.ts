/** @format */

import { defineStore } from 'pinia';
import connectService, { endpoints } from '@/services/connectService';
import defineStorage from '@/services/storageService';

type UnitStore = {
  units: ItemUnit[];
  patch: Date | null;
};

const store = 'item-units';
const storage = defineStorage<UnitStore>(store);

const unitStore = defineStore(store, {
  state: (): UnitStore =>
    storage.content() ?? {
      patch: null,
      units: []
    },
  actions: {
    async create(data: FormData) {
      const result: ActionResponse = { success: false, message: null };
      try {
        const response = await connectService<ItemUnit>(
          endpoints.Inventory.ItemUnits.Create,
          { method: 'POST', data }
        );
        result.message = response.message;
        if (response.statusCode !== 201) return result;
        result.success = true;
        this.units.push(response.content!);
        storage.setContent({ units: this.units, patch: response.timeStamp });
      } catch (e) {
        result.message = 'default';
      }
      return result;
    },
    delete() {
      this.units = [];
      this.patch = null;
      storage.dispose();
    },
    async download() {
      const response = await connectService<ItemUnit[]>(
        endpoints.Inventory.ItemUnits.Download
      );
      if (response.statusCode !== 204) {
        this.units = [];
        return;
      }
      this.patch = response.timeStamp;
      this.units = response.content!;
      storage.setContent({ units: this.units, patch: response.timeStamp });
    },
    async edit(id: string, data: FormData) {
      const result: ActionResponse = { success: false, message: null };
      try {
        const response = await connectService<ItemUnit>(
          endpoints.Inventory.ItemUnits.Edit(id),
          { method: 'PUT', data }
        );
        result.message = response.message;
        if (response.statusCode !== 204) return result;
        result.success = true;
        const index = this.units.findIndex(unit => unit.uId === id);
        if (index === -1) return result;
        this.units[index].description = response.content!.description;
        this.units[index].name = response.content!.name;
        this.units[index].normalizedName = response.content!.name.toUpperCase();
        storage.setContent({ units: this.units, patch: response.timeStamp });
      } catch (e) {
        result.message = 'default';
      }
      return result;
    },
    get(id: string) {
      return this.units.find(unit => unit.uId === id);
    }
  },
  getters: {
    refresh: state => state.patch === null,
    toSelect: state =>
      state.units.map(unit => ({
        value: unit.uId,
        title: unit.name
      })),
    any: state => state.patch !== null && state.units.length > 0,
    toTable: state =>
      state.units.map(unit => ({
        id: unit.uId,
        name: unit.name,
        description: unit.description
      }))
  }
});

export default unitStore;
