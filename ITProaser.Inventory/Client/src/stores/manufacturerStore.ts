/** @format */

import { defineStore } from 'pinia';
import connectService, { endpoints } from '@/services/connectService';
import defineStorage from '@/services/storageService';

type ManufacturerStore = {
  patch: Date | null;
  manufacturers: ItemManufacturer[];
};

const store = 'item-manufacturers';
const storage = defineStorage<ManufacturerStore>(store);

const manufacturerStore = defineStore(store, {
  state: (): ManufacturerStore =>
    storage.content() ?? {
      manufacturers: [],
      patch: null
    },
  actions: {
    async create(data: FormData) {
      const result: ActionResponse = { success: false, message: null };
      try {
        const response = await connectService<ItemManufacturer>(
          endpoints.Inventory.ItemManufacturers.Create,
          { method: 'POST', data }
        );
        result.message = response.message;
        if (response.statusCode !== 201) return result;
        result.success = true;
        this.manufacturers.push(response.content!);
        storage.setContent({
          manufacturers: this.manufacturers,
          patch: response.timeStamp
        });
      } catch (e) {
        result.message = 'default';
      }
      return result;
    },
    delete() {
      this.manufacturers = [];
      this.patch = null;
      storage.dispose();
    },
    async download() {
      const response = await connectService<ItemManufacturer[]>(
        endpoints.Inventory.ItemManufacturers.Download
      );
      if (response.statusCode !== 204) {
        this.manufacturers = [];
        return;
      }
      this.patch = response.timeStamp;
      this.manufacturers = response.content!;
      storage.setContent({
        manufacturers: this.manufacturers,
        patch: response.timeStamp
      });
    },
    async edit(id: string, data: FormData) {
      const result: ActionResponse = { success: false, message: null };
      try {
        const response = await connectService<ItemManufacturer>(
          endpoints.Inventory.ItemManufacturers.Edit(id),
          { method: 'PUT', data }
        );
        result.message = response.message;
        if (response.statusCode !== 204) return result;
        result.success = true;
        const index = this.manufacturers.findIndex(
          manufacturer => manufacturer.uId === id
        );
        if (index === -1) return result;
        this.manufacturers[index].description = response.content!.description;
        this.manufacturers[index].name = response.content!.name;
        this.manufacturers[index].normalizedName =
          response.content!.name.toUpperCase();
        storage.setContent({
          manufacturers: this.manufacturers,
          patch: response.timeStamp
        });
      } catch (e) {
        result.message = 'default';
      }
      return result;
    },
    get(id: string) {
      return this.manufacturers.find(m => m.uId === id);
    }
  },
  getters: {
    any: state => state.manufacturers.length > 0,
    refresh: state => state.patch === null,
    toSelect: state =>
      state.manufacturers.map(manufacturer => ({
        value: manufacturer.uId,
        title: manufacturer.name
      })),
    toTable: state =>
      state.manufacturers.map(manufacturer => ({
        id: manufacturer.uId,
        name: manufacturer.name,
        description: manufacturer.description
      }))
  }
});

export default manufacturerStore;
