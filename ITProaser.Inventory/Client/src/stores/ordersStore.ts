/** @format */

import defineStorage from '@/services/storageService';
import { defineStore } from 'pinia';

type OrdersStore = {
  purchase: {
    orders: PurchaseOrder[];
    patch: Date | null;
  };
  sale: {
    orders: SaleOrder[];
    patch: Date | null;
  };
};
const defaultStore = {
  purchase: {
    orders: [],
    patch: null
  },
  sale: {
    orders: [],
    patch: null
  }
};
const store = 'orders';
const storage = defineStorage<OrdersStore>(store);

const ordersStore = defineStore(store, {
  state: (): OrdersStore => storage.content() ?? defaultStore,
  getters: {
    refresh: state =>
      state.purchase.patch === null || state.sale.patch === null,
    purchaseOrders: state => state.purchase.orders,
    saleOrders: state => state.sale.orders
  }
});

export default ordersStore;
