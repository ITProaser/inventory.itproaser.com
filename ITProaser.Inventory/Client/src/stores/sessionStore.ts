/** @format */

import { defineStore } from 'pinia';
import { UserManager, UserManagerSettings } from 'oidc-client-ts';

const store = 'session';

const config: UserManagerSettings = {
  authority: 'https://localhost:7011',
  client_id: 'itproaser.inventory',
  redirect_uri: `${window.location.origin}/account/sign-in`,
  response_type: 'code',
  response_mode: 'query',
  scope: 'openid profile',
  post_logout_redirect_uri: `${window.location.origin}/account/sign-out`,
  loadUserInfo: true
};

const sessionStore = defineStore(store, {
  state: () => ({
    manager: new UserManager(config)
  }),
  actions: {
    async isLoggedIn() {
      return (await this.manager.getUser()) !== null;
    },
    async getUser() {
      return await this.manager.getUser();
    }
  }
});

export default sessionStore;
