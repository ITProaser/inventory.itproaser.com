/** @format */

import defineStorage from '@/services/storageService';
import { defineStore } from 'pinia';

const store = 'error';
const storage = defineStorage<{
  errors: string[];
  show: boolean;
}>(store);

const errorStore = defineStore(store, {
  state: () =>
    storage.content() ?? {
      errors: [],
      show: false
    },
  actions: {
    addError(error: string) {
      this.errors.push(error);
      this.show = true;
      storage.setContent({
        errors: this.errors,
        show: this.show
      });
    },
    close() {
      this.show = false;
      storage.setContent({
        errors: this.errors,
        show: this.show
      });
    }
  },
  getters: {
    current: (state): string | null =>
      !state.show ? null : state.errors[state.errors.length - 1]
  }
});

export default errorStore;
