# IT Products and Services - Inventory

[![pipeline status](https://gitlab.com/ITProaser/inventory.itproaser.com/badges/main/pipeline.svg)](https://gitlab.com/ITProaser/inventory.itproaser.com/-/commits/main)
[![coverage report](https://gitlab.com/ITProaser/inventory.itproaser.com/badges/main/coverage.svg)](https://gitlab.com/ITProaser/inventory.itproaser.com/-/commits/main)
[![Latest Release](https://gitlab.com/ITProaser/inventory.itproaser.com/-/badges/release.svg)](https://gitlab.com/ITProaser/inventory.itproaser.com/-/releases)

![IT Products and Services Logo](./ITProaser.Inventory/Client/public/inventory-512.png)